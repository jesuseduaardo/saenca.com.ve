function enviaform(formulario){
	$.ajax({
		url: $(formulario).attr("action"),
		type: $(formulario).attr("method"),
		data: $(formulario).serialize(),
		beforeSend:function(){
			mostrar();
		},
		success:function(data){
			//$(".loader").fadeOut("slow");
			 //$("#cart").load('carrito.php');
			 ocultar();
			 location.reload()
		}
	});
}
function getData(url, capa){
	$.ajax({
		url: url,
		type: 'GET',
		beforeSend:function(){
			$(capa).empty();
			$div = $("<div class='loading'></div>");
			$(capa).append($div);
		},
		success:function(data){
			$(capa).empty();
			$(capa).append(data);
		},
        error: function () {
			$(capa).empty();
			$div = $("<div class='error'>Hubo un error de comunicacion. Recargue la pagina e intente de nuevo</div>");
			$(capa).append($div);
        }
	});
}