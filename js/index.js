function removedisable(element){
	
	id = $(element).attr('data-code');
	if($('#cantidad'+id).val() > 0)
	{
		$('#save'+id).removeAttr('disabled');
	}
	else
	{
		$('#save'+id).attr('disabled', 'disabled');
	}
}

function onScroll() {
	if ($(window).scrollTop() > 50){
		$('#shop-cart').css('top', '10px');
	}else{
		$('#shop-cart').css('top', '113px');
	}
}
window.addEventListener('scroll', onScroll, false);

function addtocart(element){
		id = $(element).attr('data-code');
		formulario = $('#form'+id);
		var cart = $('#shop-cart');
        var imgtodrag = $('#img'+id);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                	'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(/*function () {
                cart.effect("shake", {
                    times: 0
                }, 200);
            }, */1500);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
	$.ajax({
		url: '../home/agregarProducto',
		type: 'POST',
		data: $(formulario).serialize(),
		success:function(response){
			$('#numero').html(response);
			$(formulario).find('select').val('0')
			
		},
		error : function() {
            alert('Disculpe, existió un problema, Recargue la pagina');
        }
	});
}
$('.btn-search').on('click', function(){
	if($('#consulta').val().length > 2){
		$("#buscar").attr('action','home/buscando#productos');
		$("#buscar").submit();
	}else{
		$("#buscar").attr('action','');
	}
});