var app = angular.module('app', ['ngRoute', 'ngResource','angular-owl-carousel-2', 'ui.bootstrap'])
.value('THROTTLE_MILLISECONDS', 250)
.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])
.config(['$routeProvider', function($routeProvider){
	$routeProvider.when('/', {
			templateUrl:'pages/productos_view.html',
			controller: 'ItemCtrl'
		})
		.when('/:categoria', {
			templateUrl:'pages/productos_view.html',
			controller: 'ItemCtrl'
		})
		.when('/:categoria/:id/:nombre', {
			templateUrl:'pages/detalle_view.html',
			controller: 'ItemCtrl'
		})
		.otherwise({ reditrectTo : "/" });
}])
.run(['$rootScope', '$route', function($rootScope, $route){
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = $route.current.title;
	});
}])
.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
		string = string.replace(/^\s+|\s+$/gm,'');
        return string.replace(/[\s]/g, '_').toLowerCase();
    };
}])
app.controller('MainController', function($scope, $route, $routeParams, $location) {
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
 })
//Item Controller
app.controller('ItemCtrl', ['$scope', '$timeout', '$sce', '$route', '$routeParams', '$http', function($scope, $timeout, $sce, $route, $routeParams, $http){
	
	$scope.loading = true;
	var owlAPi;
	var categoria = $routeParams.categoria;
	var id = $routeParams.id;
	var nombre = $routeParams.nombre;
	
	$scope.titleMaker = function(_title){
		var nTitle = _title.replace(/_/gi, ' ');
		return nTitle.charAt(0).toUpperCase() + nTitle.slice(1);
	}
	
	if(categoria != null && isFinite(id) && nombre != null){
		
		$scope.title =  $scope.titleMaker($routeParams.nombre)
		
		$http.get('/invntr/'+categoria+'/'+id+'/'+nombre).then(function(response){
				$scope.items = response.data;
				$scope.slides = [
								{ image : $scope.items.producto[0].p_1 }, 
								{ image : $scope.items.producto[0].p_2 }, 
								{ image : $scope.items.producto[0].p_3 }
								];
				$scope.loading = false;
				//owl-Carrusel
				$scope.owlitems = $scope.items.sugerencias;
			});
				
		$scope.myInterval = 1000;
		
		$scope.properties2 = {
					autoHeight: false,
    				autoHeightClass: 'owl-height',
					loop:true,
					autoplay:true,
					nav:true,
					animateIn: 'fadeIn',
					lazyLoad: true,
					items: 1,
					margin:0
		};
		
		$scope.properties = {
					autoHeight: false,
    				autoHeightClass: 'owl-height',
					loop:true,
					autoplay:true,
					animateIn: 'fadeIn',
					lazyLoad: true,
					items: 3,
					margin: 10
		};
				
		$scope.ready = function ($api) {
					owlAPi = $api;
		};
				
		$timeout(function () {
			owlAPi.trigger('next.owl.carousel',[1000]);
		}, 1000)
						
	}else if(categoria!=null){
		
		$scope.title =  $scope.titleMaker($routeParams.categoria);
		
		$http.get('/invntr/'+categoria).then(function(response){
			$scope.items = response.data;
			$scope.loading = false;
		});
		
	}else{
		
		$http.get('/invntr').then(function(response){
			$scope.items = response.data;
			$scope.loading = false;
		});
		
	}

	$scope.descripcion = function(_descripcion)
	{
		return $sce.trustAsHtml(_descripcion);
	}
	
	
/*	if(isFinite(id)){
		$http.get('home/producto',{categoria:categoria, id:id, nombre:nombre}).then(function(response) {
        	$scope.items = response.data;
			$scope.loading = false;
    	});
	}
	else if(id!=''){
		$http.get('home/producto',{categoria:id}).then(function(response) {
        	$scope.items = response.data;
			$scope.loading = false;
    	});
	}else{
		$http.get('home/producto').then(function(response) {
        	$scope.items = response.data;
			$scope.loading = false;
    	});
		}*/

	
	$scope.creafix = function(num)
	{
		if((num % 2) == 0)
		{
			return 'xs';
		}
		if((num % 3) ==0)
		{
			return 'sm';
		}
		if((num % 4) ==0)
		{
			return 'md';
		}
		return null
	}
	
	$scope.qty = function(_cantidad)
	{
		var qty = [];
		for(i=1;i<=_cantidad;i++)
		{
			qty[i]=i
		}
		return qty;
	}
	
	$scope.guardar = function(_items){
			$scope.loading = true;
			Item.save({items: _items}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					$scope.loading = false;
					alertify.success(data.response);
				}else if(typeof(data.error)!== 'undefined')
				{
					$scope.loading = false;
					alertify.error(data.error);
				}
			})
		}
	$scope.update = function(_id, _items){
		$scope.loading = true;
		Item.update({id:_id}, {items:_items}, 
			function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					$scope.loading = false;
					alertify.success(data.response);
				}
				else if(typeof(data.error)!== 'undefined')
				{
					$scope.loading = false;
					alertify.error(data.error);
				}
			});
		}
	$scope.remove = function(id)
	{
		$scope.loading = true;
		Item.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				$scope.loading = false;
				$('#fila-'+id).slideUp('slow');
				alertify.success(data.response);
			}
			else if(typeof(data.error)!== 'undefined')
			{
				$scope.loading = false;
				alertify.error(data.error);
			}
		});
	}
}])
.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
	  attrs.$observe('ngSrc', function(value) {
		  if (!value && attrs.errSrc) {
			attrs.$set('src', attrs.errSrc);
		  }
		});
    }
  }
});
String.prototype.trunc = String.prototype.trunc ||
function(n){

    // this will return a substring and 
    // if its larger than 'n' then truncate and append '...' to the string and return it.
    // if its less than 'n' then return the 'string'
    return this.length>n ? this.substr(0,n-1)+'...' : this.toString();
};