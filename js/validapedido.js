// JavaScript Document
$(document).ready(function() {
    $("#pedido").validate({
        rules: {
            nombre: { required: true, minlength: 2},
			cirif: { required: true, minlength: 8},
            telefono: { required: true, minlength: 11},
            email: { required:true, email: true},
            direccion1: { minlength: 10, maxlength: 200},
            captcha: { required: true},
        },
        messages: {
            nombre: "Debe introducir su nombre o razon social.",
			cirif: "Debe introducir un numero de documento valido.",
            telefono: "Debe introducir su telefono.",
            email : "Debe introducir un email válido.",
            direccion1 : "Debe introducir su direccion.",
            captcha : "Debe completar el captcha para procesar su informacion.",
        },
        submitHandler: function(form){/*
            var dataString = 'name='+$('#name').val()+'&lastname='+$('#lastname').val()+'...';
            $.ajax({
                type: "POST",
                url:"send.php",
                data: dataString,
                success: function(data){
                    $("#pedido").hide();
                }
            });
        */
		$('form#pedido').submit();
		}
    });
	$('#cirif').on("keydown", function(tecla){
        if(tecla.which == 86){
			$('#cirif').val('V-');
			return false;
		}
		else if(tecla.which == 74){
			$('#cirif').val('J-');
			return false;
		}
		else if(tecla.which == 69){
			$('#cirif').val('E-');
			return false;
		}
		else if(tecla.which == 71){
			$('#cirif').val('G-');
			return false;
		}
		if((tecla.which > 95 && tecla.which < 106 || tecla.which == 8) && ($('#cirif').val().length > 1))
		{
			return true;
		}else{
			return false;
		}
    });
    $('#telefono').on("keydown", function(tecla){
        if(tecla.which > 95 && tecla.which < 106 || tecla.which == 188 || tecla.which == 8){
			return true;
		}
		else
		{
			return false;
		}
    });
	$('#email').on("keydown", function(tecla){
        if(tecla.which == 32){
			return false;
		}
    });
});