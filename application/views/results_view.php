<br>
<section class="items">
  <div class="container">
    <div class="row">
    <div class="col-xs-12">
    <?php
    if(count($productos)>0)
	{
	?>
    <p style="padding:5px 0 15px;">Se encontraron <?=count($productos)?> resultado(s) para su busqueda</p>
    <?php
    foreach($productos as $valor)
    {
            ?>
            <div class="col-xs-12 col-md-3"><?php $this->load->view('templates/productbox.php', $valor)?></div>
            <?php
    }
	}
	else
	{
		?>
        <br><br><br><br><br>
		<h2 style="text-align:center;">No se encontraron resultados para su busqueda</h2>
        <div style="padding:80px 0 160px; margin:auto;text-align:center;">
        	<button class="btn btn-success" id="mostrartodo">Ver todos los productos</button>
        </div>
		<?php
	}
    ?>
    </div>
    </div>
  </div>
<br/><br/>
</section>
<div class="clearfix"></div>
<script>
$('#mostrartodo').on('click', function(){
	window.location.href = 'productos#productos'
	});
</script>