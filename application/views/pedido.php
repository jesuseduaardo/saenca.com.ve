<?php
if ($carrito = $this->cart->contents()){
?>
<br />
<div class="container">
	<h2 style="padding-top:20px;">Crear Pedido</h2>
</div>
<section id="articulos">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="pedido">
                    <table class="infopedido">
                               <legend>Articulos</legend>
                               <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th>Cant.</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                    		<?php
                            foreach ($carrito as $item) {
                            ?>
                                <tbody>
                                <tr>
                                    <td>
									<?=isset($item['options']['cuponeable']) && $item['options']['cuponeable'] == 1 ? '<i title="Aplica un cupón de descuento sobre este producto" class="fa fa-money cupon"></i>' : '<i title="Este producto no aplica para descuentos" class="fa fa-money cupon no-cupon"></i>'?>
                                    &nbsp;
									<?= ucfirst($item['name']) ?>
                                    </td>
                                        <?php
                                        $nombres = array('nombre' => ucfirst($item['name']));
                                        $precio = ($item['price'] * $item['qty']) / 1.12;
                                        ?>
                                    <td><?='Bs '.number_format($precio , 2, ',', '.');?></td>
                                    <td><?=$item['qty'] ?></td>
                                    <td>
                                    <a style="cursor:pointer" onClick="removepedido(this)" data-code="<?=$item['rowid']?>" title="Eliminar del pedido"><b class="delete">X</b></a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td id="cupon-code">
                                <?php
                                	$cupon = isset($_SESSION['cupon']) ? explode(',', $_SESSION['cupon']) : NULL; 
								?>
								<?=isset($cupon) && is_array($cupon) ? '<strong>Cupon Codigo: '.$cupon[2].'<strong>' : ''?>
                                </td>
                                <td id="cupon-val">
                                <?php
									if(isset($cupon) && is_array($cupon))
									{
										if($cupon[0] == 'freesend')
										{
											echo 'Envio Gratis';
										}
										else
										{
											echo '- '.$cupon[0].number_format($cupon[1], 2, ',', '.');
										}
									}
                                ?>
                                </td>
                                <td id="cupon-cant"><?=isset($cupon) && is_array($cupon) ? 1 : ''?></td>
                                <td id="cupon-del"><?=isset($cupon) && is_array($cupon) ? '<a style="cursor:pointer" onClick="removepedido(this)" title="Eliminar del pedido"><b class="delete">X</b></a>' : ''?></td>
                            </tr>
                            </tbody>
                            <?php 
								$CI =& get_instance();
								$totalpedido = $CI->checkout();
							?>
                            <tfoot style="font-size:15px;">
                            <tr>
                                <td align="right">
                                	Subtotal&nbsp;
                                 </td>
                                 <td colspan="2">
                                 <strong>
                                 	Bs&nbsp;
                                 	<span id="subt"><?=number_format($totalpedido[0], 2, ',', '.')?></span>
                                </strong> 
                                </td>
                                  <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">
                                	Iva 12%&nbsp; 
                                 </td>
                                 <td colspan="2">
                                    <strong>
                                    Bs&nbsp;
                                    <span id="iva"><?=number_format($totalpedido[1], 2, ',', '.')?></span>
                                    </strong>
                                 </td>
                                 <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">
                                	Total&nbsp;
                                 </td>
                                 <td colspan="2">
                                 	<strong>
                                    	Bs&nbsp;
                                 		<span id="total"><?=number_format($totalpedido[2], 2, ',', '.')?></span>
                                 	</strong>
                                 </td>
                                 <td>&nbsp;</td>
                            </tr>
                            <?php if(!isset($cupon) && !is_array($cupon)){?>
                            <tr id="cupon-fila">
                                <td colspan="4" style="text-align:center;"><a href="#" id="act-modal" data-toggle="modal" data-target="#myModal" title="Aplicar cupon de descuento">Aplicar cup&oacute;n de descuento</a></td>
                            </tr>
							<?php } ?>
                            </tfoot>
                        </table>
                	<?=form_close();?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
    <div class="col-xs-12">
    <div class="pedido">
    <a href="#pedido"></a>
    <p>Favor llenar cuidadosamente la siguiente informaci&oacute;n ya que esta servira para la elaboraci&oacute;n de su factura as&iacute; como para el envio de su compra</p>
    <?php
    echo form_open(base_url().'creapedido',' id="formpedido"');
    echo '<div>';
    echo form_label('Nombres y Apellidos o Raz&oacute;n Social<span class="obli">(*)</span>','nombre');
    echo form_input('nombre', set_value('nombre'),'id="nombre" required="required"');
    echo '<span class="indi">Nombre y Apellido Completo / Raz&oacute;n Social como aparece en el Rif</span>';
    echo form_error('codigo');
    echo '</div>';
    echo '<div>';
    echo form_label('Cedula o Rif<span class="obli">(*)</span>','cirif');
    echo form_input('cirif', set_value('cirif'),'id="cirif" required="required"');
    echo '<span class="indi">Si es cedula Colocar <b>V-12345678</b> si es Rif <b>J-123456789</b></span>';
    echo form_error('cirif');
    echo '</div>';
    echo '<div>';
    echo form_label('Tel&eacute;fono(s)<span class="obli">(*)</span>','telefono');
    echo form_input('telefono', set_value('telefono'),'id="telefono" required="required"');
    echo '<span class="indi">Si es m&aacute;s de un tel&eacute;fono favor separar con una coma (,)</span>';
    echo form_error('telefono');
    echo '</div>';
    echo '<div>';
    echo form_label('Email<span class="obli">(*)</span>','email');
    echo form_input('email', set_value('email'),'id="email" required="required"');
    echo form_error('email');
    echo '</div>';
    echo '<div>';
    echo form_label('Direcci&oacute;n fiscal<span class="obli">(*)</span>','direccion1');
    echo form_textarea('direccion1', set_value('direccion1'),'id="direccion1" required="required"');
    echo '<span class="indi">Direcci&oacute;n tal como aparece en el Rif. Necesario para elaborar su factura</span>';
    echo form_error('direccion1');
    echo '</div>';
    echo '<div>';
    echo form_label('Direcci&oacute;n de envio','direccion2');
    echo form_textarea('direccion2', set_value('direccion2'),'id="direccion2"');
    echo '<span class="indi">Sea especifico y coloque todas las referencias necesarias. Necesario para la empresa de envio</span>';
    echo form_error('direccion');
    echo '</div>';
    $numero=0;
    ?>
    <div class="pedido" style="text-align:center">
    <!--Info--Captcha-->
      <p id="data_captcha">Click sobre la imagen para cambiar</p>
      <div id="image_captha" style="margin:0"><?=$captcha['image']?></div>
      <div class="captchainput">
      <label for="captcha">Escriba lo que ve en la imagen</label>
      <input type="text" name="captcha" required="required" placeholder="captcha" autocomplete="off" />
      </div>
      <?=form_error('captcha')?>
      </div>
    <!--End--Info-Captcha-->
    <div style="text-align:center;">
    <input type="submit" class="btn btn-lg btn-success" value="Generar Pedido"/>
    <?=form_close();?>
    <div class="obli" style="font-size:14px">(*)Campos obligatorios</div>
    </div>
    </div>
    </div>
    </div>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<form id="cupon" class="form-horizontal">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
        	<h4 class="modal-title">Ingrese el Codigo de su Cup&oacute;n</h4>
      	</div>
      	<div class="modal-body">
        	<div class="form-group">
        		<label for="codigo" class="control-label col-sm-2">Codigo</label>
                <div class="col-sm-10">
            		<input type="text" name="codigo" id="cupval" class="form-control" autofocus autocomplete="off" maxlength="16" min="8">
                </div>
             </div>
      	</div>
      	<div class="modal-footer">
        	<input type="submit" class="btn btn-success" value="Aceptar" id="cup-act"/>
      	</div>
      	</form>
    </div>

  </div>
</div>


<?php $this->load->view('templates/pasoscompra.php')?>
</div>
<?php
}
else
{
	redirect('/productos#productos');
}
?>
<script type="text/javascript">
	$('#image_captha').click(function(){
		Enviar('<?=base_url()?>recarga_captcha','image_captha')
	});
	$('#cupon').on('submit', function(e){
		e.preventDefault();
		if($('#cupval').val() != '' && $('#cupval').val().length >= 8)
		{
			$.ajax({
				url: 'home/addcupon',
				type: 'POST',
				data: $('.form-horizontal').serialize(),
				beforeSend:function(){
					$div = $("<div class='loading'></div>");
					$('.modal-body').html($div);
				},
				success:function(data){
					$('#cup-act').attr('type', 'button');
					$('#cup-act').attr('data-dismiss', 'modal');
					$('#cupon-fila').slideUp();
					$('.modal-body').html('<h4>Se ha aplicado el cupón a su pedido</h4>');
					setdata(data);
				},
				error: function (jqXHR, textStatus, error) {
					$('.modal-body').html('<h4>'+jqXHR.responseText+'</h4>');
					$('#cup-act').hide();
				}
			});
		}
	});
	function setdata(data)
	{
		$('#cupon-code').html('<strong>Cupon Codigo:  ' + data.response[0].cu_code+ '<strong>');
		if(data.response[0].cu_modo > 2)
		{
			$('#cupon-val').html('<strong>Envio Gratis<strong>');
		}
		else if(data.response[0].cu_modo == 2)
		{
			$('#cupon-val').html('<strong>- Bs' + data.response[0].cu_monto+ '<strong>');
		}
		else if(data.response[0].cu_modo == 1)
		{
			$('#cupon-val').html('<strong>- %' + data.response[0].cu_monto+ '<strong>');
		}
		$('#iva').html(parseFloat(data.response[0].total[1]).toFixed(2));
		$('#subt').html(parseFloat(data.response[0].total[0]).toFixed(2));
		$('#total').html(parseFloat(data.response[0].total[2]).toFixed(2));
		$('#cupon-cant').html(1);
		$('#cupon-del').html('<a style="cursor:pointer" onClick="removecupon(this)" title="Eliminar del pedido"><b class="delete">X</b></a>');
	}
</script>