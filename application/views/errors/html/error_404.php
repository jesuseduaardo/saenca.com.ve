<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>404 Pagina no encontrada</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<style>
body{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,f7f7f7+49,f2f2f2+51,ffffff+100 */
background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(top,  #ffffff 0%, #f7f7f7 49%, #f2f2f2 51%, #ffffff 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #ffffff 0%,#f7f7f7 49%,#f2f2f2 51%,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #ffffff 0%,#f7f7f7 49%,#f2f2f2 51%,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
	}
h1, h2{
	text-align:center;
	}
h2{
	padding:2px;
	}
.col-xs-12 img{
	text-align: center;
    margin: auto;
    display: block;
	}
ul{
	display:block;
	margin:auto;
	text-align:center;
	}
ul > li{
	float:left;
	font-size:20px;
	padding:10px;
	list-style:none;
	}
ul > li > a{
	text-decoration:none;
	}

#buttons{
    float:right;
    position:relative;
    left:-50%;
    text-align:left;
}
#buttons ul{
    list-style:none;
    position:relative;
    left:50%;
}

#buttons li{float:left;position:relative;}/* ie needs position:relative here*/

#buttons a{
    text-decoration:none;
    margin:10px;
    float:left;
    padding:2px 5px;
    text-align:center;
    white-space:nowrap;
}
#buttons a:hover{ 
	border:1px solid #E4E4E4;
	}
#content{overflow:hidden}/* hide horizontal scrollbar*/
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
			<h1>La Pag&iacute;na que buscas no se encuentra o ya no existe</h1>
            <img src="http://saenca.com.ve/img/404.png" class="img-responsive" alt="Pagina no encontrada"/>
        </div>
        <div class="col-xs-12">
        	<h2>Vuelve a Salvo</h2>
            <div id="buttons">
                <ul>
                    <li><a href="http://saenca.com.ve/">Inicio</a></li>
                    <li><a href="http://saenca.com.ve/nosotros#empresa">Nosotros</a></li>
                    <li><a href="http://saenca.com.ve/productos#productos">Productos</a></li>
                    <li><a href="http://saenca.com.ve/servicios#servicios">Servicios</a></li>
                    <li><a href="http://saenca.com.ve/contacto#ubicacion">Contactenos</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>