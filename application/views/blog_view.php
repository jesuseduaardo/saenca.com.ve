<?php 
require_once('templates/tiempo.php');
?>
<br/>
<br/>
<br/>
<br/>
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-9">
<?php if(!empty($entries)):?>
<?php foreach($entries as $entry):?>
<h2><?=anchor(base_url().'blog/articulo/'.$entry['permalink'].'#articulo',$entry['titulo'])?> </h2>
<p><?=$entry['cintro']?></p> 
<div class="col-xs-12">
  <img src="<?=$entry['imgprincipal']?>" class="img-responsive" alt="<?=$entry['titulo']?>"/> </div>	
<?php 
	$this->load->model('site_model','model');
	$visitas['visitas'] = $this->model->visto($entry['id']);
	$this->load->view('templates/contador', $visitas);
?>
<p class="datecreate">Creado el <?=date_format(date_create($entry['fecha']), 'd-m-Y') ?> - Hace <?=damefecha($entry['fecha'])?></p>
<hr/>
<?php endforeach; ?>
<?php else : ?>
<h1>No entries</h1>
<?php endif; ?> 
<div class="col-xs-12" style="text-align:center;">
<?php
if(isset($paginas) && !empty($paginas))
{
	echo $paginas;
}
?>
</div>
</div>
<div class="col-xs-12 col-md-3">
<div class="row">
<div class="col-xs-12" id="archivos">
<legend id="archivos">Archivos</legend>
<?php
if(isset($fechas))
{
	$ano =0;
	$mes ='';
	$vuelta=0;
	echo '<ul>';
	foreach($fechas as $val)
	{
		$nfecha = explode('-', (date_format(date_create($val['fecha']), 'd-m-Y')));
		if($ano!=$nfecha[2])
		{
			if($vuelta > 1)
			{
				echo '</ul>';
				echo '</li>';
			}
			$ano = $nfecha[2];
			echo '<li><a href="#" rel="nofollow">'.$ano.'</a>';
			echo '<ul>';
		}
		if($mes!= mes($nfecha[1]))
		{
			$mes = mes($nfecha[1]);
				echo '<li><a href="#" rel="nofollow">'.$mes.'</a></li>';
		}
		$vuelta++;
	}
	echo '</ul>';
}
?>
</div>
<div class="col-xs-12">
<legend id="masvistoleg">Mas Vistos</legend>
<div class="col-xs-12" id="masvistos">
<?php 
if(!empty($masvisto)): 
foreach($masvisto as $visto):
?>
<h4><?=anchor(base_url().'blog/articulo/'.$visto['permalink'].'#articulo',$visto['titulo'])?> </h4>
<p class="datecreate">Hace <?=damefecha($visto['fecha'])?></p>	
<?php 
	$this->load->model('site_model','model');
	$visitas['visitas'] = $this->model->visto($visto['id']);
	$this->load->view('templates/contador', $visitas);
?>
<hr/>
<?php endforeach; ?>
<?php else : ?>
<h1>No entries</h1>
<?php endif; ?> 
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$(function() {
    $('li').filter(function(i){
		return $('ul', this).length >= 1; 
		})
		.each(function(i){
        	$(this).children("a").click(function(e){
				event.preventDefault();
            	var $ul = $(this).next("ul");
            	if ($ul.is(":visible")) {
               	 	$ul.find("ul").slideUp();
                	$ul.slideUp();
            	}
            	else {
                	$ul.slideDown();
            	};
        })
    });
});
</script>