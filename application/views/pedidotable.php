<div id="tablanota">
<table class="nota" width="100%;">
  <tbody>
		  <td colspan="3">
          <img src="<?=base_url()?>img/logo.png" alt="<?=RAZONSOCIAL?>" style="margin:auto auto -20px;" width="200"/>
          <br>
          <font><?=RAZONSOCIAL?><br><?=RIF?></font>
          <br><br>
          </td>
		  <td valign="middle" align="right">
          <h3>Pedido Nro <strong style="color:#D90003 !important"><?=str_pad($id, 6, '0', STR_PAD_LEFT)?></strong></h3>
          <b>Fecha</b>: <?=date_format(date_create($fecha), 'd-m-Y')?>
          </td>
		  </tr>
		<tr>
		  <td colspan="4"><b>Razon Social: </b><?=$nombre?></td>
		  </tr>
		<tr>
		  <td colspan="4"><b>
		    <?php
          $identificacion = explode('-', $cirif);
		  echo strtolower($identificacion[0])=='v' || strtolower($identificacion[0])=='e' ? 'Cedula:' : 'Rif:';
		  ?>
		    </b> <?=$cirif?></td>
		  </tr>
		<tr>
		  <td colspan="4"><b>Direccion</b>: <?=$direccion1?></td>
		  </tr>
        <?php if(isset($telefono)){?>  
		<tr>
        <?php
        $telefono = explode(',', $telefono);
		?>
		  <td colspan="4"><b>Telefono</b>: 
		  <?php 
		  foreach($telefono as $key => $valor)
		  { 
		  	if($key==0)
			{
		  		echo $valor;
			}
			else
			{
				echo ' - '.$valor;
			}
		  }
		  ?>
          </td>
		  </tr>
        <?php }?>
		<tr>
		  <td colspan="4"><b>Email</b>: <?=$email?></td>
		  </tr>
          <tr>
          	<td colspan="4">&nbsp;</td>
          </tr>
		<tr>
        <td colspan="4">
        	<table width="100%" class="table table-striped">
        		<thead>
            		<tr>
		  				<th width="8%">Cant.</th>
		  				<th width="15%">Cod.</th>
		  				<th colspan="2">Nombre</th>
		  				<th width="20%">Subtotal</th>
		  			</tr>
                </thead>
                <tbody>
 					<?php
					$valid = 0;
					$novalid = 0;
					foreach($productos as $value)
					{
						$cupon = explode('-', $value['i_cod']);
						if($cupon[0] == 'cupon')
						{
							$cup = $value;
							continue;
						}
						else
						{
							unset($cupon);
						}
					?>
						<tr>
						<td><?=$value['cantidad']?></td>
						<td><?=$value['i_cod']?></td>
						<td colspan="2"><?= $value['i_nombre']?></td>
						<?php 
							$sub= round(($value['cantidad'] * $value['subtotal']), 2);	
						?>
						<td>
                        <?php
							echo number_format(sprintf("%01.2f", $sub),"2", ",", ".");
						?>
                        </td>
					  </tr>
					<?php
						if($value['i_cupon'] > 0)
						{
							$valid += $sub;
						}
						else
						{
							$novalid += $sub;
						}
					}
					?>               
                </tbody>
                <tfoot>	
                  <?php
				  if(isset($cup))
				  {
					switch($cup['m_nombre'])
					{
						case 'porc':
							$val = -($valid * $cup['subtotal']) / 100;
							$valid+= $val;
							$nombre = 'descuento de %'.$cup['subtotal'];
						break;
						case 'monto':
							$val = $valid - $cup['subtotal'];
							$valid-= $val;
							$cup = 'descuento de Bs'.number_format(sprintf("%01.2f", $cup['subtotal']),"2", ",", ".");
						break;
						case 'envio':
							$val = 'Envio Gratis';
							$cup = 'Envio Gratis';
						break;
					}
				  ?>
                  <tr>
		  				<td><?=$cup['cantidad']?></td>
		  				<td><?=$cup['c_nombre']?></td>
		  				<td colspan="2">Cupon <?='<strong>'.$cup['i_nombre'].'</strong> '.$nombre?></td>
		  				<td><?=$val?></td>
		  		  </tr>
                  <?php } ?>
                  <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td colspan="2" align="right">&nbsp;</td>
                      <td>&nbsp;</td>
                  </tr>
                  <?php
                  $subtotal= $valid + $novalid;
				  ?>
                  <tr>
                      <td colspan="3">&nbsp;</td>
                      <td width="5%" align="right"><strong>Subtotal</strong></td>
                      <td><?=number_format(sprintf("%01.2f", $subtotal),"2", ",", ".")?></td>
                  </tr>
                  <?php
				  if((strtolower($identificacion[0])=='v' || strtolower($identificacion[0])=='e') && ($subtotal < 200000))
				  {
					  $i = 10;
				  }
				  else
				  {
					  $i = 12;
				  }
				  ?>
                  <tr>
                      <td colspan="3">&nbsp;</td>
                      <td align="right"><strong>
                        Iva (<?=$i?>%)
                      </strong></td>
                      <td>
					  	<?php
                        $iva = ($subtotal * $i) / 100;
						echo number_format(sprintf("%01.2f", $iva),"2", ",", ".");
						?>
                       </td>
                  </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                      <td align="right"><strong>Total</strong></td>
                      <td><?=number_format(sprintf("%01.2f", $subtotal + $iva),"2", ",", ".");?></td>
                      </tr>
                </tfoot>
        	</table>
        </td>
        </tr>
        <tr>
		  <td colspan="4">
          <?php
	if(isset($contacto))
	{
          foreach($contacto as $cont)
		  {
			  $email = $cont['email'];
			  $telefono = $cont['telefono'];
		  }
		  $numero = str_split(trim($telefono));
		  for($i = 0;$i < count($numero);$i++)
		  {
			  switch($i)
			  {
				  case 0:
				  $nuevo_numero='('.$numero[$i];
				  break;
				  case 3:
				  $nuevo_numero.=$numero[$i].')';
				  break;
				  case 6:
				  $nuevo_numero.=$numero[$i].'-';
				  break;
				  case 8:
				  $nuevo_numero.=$numero[$i].'-';
				  break;
				  case 10:
				  $nuevo_numero.=$numero[$i];
				  break;
				  default:
				  $nuevo_numero.=$numero[$i];
				  break;
			  }
		  }
	}
		  ?>
          <br/>
          <ul style="margin: 0 0 0 25px;">
          <li><strong>Este pedido tendr&aacute; una vigencia de 24 horas h&aacute;biles</strong></li>
          <li><strong>La empresa no garantiza la disponibilidad total de los productos al momento de relizar el pedido.</strong></li>
          <li style="color:#FF3F00"><strong>Para Concretar la Venta Comunicarse al los tel&eacute;fonos <em><?=$telefono?></em> o a trav&eacute;s de <em><?=$contacto[0]['email']?></em> indicando el nro del pedido</strong></li>
<!--		  <li><strong>Puede Pagar En Efectivo En Nuestro Local Comercial o Con Su Tarjeta De
Débito o (Credito 10% adicional)</strong></li>-->
		  <li><strong>Disponemos para su pago en dep&oacute;sito o transferencia de las siguentes cuentas bancarias:</strong></li>
          </ul>
          <br/>
          <table width="100%" cellpadding="10" class="table table-condensed" align="center" style="margin:auto;">
            <tr>
              <th >Banco</th>
              <th >Cuenta</th>
              <th >Titular</th>
              <th >CI/RIF</th>
              <th >Nro  Cuenta</th>
            </tr>
            <tr>
              <td >Banco Nacional De Credito BNC</td>
              <td >Corriente</td>
              <td >Distribuidora Saenca Comercial CA</td>
              <td >J407695320</td>
              <td >0191-0142812100045338</td>
            </tr>
            <tr>
              <td >Banesco</td>
              <td >Corriente</td>
              <td >Saul Castillo</td>
              <td >V-17.929.180</td>
              <td >0134-0866-1686-6101-4750</td>
            </tr>
            <tr>
              <td >Mercantil</td>
              <td >Ahorros</td>
              <td >Saul Castillo</td>
              <td >V-17.929.180</td>
              <td >0105-0083-4100-8350-8902</td>
            </tr>
            <tr>
              <td >Venezuela</td>
              <td >Corriente</td>
              <td >Saul Castillo</td>
              <td >V-17.929.180</td>
              <td >0102-0221-3100-0051-6332</td>
            </tr>
          </table>
          <br />
          <strong>Recuerde notificar su pago, enviandonos el comprobante de deposito o tranferencia asi como el nro de su pedido</strong>
          <br/>
          <h3 align="center">VALIDO POR 24 HORAS</h3>
          </td>
		</tr>	
  </tbody>
</table>
</div>