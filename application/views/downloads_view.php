<a name="providencias&manuales" id="info">.</a>
<section>
<div class="container">
<div class="row">
<div class="col-md-9 route">
<i class="glyphicon glyphicon-home"></i>&nbsp;<a href="./inicio">Inicio</a> / Descargas
</div>
<div class="col-xs-12 col-md-9 spacebefore">
<legend style="text-align:center;">Providencias Adminstrativas</legend>
<div class="col-xs-12 col-md-8">
<div class="panel panel-default">
  <div class="panel-heading">Providencia 0257</div>
  <div class="panel-body">
    Providencia que establece las normas generales de emisi&oacute;n de facturas y otros documentos
  </div>
  <div class="panel-footer" style="text-align:right">
      <a class="glyphicon glyphicon-eye-open" href="#" title="Ver"></a>&nbsp;
      <a class="glyphicon glyphicon-arrow-down" title="Descargar"></a>
  </div>
</div>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-md-8">
<div class="panel panel-default">
  <div class="panel-heading">Providencia 071</div>
  <div class="panel-body">
    Providencia que establece las normas generales de emisi&oacute;n de facturas y otros documentos
  </div>
  <div class="panel-footer" style="text-align:right">
  	<a class="glyphicon glyphicon-eye-open" href="#" title="Ver"></a>&nbsp;
    <a class="glyphicon glyphicon-arrow-down" title="Descargar"></a>
  </div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 category">
<br />
<legend style="text-align:center;">Categorias</legend>
<ul>
<?php
if(isset($categorias))
{
	foreach($categorias as $val)
	{
		echo '<li><a href="'.base_url().'producto/'.strtolower($val['nombre_category']).
			 '#only" rel="nofollow">'.$val['nombre_category'].'</a></li>';
	}
}
?>
</ul>
<br/><br/>
<legend style="text-align:center;">Noticias</legend>
<?php include('templates/redes.php');?>
</div>
</div>
</div>
</section>