<br>
<div class="container spacebefore">
    <div class="row">
        <div class="col-xs-12">
        <?php
        if(isset($pedidoinfo))
        {
            ?>
            <p>Se ha envido una copia de este pedido a su correo electronico <b><em>(Asegurese de chequear la bandeja de spam o correo no deseado)</em></b>, junto con los datos para que realice su pago</p>
            <p>Recuerde que tiene 24 horas h&aacute;biles para realizar su pago, de lo contrario no garantizamos la disponibilidad total de su pedido, una vez hecho el pago debe enviar una copia del deposito o transferencia junto con el nro del pedido para realizarle el envio de sus productos</p>
            <?php
        }
        $this->load->view('pedidotable', $pedidoinfo);
        ?>
        </div>
    </div>
    <div class="noprint" style="width:200px; margin:auto; padding:5px;text-align:center;">
    <button class="btn btn-success btn-lg imprimir">
    <i class="fa fa-print"></i>&nbsp;Imprimir
    </button>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
        	<h4 class="modal-title">Felicidades! Se ha creado su pedido con nro <?=str_pad($pedidoinfo['id'], 6, '0', STR_PAD_LEFT)?></h4>
      	</div>
      	<div class="modal-body" align="center">
        	<h5>Recuerde que tiene 3 dias habiles para realizar su pago, de lo contrario no garantizamos la disponibilidad total de su pedido</h5>
        	<h5 style="color:#FF1F00">
            Confirme su pedido al los tel&eacute;fonos <em><?=$contacto[0]['telefono']?></em> o a trav&eacute;s de <em><?=$contacto[0]['email']?></em> indicando el nro del pedido
            </h5>
            
      	</div>
      	<div class="modal-footer">
        	<button type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
      	</div>
    </div>

  </div>
</div>
<script type="text/javascript">
$('#myModal').modal('show')

$('.imprimir').click(function(){
    window.print();
    return false;
});
</script>