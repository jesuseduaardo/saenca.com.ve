<section class="main-section">
    <div class="container">
        <div class="row wow fadeInLeft">
            <br/>
            <div class="col-xs-12">
            <!--<h1 id="nombreempresa">SAENCA C.A.</h1>-->
            </div>
        	<div class="empresa col-xs-12 col-md-8">
                <h2>Distribuidora Saenca Comercial, C.A</h2>
                <p>
                    Empresa liderada por talento joven venezolano colmado de ideas nuevas, mucha energ&iacute;a y entusiasmo con m&aacute;s de 4 
                    a&ntilde;os ofreciendo productos y atenci&oacute;n al p&uacute;blico a trav&eacute;s de los diferentes canales digitales de 
                    compra y venta de el pa&iacute;s logrando desarrollar un excelente servicio de ventas en l&iacute;nea, ofreciendo seguridad, 
                    atenci&oacute;n personalizada, respaldo, inigualable  comunicaci&oacute;n pre y post venta, rapidez en el despacho y envi&oacute;
                     a nivel nacional, con m&aacute;s 4 mil clientes ubicados en diferentes ciudades y estados de Venezuela satisfechos con la atenci&oacute;n 
                     y productos.
                </p>
                <p>
                    En Saenca trabajamos de la mano con importadores, es por ello que podemos ofrecer precios competitivos y excelente ofertas. 
                </p>
            </div>
        <div class="col-xs-12 col-md-4"><img src="img/team.png" class="img-responsive" style="height:auto;" /></div>
        </div>
    </div>
</section>
<section class="main-section gray">
	<div class="container">
    	<div class="row wow fadeInRight">
			<div class="col-md-4 hidden-xs hidden-sm"><img src="img/saencauniform.png" class="img-responsive" style="height:auto;" /></div>
        	<div class="empresa col-xs-12 col-md-8">
                <h2>Historia</h2>
                <p>Saenca.com.ve nace con el objetivo de saciar la demanda de muchos usuarios que requieren de un producto pero no se atreven a 
                realizar un pago en l&iacute;nea motivada en gran parte por la desconfianza, la inseguridad o vendedores que no ofrecen respaldo o garant&iacute;a.</p>
                <p>La familia fundadora de Saenca comenzó este fant&aacute;stico proyecto en el año 2012 usando el canal de ventas en l&iacute;nea m&aacute;s 
                popular del pa&iacute;s para el momento.</p>
                <p>En ese entonces este gran proyecto era solo la versi&oacute;n beta, aun as&iacute; contin&uacute;o impulsado el deseo de emprendimiento 
                atendiendo solamente a clientes ubicados en la ciudad capital. (Caracas) </p>
                <blockquote>“Hoy d&iacute;a atendemos clientes de todas partes de Venezuela”</blockquote>
                <p>A mediados del 2013 se consolida la idea y se crean alianzas comerciales con las empresas de envi&oacute; m&aacute;s populares 
                para extender nuestro alcance a nivel nacional.</p>
            </div>
			<div class="clearfix"></div>
            <div class="empresa col-xs-12 col-md-8 pull-left">
                <p>A pesar de las dificultades en Saenca estamos comprometidos en emplear las estrategias comerciales necesarias  para poder mantener los precios.</p>
                <blockquote>“Los precios publicados en Saenca.com.ve ya incluyen IVA”</blockquote>
                <p>En este 2017 nos sentimos m&aacute;s capacitados y comprometidos a pr&eacute;star el mejor servicio de ventas totalmente en l&iacute;nea 
                ofreciendo entregas directo a su casa u oficina con nuestro personal en la zona centro de Caracas y a trav&eacute;s de las principales 
                empresas de envi&oacute; al resto de ciudades.</p>
                <blockquote>“Saenca.com.ve cuenta con una dirección fiscal para cualquier asunto legal mas no cuenta con una sucursal abierta al p&uacute;blico.”</blockquote>
                <p>No paramos de mejorar y para este 2017 lo seguiremos haciendo, ya son más de 4 mil clientes y siguen aumentando, lo mejor de todo 100% 
                satisfechos. A pesar de las dificultades como las que atraviesa nuestro pa&iacute;s no nos damos por vencidos, seguimos luchando por este 
                sueño y apostando por Venezuela el mejor pa&iacute;s del mundo.</p>
            </div>
        </div>
    </div>
</section>
<section class="main-section">
     <div class="container">
         <div class="row wow fadeInDown">
         	<div class="empresa col-xs-12 col-md-6">
				<div class="empresa col-xs-12">
                    <h2>Misi&oacute;n</h2> 
                    <p>
                        Ofrecer a los venezolanos la oportunidad de comprar diversos productos De electr&oacute;nica y otros, ofreciendo seguridad, 
                        confiabilidad, respaldo y garant&iacute;a, todo desde su computador, laptop, tableta o celular y en la comodidad de su casa u oficina. 
                        Estamos seguros que como empresa lograremos brindar un excelente servicio de entrega en el menor tiempo posible, brindando productos de calidad 
                        a precios competitivos y una excelente atenci&oacute;n en todo momento.
                    </p>
                </div>
                <div class="empresa col-xs-12">
                    <h2>Visi&oacute;n</h2>
                    <p>
                    Posicionarnos como el principal punto de referencia de los venezolanos en sus compras En Línea, transformar la inseguridad que aqueja a muchas personas en sus compras En Línea y transformarlas en confianza, satisfacción y sobre todo seguridad para nuestros clientes. 
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12">
                    <h2>Valores</h2> 
                    <ul>
                        <li>Alta capacidad de trabajo</li>
                        <li>Honestidad</li>
                        <li>Calidad</li>
                        <li>Integridad</li>
                        <li>Compromiso</li>
                        <li>Respeto</li>
                        <li>Eficiencia</li>
                        <li>Responsabilidad</li>
                        <li>Ética</li>
                        <li>Vocación de Servicio</li>
                    </ul>
                </div>
                <div class="empresa col-xs-12">
                    <h2>Filosof&iacute;a</h2> 
                    <p>
                    Involucrarnos con nuestros clientes desde el primer contacto, de manera abierta, proactiva y profunda que nos permita identificar y responder estratégicamente a cualesquiera que sean sus oportunidades de negocio en tiempo y forma.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>