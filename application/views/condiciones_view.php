<div class="container">
	<div class="row">
		<div class="col-xs-12 term" >
        <legend><h2>Condiciones de compra y devoluci&oacute;n de art&iacute;culos</h2></legend>
        <ol>
        	<li>Se entiende que el cliente al realizar una compra o pedido estar&aacute; aceptando nuestros terminos y condiciones.</li>
            <li>Los precios publicados en Saenca.com.ve ya poseen el IVA incluido.</li>
            <li>Al crear un pedido Ud acepta estar en la entera disposici&oacute;n econ&oacute;mica y legal de adquirir el/los producto(s).</li>
			<li>Para realizar sus compras en Saenca.com.ve ud acepta suministrar sus datos personales para la elaboraci&oacute;n de la factura y corroborar que no se trate de menores de edad.</li>
			<li style="font-size:16px;">NO atendemos, vendemos, despachamos a menores de 18 a&ntilde;os.</li>
			<li>Ud da fe que el dinero de la compra es honrado de curso legal y no mal habido.</li> 
			<li>Aun realizando el pedido se mantendr&aacute; en existencia el art&iacute;culo, hasta que el cliente haya realizado el pago en los d&iacute;as correspondientes.</li>
			<li>El cliente tendr&aacute; tres d&iacute;as h&aacute;biles para realizar su pago de lo contrario no garantizamos existencias o precio.</li>
		<li>Aceptamos como medio de pagos transferencias, dep&oacute;sitos (Pr&oacute;ximamente estaremos aceptando tarjetas de cr&eacute;dito).</li>
        <li>Bajo ning&uacute;n concepto se le har&aacute; entrega o env&iacute;o del art&iacute;culo bajo pedido, sin antes realizar el pago.</li>
        <li>El cliente se compromete, para validar su pedido y garantizar precio o existencia hacernos llegar a nuestro correo el n&uacute;mero de Dep&oacute;sito o Transferencia para confirmar su pago en un periodo no mayor a tres d&iacute;as.</li>
        <li>Saenca.com.ve se compromete a embalar correctamente el producto a fin de protegerlo lo mejor posible durante su envi&oacute;.</li>
        <li>Saenca.com.ve no se har&aacute; responsable por perdidas o mal manejo de la mercanc&iacute;a en manos de las empresas de encomiendas.</li>
        <li>La mercanc&iacute;a que requiera ser enviada por alguna empresa de encomiendas con las que trabajamos viajara a riesgo del cliente.</li>
        <li>Para env&iacute;os a nivel nacional trabajamos con Mrw, Domesa, Tealca Zoom e Ipostel.</li>
        <li>La devoluci&oacute;n o devoluciones de uno o varios productos debe hacerse obligatoriamente en las Condiciones originales, entiendase su empaque original con todos los envoltorios y etiquetas correspondientes.</li>
        <li>Es obligatorio para poder hacer uso de la garant&iacute;a ya sea que el producto este defectuoso o equivocado presentar la factura y enviar o entregar el producto en las mismas condiciones, tal cual lo recibe en su empaque original en perfecto estado, es por ello que le sugerimos conservar la factura, abrir y revisar su producto con cuidado y en todo momento dentro del periodo de garant&iacute;a conservar la caja y accesorios en perfecto estado.</li>
        </ol>
        
        </div>
	</div>
</div>