<br/>
<div class="container" style="min-height:600px">
    <div class="row">
        <div class="clearfix" style="padding:50px 0;"></div>
        <a name="opinion" id="info" style="background:none;">&nbsp;</a>
        <div class="col-xs-12 col-md-offset-3 col-sm-8 col-md-6" id="opinion">
        <?php
        if($coment)
        {
			?>
			<h2>Denos su opinion</h2>
        	<h4>Para nosotros su opini&oacute;n es muy importante</h4>
            <form id="comentario" novalidate="novalidate">
            <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Puntuaci&oacute;n</span>
                <div id="stars-default">
                    <input type="hidden" name="value"/>
                </div>
            </div>
            <div class="input-group">
            <div class="contenido" id="basic-addon1">Coloque su comentario</div>
            <?php
			$data = array(
					'name'	=> 'coment',
					'id'    => 'coment',
					'rows'	=> '4',
					'cols'	=> '100',
					'maxlength' => '500',
					'style'	=> 'width:100%'
					);
			echo form_textarea($data);
			?>
            <span id="error"></span>
            </div>
            <?php
            echo form_hidden('nro', $nro);
            echo form_hidden('code', $code);
			$data = array(
					'name' => 'enviar',
					'value'=> 'Enviar',
					'class'=> 'btn btn-lg btn-success'
					);
			echo form_submit($data);
			?>
			</form>
            <?php
        }
        else
        {
            echo '<h2>Disculpe el enlace ha caducado o ya se ha enviado su opinion</h2>';
        }
        ?>
        </div>
    </div>
</div>
<script src="<?=base_url('js/jquery.validate.min.js')?>"></script>
<script>
$(document).ready(function(){
	$("#stars-default").rating();
	$('#comentario').on('submit', function(e){
		e.preventDefault();
		$(this).validate({
			rules: {
				value: { required:true},
				coment: { required: true, minlength: 10, maxlength:500},
			},
			messages: {
				value : "Debe indicar una valoracion.",
				coment: "Debe introducir un comentario entre 10 y 500 caracteres.",
			},
			submitHandler: function(){
				$.ajax({
					type: "POST",
					url:"<?=base_url('home/send_opinion')?>",
					data: $('#comentario').serialize(),
					dataType:"json",
					beforeSend:function(){
						$div = $("<div class='loading'></div>");
						$("#opinion").html($div);
					},
					success: function(data){
						$("#opinion").html('<h1>'+data.response+'</h1>');
					},
					error: function(data){
						$("#opinion").html('<h1>'+data.responseText+'</h1>');
					}
				});
			}
		});
	});
});
</script>
