<br>
<div class="container">
<div class="row">
<div class="col-xs-12 router">
<a href="<?=base_url()?>inicio"><i class="glyphicon glyphicon-home"></i>&nbsp;Inicio</a>&nbsp;/&nbsp;
<?=anchor(base_url().'productos#inventario', 'Productos')?>&nbsp;/&nbsp;<span id="actual"><?=$titulo?></span>
</div>
<div class="clearfix"></div>
<div class="hidden-xs col-md-2 category">
<br />
<legend>Categorias</legend>
<ul>
<?php
if(isset($categorias))
{
	foreach($categorias as $val)
	{
		echo '<li><a href="'.base_url().'productos/'.str_replace(' ', '_', strtolower($val['c_nombre'])).
			 '#only" rel="nofollow">'.$val['c_nombre'].'</a></li>';
	}
}
?>
</ul>
</div>
<div class="col-xs-12 col-md-10">
<div class="row">
<h3><?=$titulo?></h3>
<div class="clearfix"></div>
<?php 
$i=0;
$j=0;
$vuelta=0;
foreach($productos as $val){
	$val['numero']=$j;
	echo '<div class="col-xs-12 col-md-4">';
	$this->load->view('templates/productbox.php', $val);
	echo '</div>';
	$vuelta++;
	if($vuelta==3)
	{
		echo '<div class="clearfix"></div>';
		$vuelta=0;
	}
	$j++;
}
?>
</div>
</div>
</div>
</div>
<div class="clearfix" style="padding:20px 0"></div>
<script type="text/javascript">
function activaenvio(check)
{
	$button = $('#save'+check);
	$button.removeAttr('disabled');
};
</script>
<script type="text/javascript">
$(document).bind('contextmenu', function(e)
{
	return false;
});
</script>