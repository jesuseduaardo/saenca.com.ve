<section class="main-section wow fadeInLeft animated"><!--main-section-start-->
	<div class="container">
    	<h2>Categorias</h2>
    	<h6>Tenemos eso que buscas</h6>
    	<div class="row">
            <div class="categorias-nav">
            <?php
             if(isset($categorias))
             {
				$i=0;
                foreach($categorias as $val)
                {
					if(is_int($i/3))
					{
						echo '<div class="clearfix visible-xs"></div>';
					}
					if(is_int($i/6))
					{
						echo '<div class="clearfix visible-sm visible-md visible-lg"></div>';
					}
                    ?>
                    <div class="col-xs-4 col-sm-2">
                        <a href="<?=base_url().'productos#/'.strtolower(str_replace(' ', '_',$val['c_nombre']))?>#only" rel="nofollow"><?=$val['c_nombre']?></a>
                    </div>
                    <?php
					$i++;
                 }
             }
             ?>
    <!--<div class="fb-like" data-href="http://saenca.com.ve/" data-layout="box_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>-->
            </div>
        </div>
	</div>
</section>
<section class="main-section"><!--main-section-start-->
	<div class="container">
    	<div class="row">
        	<div class="col-sm-12">
                <!--<div class="wow fadeInUp animated">
                <h2>Productos</h2>
                    <h6>Recientes</h6>
                    <div id="theOwlCarousel">
                        <?php foreach($recientes as $val)
                        {
                            $this->load->view('templates/box.php', $val); 
                        }
                        ?>
                    </div>
                </div>-->
                <div class="wow fadeInRight delay-05s animated">
                    <h6>Los m&aacute;s Populares</h6>
                    <div ng-app="app">
                        <?php
                        if(isset($productos))
                        {
                            $i=0;
                            $j=0;
                            foreach($productos as $val)
                            {
                                if(is_int($i/2))
                                {
                                    echo '<div class="clearfix visible-xs"></div>';
                                }
                                if(is_int($i/3))
                                {
                                    echo '<div class="clearfix visible-sm"></div>';
                                }
                                if(is_int($i/4))
                                {
                                    echo '<div class="clearfix visible-md visible-lg"></div>';
                                }
                                $val['numero'] = $j;
                                echo '<div class="col-xs-6 col-sm-4 col-md-3 item">';
                                $this->load->view('templates/productbox.php', $val);
                                echo '</div>';
                                $j++;
                                $i++;
                            }
                        }
                        ?>
                        <div class="clearfix" style="text-align:center;"></div>
                        <div class="text-center">
                            <a href="/productos#/" class="link">Ver Todos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>    
<div class="container">
	<div class="row">
		<div class="col-xs-12" id="paginasweb" style="display:none;"></div>
	</div>
</div>
<?php $this->load->view('templates/pasoscompra.php')?>
<section id="ml"></section>
<script>
 $(document).ready(function(){
	$("#theOwlCarousel").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]

  });
});	
</script>