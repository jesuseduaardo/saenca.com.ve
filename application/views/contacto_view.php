<?php
if(isset($contacto)){
        	foreach($contacto as $values){
				?>
<br/>
    <div class="container">
        <div class="row">
        <div class="col-md-12 router">
        <a href="<?=base_url()?>inicio"><i class="glyphicon glyphicon-home"></i>&nbsp;Inicio</a>&nbsp;/&nbsp;Cont&aacute;ctanos
        </div>
        <?php
				/*echo '<h3>Vis&iacute;tenos</h3>';
				?>
				<div class="col-xs-12 col-md-12">
				<?='<p>'.$values['direccion'].'</p>'?>
				<?php
				if(isset($values['gmaps']))
				{
					$coordenadas = explode(',', $values['gmaps']);
					$latitud = $coordenadas[0];
					$longitud = $coordenadas[1];
					?>
				<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
				<script>	
				function init_map() {
				  var var_location = new google.maps.LatLng(<?=$latitud?>, <?=$longitud?>);
				
				  var var_mapoptions = {
					center: var_location,
					zoom: 17
				  };
				
				  var var_marker = new google.maps.Marker({
					  position: var_location,
					  map: var_map,
					  title:"<?=$titulo?>"});
				
				  var var_map = new google.maps.Map(document.getElementById("map-container"),
					  var_mapoptions);
				
				  var_marker.setMap(var_map);	
				
				}
				
				google.maps.event.addDomListener(window, 'load', init_map);
				</script>
				<div class="container info mapa" id="map-container">
				</div>
				<?php
				}
				?>
				</div>*/
        ?>
        <div class="hidden-md hidden-lg" style="padding:50px 0;"></div>
        <div class="clearfix"></div>
        <a name="telefonos" id="info" style="background:none;">&nbsp;</a>
        </div>
    </div>
    <section style="background:url(https://image.ibb.co/kWtfM5/contact_zpszcytpeuc.jpg) no-repeat right;padding: 43px 0px;">
    	<div class="container">
        	<div class="col-xs-12 col-md-6">
    <h3>Cont&aacute;ctenos</h3>
    <?php
    echo '<ul class="contact-medi">';
    if(isset($values['telefono']))
    {
        echo '<li><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;';
        if (!strpos($values['telefono'], ','))
        {
            echo telefono($values['telefono']);
        }
        else
        {
            $telefonos = explode(',',$values['telefono']);
            for($i=0;$i<count($telefonos);$i++)
            {
                if($i > 0)
                {
                    echo ' - '.telefono($telefonos[$i]);
                }
                else
                {
                    echo telefono($telefonos[$i]);
                }
            }
        }
        echo '</li>';
    }
    if(isset($values['email']) && $values['email']!='')
    {
        $mails = explode(',', $values['email']);
        echo '<li><span class="glyphicon glyphicon-comment"></span>&nbsp;';
        for($i=0;$i < count($mails);$i++)
        {	
            if($i==0)
            {
                $destino = $mails[$i];
            }
            if((1+$i) == count($mails))
            {
                echo '<a href="mailto:'.$mails[$i].'">'.$mails[$i].'</a>';
            }
            else
            {
                echo '<a href="mailto:'.$mails[$i].'">'.$mails[$i].'</a> - '; 
            }
        }
        echo '</li>';
    }
    ?>
    <?php
    if(isset($values['facebook'])&& $values['facebook']!='')
    {
        echo '<li>';
        echo '<i style="font-size:20px;" class="fa fa-facebook"></i>&nbsp;<a href="https://www.facebook.com/'.$values['facebook'].'" target="blank">'.$values['facebook'].'</a>';
        echo '</li>';
    } 
    if(isset($values['twitter'])&& $values['twitter']!='')
    {
        echo '<li>';
        echo '<i style="font-size:20px;" class="fa fa-twitter"></i>&nbsp;<a href="https://www.twitter.com/'.$values['twitter'].'" target="blank">'.$values['twitter'].'</a>';
        echo '</li>';
    }
    if(isset($values['whatsapp'])&& $values['whatsapp']!='')
    {
        echo '<li>';
        echo '<strong>Whatsapp:</strong>&nbsp;'.$values['whatsapp'];
        echo '</li>';
    }
    if(isset($values['bbm'])&& $values['bbm']!='')
    {
        echo '<li>';
        echo '<strong>BBM Pin:</strong>&nbsp;'.$values['bbm'];
        echo '</li>';
    }
    if(isset($values['youtube'])&& $values['youtube']!='')
    {
        echo '<li>';
        echo '<i style="font-size:20px;" class="fa fa-youtube-play"></i>&nbsp;<a href="https://www.youtube.com/'.$values['youtube'].'" target="blank">'.$values['youtube'].'</a>';
        echo '</li>';
    }
    if(isset($values['googleplus'])&& $values['googleplus']!='')
    {
        echo '<li>';
        echo '<i style="font-size:20px;" class="fa fa-google-plus"></i>&nbsp;<a href="https://www.googleplus.com/'.$values['googleplus'].'" target="blank">'.$values['googleplus'].'</a>';
        echo '</li>';
    }
    if(isset($values['instagram'])&& $values['instagram']!='')
    {
        echo '<li>';
        echo '<i style="font-size:20px;" class="fa fa-instagram"></i>&nbsp;<a href="https://www.instagram.com/'.$values['instagram'].'" target="blank">'.$values['instagram'].'</a>';
        echo '</li>';
    }
    ?>
    <?php
    if(isset($values['website']))
    {?>
    <li>
    <?php
        echo '<strong>Sitio Web:</strong>&nbsp;<a href="http://www.'.$values['website'].'">www.'.$values['website'].'</a>';
    ?>
    </li>
    <?php } ?>
    </ul>
    <br/><br/>
    </div>
   		</div>
    </section>
    <section style="background-color:#f3f3f3;">
        <div class="container" id="consulta">
            <div class="form col-xs-12" align="center">	
            <h4>Formulario de Contacto</h4>
            <em>Escribanos para consultas, presupuestos e inquietudes</em>
            <!--Formulario de Contacto-->
            <form class="cf" id="contact">
              <div class="half left cf">
                <input type="text" name="nombre" id="input-name" placeholder="Nombre" required>
                <input type="email" name="email" id="input-email" placeholder="Email" required>
                <input type="text" name="asunto" id="input-subject" placeholder="Asunto" required>
              </div>
              <div class="half right cf">
                <textarea name="mensaje" type="text" id="input-message" placeholder="Mensaje" required></textarea>
              </div>
              <div class="clearfix" style="clear:both"></div>
              <div class="g-recaptcha" data-callback="active" data-sitekey="6LcOvhEUAAAAAM3CfzCVEZ_dS_NUqKRH2ldpl0o1"></div>
              <input type="submit" value="Enviar" disabled='disabled' id="input-submit">
            </form>	
            </div>
        <!--/Formulario de Contacto-->
        </div>
    </section>
 <?php
 	}
		}
 ?>
<script>
$(document).ready(function(e) {
	active = function(){
		$('#input-submit').removeAttr('disabled');
	}
	$('#contact').on('submit', function(e)
	{
		e.preventDefault();
		  $.ajax({
			  type:"POST",
		        url:"send/email",
		        data: $('#contact').serialize(),
				  beforeSend:function(){
					  $div = $("<div class='loading'></div>");
						  $("#consulta").append($div);
					  },
		          success: function(data){
							$div = $("<div id='resp' align='center'><i class='fa fa-check'></i><br><h4>"+data.response+"</h4></div>");
		                    $("#consulta").html($div);
		                },
				  error: function(data){
						  $("#consulta").html(data.response);
					  }
			});
	});
});
</script>