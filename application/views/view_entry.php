<?php 
require_once('templates/tiempo.php');
?>
<a name="articulo" id="info">.</a>
<br>
<br>
<br>
<br>
<br>
<div class="container">
<div class="row">
<div class="col-xs-12 router">
<?=anchor(base_url(),'Inicio')?>&nbsp;/&nbsp;<?=anchor(base_url().'blog#enterate', 'Blog')?>&nbsp;/&nbsp;<span id="actual"><?=$titulo?></span>
</div>
<div class="col-xs-12 col-md-9">
<?php if(!empty($entries)):?>
<?php foreach($entries as $entry):
$id = $entry['id']; 
?>
<div class="col-xs-12">
<h1><?=$entry['titulo']?></h1>
</div>
<?php
if(isset($entry['imgprincipal']))
{
	echo '<div class="col-xs-12">';
	echo '<img src="'.$entry['imgprincipal'].'">';
	echo '</div>';
}
?>
<div class="col-xs-12">
<p><em><?=$entry['cintro']?></em></p>
<p><?=$entry['contenido']?></p>
</div>
<?php
if(isset($entry['imgsecundaria']))
{
	echo '<div class="col-xs-12 col-md-6">';
	echo '<img src="'.$entry['imgprincipal'].'">';
	echo '</div>';
}
?>
<div class="clearfix"></div>
<?php
if(isset($entry['enlace']))
{
	echo '<div class="col-xs-12">';
	$param = array('http://', 'https://');
	$cleanurl = str_replace($param, '', $entry['enlace']);
	echo '<p><strong>Enlace:</strong> <a href="'.$entry['enlace'].'" rel="nofollow" target="_blank">'.$cleanurl.'</a></p>';
	echo '</div>';
}
?>
<?php 
$ago = damefecha($entry['fecha']);
$fecha = date_create($entry['fecha']);
?>

<div class="col-xs-12 col-md-9">
<?php $this->load->view('templates/contador', $visitas)?>
<p class="datecreate">Creado el <?=date_format($fecha, 'd-m-Y') ?> - Hace <?=$ago?></p>
</div>
<?php endforeach; ?>
<?php endif; ?> 
</div>
<div class="col-xs-12 col-md-3" id="relacionadas">
<legend id="relacionadas">Entradas Relacionadas</legend>
<?php 
if(!empty($relacionadas)): 
	foreach($relacionadas as $val):
		foreach($val as $valor):
		if($valor['id']!=$id)
		 {
?>
<h4><?=anchor(base_url().'blog/articulo/'.$valor['permalink'].'#articulo',$valor['titulo'])?> </h4>
<p class="datecreate">Hace <?=damefecha($valor['fecha'])?></p>	
<?php 
	$this->load->model('site_model','model');
	$visitas = array(
					'visitas' => $this->model->visto($valor['id'])
					);
	$this->load->view('templates/contador', $visitas);
?>
<hr/>
	<?php 
		 }
	endforeach; ?>
<?php endforeach; ?>
<?php else : ?>
<h1>No entries</h1>
<?php endif; ?> 

</div>
</div>
</div>