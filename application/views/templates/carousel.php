<?php if(isset($slider)){?>
<section class="main-section" id="service">
  	<!-- Carousel-->
    <div class="slider-wrapper" style="display:none">
        <div class="container">
            <div class="slider">
            <div class="fs_loader"></div>
				<?php
                foreach($slider as $values)
                {
                    if(isset($values['nombre'])&&($values['nombre']!=''))
                    {
                        $img = explode(',',$values['nombre']);
                        if(count($img) >= 1)
                        {
                            $left = 'left';
                            $right = 'right';
                            $delay=200;
                            ?>	
                            <div class="slide">
                            <?php
                            foreach($img as $pic)
                            {
                                
                                if($pic != '')
                                {
                                    ?>
                                    
                                    <img src="<?=$pic?>" 
                                         class="img-responsive" 
                                         data-position="0, 0" 
                                         data-in="<?=$left?>" 
                                         data-out = "<?=$right?>"
                                         data-delay="<?=$delay?>" />
                                    <?php
                                    $delay+=300;
                                    $left = 'right';
                                    $right = 'left';
                                }
                            }
                            ?>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
	<!-- /.carousel -->
</section>
<?php  }?>
<script>
$(document).ready(function() {
    $(".slider-wrapper").delay(2500).fadeIn('slow', function(){
		$('.slider').fractionSlider({
			'fullWidth': 			true,
			'controls': 			false, 
			'pager': 				false,
			'responsive': 			true,
			'dimensions': 			"1024, 500",
			'increase': 			false,
			'pauseOnHover': 		true
			});
		});
});
</script>