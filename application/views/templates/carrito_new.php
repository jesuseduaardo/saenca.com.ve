<div id="productlist">
    <div class="col-xs-12 no-padding">
<?php
//si el carrito contiene productos los mostramos
if ($carrito = $this->cart->contents()) 
{
	?>

    	<ul style="padding-left: 0;">
		<?php
		foreach($carrito as $item){
			?>
            <li class="clearfix">
            <div class="col-xs-11">
            <div class="productimg" style="background-image:url(<?=$item['options']['img']?>)"></div>
              <h4><?= ucfirst($item['name']) ?></h4>
              <?php $nombres = array('nombre' => ucfirst($item['name']));?>
              <span class="item-price">Bs <?=number_format($item['price'], 2, ',', '.');?></span>
              <span class="quantity">Cantidad: <?=$item['qty']?></span>
              </div>
              <div class="col-xs-1">
              <span class="delete">
              		<a href="javascript:removeitem('<?=$item['rowid']?>')" id="<?=$item['rowid']?>"><b class="delete">X</b></a>
              </span>
              </div>
            </li>
			<?php
		}
		?>
        </ul>
       <div class="col-xs-7">
       <?php
       $CI =& get_instance();
	   $total = $CI->checkout();
	   ?>
       	Total Bs: <strong><?= number_format($total[2], 2, ',', '.') ?></strong>
        </div>
        <div class="col-xs-5">
        	<a class="vaciar" href="javascript:enviadata('<?=base_url()?>home/eliminarCarrito')">Vaciar</a>
        </div>
        <div class="clearfix"></div>
        <div class="text-center">
        	<a style="margin:0px;" href="<?=base_url()?>crear#pedido" class="link crearpedido">Crear Pedido</a>
        </div>
        </div>
    <?php
}
else
{
	?>
	<h4>No hay articulos en el carrito</h4>
	<?php
}
?>
<!--fin de nuestro carrito-->
	</div>
</div>