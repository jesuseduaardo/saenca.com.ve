<div class="productbox">
	<?php
    switch($i_clasificacion)
    {
        case 4:
        $label = 'oferta';
        break;
        case 2:
        $label = 'recomendado';
        break;
        case 3:
        $label = 'masvendido';
        break;
    }
    if(isset($label))
    {?>
        <div class="product-label <?=$label?>"></div>
    <?php }
    if(isset($i_condicion) && $i_condicion > 0)
    {?>
        <div class="usado-label"></div>
    <?php }
    ?>
    	<form id="form<?=$i_id?>" class="boxform">
        <?=$i_cantidad > 0 ? '' : '<div class="agotado"></div>'?>
            <div class="gallery product_img">
            <a href="<?=base_url()?>productos#/<?=strtolower(str_replace(' ', '_', $c_nombre)).'/'.$i_id.'/'.strtolower(str_replace(' ', '_', $i_nombre))?>">
            <img id="img<?=$i_id?>" class="img-responsive imglink" title="Ver Detalles" src="<?=$p_1?>" 
            	onError="this.onError=null;this.src='/img/no-product.jpg';" alt="<?=$i_nombre?> - Cod: <?=$i_cod?>" />
            </a>
            </div>
            <div class="propertiesgroup">
                <div class="col-xs-12">
                    <p class="productname"><b>Cod. <?=$i_cod?></b><br/><?=$i_nombre?></p>
                </div>
            <div class="clearfix"></div>
            <div class="col-xs-12" style="padding:20px 0 5px;">
            <?php
            if(isset($i_precio))
            {
                if(strstr($i_precio, '-'))
                {
                    $precios = explode('-', $i_precio);
					foreach($precios as $key=>$val)
					{
						$number = explode(',', number_format(sprintf("%01.2f",$precios[$key]),"2", ",", "."));
						if($key>0)
						{
							echo '<div class="col-xs-6 precio"><span class="moneda">Bs </span><span class="ahora">';
						}
						else
						{
							echo '<div class="col-xs-6 precio pantes"><span class="moneda">Bs </span><span class="antes">';
						}
						echo $number[0].'<sup>'.$number[1].'</sup>';
						echo '</span></div>';
					}
                }
                else
                {
					$number = explode(',', number_format(sprintf("%01.2f",$i_precio),"2", ",", "."));
                    echo '<div class="col-xs-12 precio"><span class="moneda">Bs </span><span class="ahora">';
                    echo $number[0].'<sup>'.$number[1].'</sup>';
                    echo '</span></div>';
                }
            }
            ?>
            </div>
            <?php
            if(isset($i_costoenvio) && $i_costoenvio > 0)
            {?>
                <div class="col-xs-12 genvio">
                Envio Gratis
                </div>
            <?php } ?>
            <div class="col-xs-12">
            <?php
			if(SHOPCART > 0)
			{
			?>
            	<div class="input-group">
                    <select class="form-control cantidad" name="cantidad" id="cantidad<?=$i_id?>" data-code="<?=$i_id?>" onchange="removedisable(this)" tabindex="-1" aria-hidden="true">
                    <?php
                    for($i=0; $i <= $i_cantidad; $i++)
                    {
                        if($i==0)
                        {
                            echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
                        }
                        else
                        {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                    }
                    ?>
                    </select>
                    <div class="input-group-btn">
                        <?php
                        $button = array(
                                        "name"		=>	"action",
                                        //"value"   =>	"Agregar al Carrito",
                                        "content"	=>	"Al carrito <i style='font-size:28px' class='fa fa-shopping-cart'></i>",
                                        "class" 	=>	"btn btn-success cartbutton",
                                        "id"		=>	"save".$i_id,
                                        "disabled"	=>	"disabled",
										"data-code"	=>  $i_id,
                                        "onclick"	=>	"addtocart(this)"
                                       // "onclick"	=>	"enviaform('#form".$numero."')"
                                        );
                        ?>
                        <?= form_button($button)?>
                        <?= form_hidden('id', $i_id) ?>
                    </div>
                </div>
            <?php
			}
			else
			{
			?>
            	<button class="btn btn-success">COMPRAR</button>    
            <?php
			}
			?>
             </div>
            <div class="col-xs-7">
            <span class="valor"></span>
            </div>
        </div>
		</form>
</div>