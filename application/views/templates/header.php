<?php
function telefono($telefono)
{
	$numero = str_split(trim($telefono));
	for($i = 0;$i < count($numero);$i++)
	{
		switch($i)
		{
			case 0:
			$nuevo_numero='('.$numero[$i];
			break;
			case 3:
			$nuevo_numero.=$numero[$i].')';
			break;
			case 6:
			$nuevo_numero.=$numero[$i].'-';
			break;
			case 8:
			$nuevo_numero.=$numero[$i].'-';
			break;
			case 10:
			$nuevo_numero.=$numero[$i];
			break;
			default:
			$nuevo_numero.=$numero[$i];
			break;
		}
	}
	return $nuevo_numero;
}
?>
<?=doctype($doctype)?>
<html ng-app="app" lang="<?=$lang?>" ng-controller="MainController">
<head>
<meta property="fb:app_id" content="302153396833264" />
<?php
if($pagina == 'productos')
{?>
<meta property="og:url" content="<?= 'http://'.DOMAIN?>" />
<meta property="og:title" content="{{ $route.current.scope.title }}" />
<meta property="og:image" content="{{ $route.current.scope.items.producto[0].p_1 }}" />
<meta property="og:site_name" content="Saenca" />
<meta property="og:type" content="website" />
<title ng-bind="$route.current.scope.title"></title>
<?php
}else{
?>
<?=meta($meta)?>
<title><?=ucfirst($title)?></title>
<?php
}
foreach($link as $value)
{
	echo $value;
}
?>

<script src="<?=JQUERY?>"></script>
<script src="<?=JBOOTSTRAP?>"></script>
<script src="<?=base_url('js/jquery.fractionslider.min.js')?>"></script>
<script src="<?=base_url('js/ajax.js')?>"></script>
<?php
if($pagina == 'productos')
{?>
	<script src="<?=base_url('js/angular/angular.min.js')?>"></script>
    <script src="<?=base_url('js/angular/angular-route.min.js')?>"></script>
    <script src="<?=base_url('js/angular/angular-resource.min.js')?>"></script>
    <script src="<?=base_url('js/angular/angular-cookies.min.js')?>"></script>
    <script src="<?=base_url('js/angular/angular-owl-carousel-2.js')?>"></script> 
<?php 
}
if ($this->uri->segment(1) == 'inicio')
{
	?>
	<script>
		jQuery(window).load(function(){
			$('.autoplay').slick({
			  slidesToShow: 5,
			  slidesToScroll: 1,
			  autoplay: true,
			  autoplaySpeed: 2000,
			  arrows:false
			});
		});
	</script>
<?php
}
?>
<script>
jQuery(window).load(function(){
	$('.main-nav li a').bind('click',function(event){
		  var $anchor = $(this);
		  
		  $('html, body').stop().animate({
			  scrollTop: $($anchor.attr('href')).offset().top - 102
		  }, 1500,'easeInOutExpo');
		  /*
		  if you don't want to use the easing effects:
		  $('html, body').stop().animate({
			  scrollTop: $($anchor.attr('href')).offset().top
		  }, 1000);
		  */
		  event.preventDefault();
		});
});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div id="perspective" class="perspective effect-airbnb">
<div class="contenedor">
<div class="wrappp">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-68249915-1', 'auto');
  ga('send', 'pageview');
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=302153396833264";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<header><!--header-start-->
<!--main-nav-start-->
<nav class="main-nav-outer" id="test">
	<div class="container">
        <ul class="main-nav">
        	<li>
            	<span class="small-logo hidden-xs">
            	<a href="/inicio"><?=LOGO?></a>
                </span>
            </li>
        	<li><a href="<?=base_url('inicio')?>">Inicio</a></li>
            <li><a href="<?=base_url('nosotros')?>">Empresa</a></li>
            <li><a href="https://saenca.blogspot.com/">Blog</a></li>
             <li><a href="<?=base_url('productos')?>#/">Productos</a></li>
            <li><a href="https://www.facebook.com/pg/saencaenlinea/reviews/?ref=page_internal">Opiniones</a></li>
            <li><a href="<?=base_url('contacto')?>">Contacto</a></li>
            <?php
            if(SHOPCART > 0)
			{
			?>
            <li>
            	<span id="shop-cart">
            	<?php
				if ($carrito = $this->cart->contents()) 
				{
					$numero=count($this->cart->contents());
					echo '<span id="numero">'.$numero.'</span>';
				}else{
					echo '<span id="numero">0</span>';
				}
				?>
                <div class="dialog" style="display:none"></div>
            	</span>
            </li>
            <?php
			}
			?>
        </ul>
        <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
    </div>
</nav>
<!--main-nav-end-->
</header><!--header-end-->