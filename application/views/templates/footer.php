<footer class="footer contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">    
            	<ul>
                	<li><a href="<?=base_url('inicio')?>">Inicio</a></li>
                    <li><a href="<?=base_url('nosotros')?>">Empresa</a></li>
                	<li><a href="<?=base_url('productos')?>" title="Revisa nuestro inventario de Productos">Productos</a></li>
                	<li><a href="<?=base_url('servicios')?>" title="Descubre todos los servicios que tenemos para ti">Servicios</a></li>
                	<li><a href="<?=base_url('condiciones')?>" title="Condiciones de compra y devoluci&oacute;n de art&iacute;culos">Terminos y condiciones</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-9">
            	<div class="col-sm-7">
                <?php
                    if(isset($contacto)){
                    ?>
                    <br>
                    <?php
                        foreach($contacto as $values){
                            if(isset($values['telefono']))
                            {
								?>
                                <div class="contact-info-box phone clearfix">
                                    <?php
                                	if (!strpos($values['telefono'], ','))
                                	{
										echo '<h3><i class="fa-phone"></i>Tel&eacute;fono</h3>';
										echo '<span>';
                                    	echo telefono($values['telefono']);
                                	}
                                	else
                                	{
                                    	$telefono = explode(',',$values['telefono']);
										echo '<h3><i class="fa-phone"></i>Tel&eacute;fono(s)</h3>';
										echo '<span>';
                                    	for($i=0;$i<count($telefono);$i++)
                                    	{
                                        	if($i > 0)
                                        	{
                                            	echo ' / '.telefono($telefono[$i]);
                                        	}
                                        	else
                                        	{
                                            	echo telefono($telefono[$i]);
                                        	}
                                    	}
                                	}
									?>
                                    </span>
                            	</div>
                                <?php
                            }
                            if(isset($values['email']) && $values['email']!='')
                            {
								?>
                                <div class="contact-info-box email clearfix">
                                    <h3><i class="fa-pencil"></i>email:</h3>
                                    <span>
                                <?php
                                $mails = explode(',', $values['email']);
                                for($i=0;$i < count($mails);$i++)
                                {	
                                    if($i==0)
                                    {
                                        $destino = $mails[$i];
                                    }
                                    if((1+$i) == count($mails))
                                    {
                                        echo '<a href="mailto:'.$mails[$i].'">'.$mails[$i].'</a>';
                                    }
                                    else
                                    {
                                        echo '<a href="mailto:'.$mails[$i].'">'.$mails[$i].'</a>, '; 
                                    }
                                }
							?>
                            		</span>
                            	</div>
                            <?php	
                            }
							if(isset($values['direccion']) && $values['direccion']!='')
							{
							?>
							<div class="contact-info-box address clearfix">
                                <h3><i class=" icon-map-marker"></i>Direcci&oacute;n:</h3>
                                <span><?=$values['direccion']?></span>
                            </div>	
							<?php
                            }
						}
					}
                    ?>
                    <div class="contact-info-box hours clearfix">
                        <h3><i class="fa-clock-o"></i>Horario:</h3>
                        <span><strong>Lunes - Viernes:</strong> 8:00am - 4:00pm</span>
                    </div>
               	</div>
                <div class="col-sm-5">
                	<!--<div class="form">
                    <form id="message">
                    	<input class="input-text" type="text" id="nombre" placeholder="Tu nombre *" maxlength="50" required>
                        <input class="input-text" type="email" id="email" value="Tu E-mail *" placeholder="Tu email *" maxlength="50" required>
                        <textarea id="mensaje" class="input-text text-area" cols="0" rows="0" placeholder="Tu Mensaje *" maxlength="300" required></textarea>
                        <div class="input-group" style="margin-bottom:15px;">
                        <span class="input-group-addon captcha" id="image_captha">
                        	<?=$captcha['image']?>
                        </span>
                        <input type="text" id="captcha" name="captcha" maxlength="5" class="form-control" required="required" placeholder="captcha" autocomplete="off" />
                        </div>
                        <?=form_error('captcha')?>
        				<!--End--Info-Captcha
                        <input class="input-btn" style="height:28px;width:100%;" type="submit" value="Enviar Mensaje">
                        </form>
                    </div>-->	
                </div>
            </div>
            <div class="col-xs-12" style="text-align:center;">
            	<ul class="social-link">
                        <li class="twitter"><a href="#"><i class="fa-twitter"></i></a></li>
                        <li class="facebook"><a href="#"><i class="fa-facebook"></i></a></li>
                        <li class="gplus"><a href="#"><i class="fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<section id="copyrigth">
    <div class="container">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
        	<div class="emblem"></div>
        	&copy;<?=RAZONSOCIAL?> - <?=date('Y');?>
        </div>
        <div class="col-xs-12 col-md-4">
        	<p>Creado por <span class="creator"></span><p>
        </div>
    </div>
</section>
<div id="up" style="display:none"></div>
        </div><!-- wrappp -->
    </div><!-- /container -->
    <?php
		if(isset($categorias) && $pagina == 'productos')
		{
		?>
        <div class="category-label" id="showMenu">
            Categorias
        </div>
        <nav class="outer-nav left vertical" id="menu-cat">
        <?php
			foreach($categorias as $val)
			{
			echo '<a href="#/'.str_replace(' ', '_', strtolower($val['c_nombre'])).'" rel="nofollow" class="ctgry">'.$val['c_nombre'].'</a>';
			}
			?>
    	</nav>
           <?php
		}
	?>
</div><!-- /perspective -->
<!-- Modal -->

<div id="comprado" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">¡Felicitaciones! ¡Ya es tuyo!</h4>
      </div>
      <div class="modal-body">
      	<h5>Sigue estos pasos para cordinar el pago y la entrega del Producto</h5>
        <ol>
        	<li><h4>Cont&aacute;ctanos a traves de <strong>saenca.ventas@gmail.com</strong> o por el <strong>tel&eacute;fono (0412)902-79-51</strong></h4> y as&iacute; confirmar la disponibilidad del producto y el tiempo de entrega.</li>
			<li><h4>Deposita o transfiere a cualquiera de las siguientes cuentas:</h4>
			<ul>
				<li><img src="<?=base_url('img/bnc.jpg')?>" alt="Banco Nacional de Credito"/>
                	<div class="visible-xs clearfix"></div>
                     Cta Corriente: 0191 0142 8121 00045338 a nombre de <strong>Distribuidora Saenca Comercial</strong> RIF: J-407695320</li>
				<li><img src="<?=base_url('img/banesco.jpg')?>" alt="Banesco"/>
                	<div class="visible-xs clearfix"></div>
                    Cta Corriente: 0134 0866 1686 61014750 a nombre de <strong>Saul Castillo</strong> C.I: V-17929180</li>
				<li><img src="<?=base_url('img/bdv.jpg')?>" alt="Banco de Venezuela"/>
                	<div class="visible-xs clearfix"></div>
                     Cta Corriente: 0102 0221 3100 00516332 a nombre de <strong>Saul Castillo</strong> C.I: V-17929180</li>
			</ul>
			</li>
			<li><h4>Envianos el numero de deposito/transferencia</h4> por cualquiera de nuestros <a href="<?=base_url('contacto')?>">medios de contacto</a></li>
            <li><h4>Recibe el producto</h4> En la comodidad de tu casa o acuerda recogerlo en nuestra oficina</li>
		</ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
<script src="<?=base_url('js/jquery.touchSwipe.min.js')?>"></script>
<!--<script src="<?=base_url()?>js/jquery.velocity.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js"></script>
<script src="<?=base_url('js/jquery-scrolltofixed.js')?>"></script>
<script src="<?=base_url('js/jquery.easing.1.3.js')?>"></script>
<script src="<?=base_url('js/jquery.isotope.js')?>"></script>
<script src="<?=base_url('js/wow.js')?>"></script>
<script src="<?=base_url('js/classie.js')?>"></script>
<script src="<?=base_url('js/index.js')?>"></script>
<script src="<?=base_url('js/modernizr.custom.25376.js')?>"></script>
<script src="<?=base_url('js/owl.carousel.min.js')?>"></script>
<script src="<?=base_url('js/jquery.validate.min.js')?>"></script>
<script>
	if (screen.width < 760)
	{
		$('.navbar').css('height', screen.height);
		$('.navbar-brand').css('margin-top', '25%');
		$('#shop-cart').css('right', '-1%');
		$('#shop-cart').css('top', screen.height);
	}
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100
      }
    );
    wow.init();

	function removepedido(element){
		id = $(element).attr('data-code');
		removeitem(id);
		document.location.reload();
		}
	function removeitem(id){
		var cart = $('#shop-cart');
		$.ajax({
			url: '<?=base_url()?>home/eliminarProducto/'+id,
			success:function(response){
				$('#numero').html(response);
				$('.dialog').load("<?=base_url()?>home/shopcart");
				},
			error : function() {
				alert('Disculpe, existió un problema, Recargue la pagina');
			}
		});
	}
$(document).ready(function(e) {
	
        $('#test').scrollToFixed();
		
        $('.res-nav_click').click(function(){
            $('.main-nav').slideToggle();
            return false; 
        });
	
		$('#message').on('submit', function(e){
			e.preventDefault();
			$("#message").validate({
				rules: {
					nombre: { required: true, minlength: 3},
					email: { required: true, minlength: 15},
					mensaje: { required: true, minlength: 200},
					captcha: { required: true, minlength: 5},
				},
				messages: {
					nombre: "Debe introducir su nombre o razon social.",
					email : "Debe introducir un email válido.",
					mensaje : "Debe un mensaje valido.",
					captcha : "Debe completar el captcha para procesar su informacion.",
				},
			submitHandler: function(form){
				$.ajax({
					url:'./send_mail',
					type: 'POST',
					data: {	nombre : $('#nombre').val(),
							email : $('#email').val(),
							mensaje : $('#mensaje').val(),
							captha : $('#captcha').val()
							},
					dataType : 'json',
					beforeSend:function(){
						$loading = '<div class="loading"></div>';
						$('.form').append($loading);
					},
					success:function(data){
						$('.loading').fadeOut("slow");
						$('.form').html(data)
					},
					error:function(){
						
						}
				});
			}
		});
	});
	
    $('#shop-cart').on('click', function(e){
	   e.stopPropagation();
	  if ($(this).hasClass('active'))
	  {
		$('.dialog').fadeOut(200);
		$(this).removeClass('active');
	  } else { 
		$('.dialog').delay(300).fadeIn(200);
		$('.dialog').load("<?=base_url('shopcart')?>");
	  }
	});

	function closeMenu(){
	  $('.dialog').fadeOut(200);
	  $('#shop-cart').removeClass('active');  
	}

	$(document.body).click( function(e) {
		 closeMenu();
	});
	
	$(".dialog").click( function(e) {
		e.stopPropagation();
	});
});

$('.ctgry').on('click', function(){
	//e.preventDefault();
	var docElem = window.document.documentElement,
		// support transitions
		support = Modernizr.csstransitions,
		// transition end event name
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		docscroll = 0;
		perspectiveWrapper = document.getElementById( 'perspective' );
	if( classie.has( perspectiveWrapper, 'animate') ) {
		var onEndTransFn = function( ev ) {
			if( support && ( ev.target.className !== 'contenedor' || ev.propertyName.indexOf( 'transform' ) == -1 ) ) return;
			this.removeEventListener( transEndEventName, onEndTransFn );
			classie.remove( perspectiveWrapper, 'modalview' );
			// mac chrome issue:
			document.body.scrollTop = document.documentElement.scrollTop = docscroll;
		};
		if( support ) {
			perspectiveWrapper.addEventListener( transEndEventName, onEndTransFn );
		}
		else {
			onEndTransFn.call();
		}
		classie.remove( perspectiveWrapper, 'animate' );
	}
});
</script>
<?php
if(isset($jquery))
{
	foreach($jquery as $val)
	{
		if($val!='')
		{
			echo '<script type="text/javascript" src="'.$val.'"></script>';
		}
	}
}
?>
<?php 
if(isset($categorias) && $pagina == 'productos')
{?>
	<script src="<?=base_url('js/owl.carousel2.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('js/angular/app.js')?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js" onError="<?=base_url('js/angular/ui-bootstrap-tpls.min.js')?>"></script>
<?php } ?>
<script type="text/javascript" src="<?=JQUERYUI?>"></script> 
<script type="text/javascript" src="<?=base_url('js/animatescroll.js')?>"></script>
<!--Navidad ON
<script type="text/javascript" src="js/snoweffect.js"></script>
<script>
if($(window).width() > 1024){
	$.fn.snow({newOn: 500 });
}
</script>-->
</body>
</html>