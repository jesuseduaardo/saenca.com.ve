<section class="main-section gray"><!--main-section-start-->
    <div class="container">
        <div class="row wow fadeInUp delay-03s animated">
            <h2>Comprar con nosotros</h2>
            <h6>Es muy f&aacute;cil y sencillo</h6>
            <div class="col-xs-12 col-md-3 info">
                <div class="col-xs-2 step">1</div>
                <span class="col-xs-10 step-instruction">
                    <img src="<?=base_url()?>img/mail-icon.png" class="img-responsive" alt=""/>
                </span>
                <h3>Cree su pedido</h3>
                <p>Elija los productos que desee y cree su pedido on-line</p>
            </div>
            <div class="col-xs-12 col-md-3 info">
                <div class="col-xs-2 step">2</div>
                <span class="col-xs-10 step-instruction">
                    <img src="<?=base_url()?>img/payment-icon.png" class="img-responsive" alt=""/>
                </span>
                <h3>Realice el pago</h3>
                <p>Una vez realizado el pedido recibira en su correo los datos para realizar su pago v&iacute;a deposito o transferencia</p>
            </div>
            <div class="col-xs-12 col-md-3 info">
                <div class="col-xs-2 step">3</div>
                <span class="col-xs-10 step-instruction">
                	<img src="<?=base_url()?>img/email-icon.png" class="img-responsive" alt=""/> 
                </span>
                <h3>Informe el pago</h3>
                <p>Envienos al correo electronico <a href="mailto:saenca.ventas@gmail.com">saenca.ventas@gmail.com</a> la informacion de el pago asi como el numero de su pedido</p>
            </div>
            <div class="col-xs-12 col-md-3 info">
                <div class="col-xs-2 step">4</div>
                <span class="col-xs-10 step-instruction">
                	<img src="<?=base_url()?>img/truck-icon.png" class="img-responsive" alt=""/>
                </span>
                <h3>Reciba su pedido</h3>
                <p>En la comodidad de su casa u oficina. Tambien puede retirarlo en nuestras oficinas.</p>
            </div>
        </div>
    </div>
</section>