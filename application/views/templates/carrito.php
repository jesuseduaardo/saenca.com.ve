<div class="container">
<?php
//si el carrito contiene productos los mostramos
if ($carrito = $this->cart->contents()) 
{
	?>
    <div id="contenidoCarrito">
		<table>
			<tr>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Cant.</th>
                <th></th>
			</tr>
		<?php
		foreach ($carrito as $item) {
			?>
			<tr>
				<td><p style="font-size:12px;"><?= ucfirst($item['name']) ?></p></td>
					<?php
					$nombres = array('nombre' => ucfirst($item['name']));
					?>
				<td><?=number_format($item['price'], 2, ',', '.');?></td>
				<?php
                if($this->cart->has_options($item['rowid']))
					{
						foreach($this->cart->product_options($item['rowid']) as $opcion => $value)
						{
							$peso = $value;
						}
					}
				?>
				<td><?=$item['qty'].' '.ucfirst($peso)?></td>
				<td>
                <a href="javascript:removeitem('<?=$item['rowid']?>')" id="<?=$item['rowid']?>"><b class="delete">X</b></a>
                </td>
			</tr>
			<?php
		}
		?>
		<tr id="total">
			<td colspan="2"><strong>Total Bs:</strong> <?= number_format($this->cart->total(), 2, ',', '.') ?></td>
			<td colspan="2" id="eliminarCarrito">
			<a href="javascript:enviadata('<?=base_url()?>home/eliminarCarrito')">Vaciar</a>
            </td>
		</tr>
        <tr>
        <td colspan="4">
        <div class="text-center">
        	<a style="margin:0px;" href="<?=base_url()?>crear#pedido" class="link">Crear Pedido</a>
        </div>
        </td>
        </tr>
	</table>
    </div>
    
    <?php
}
else
{
	?>
	<div id="contenidoCarrito">
		<table>
			<tr>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Cant.</th>
                <th></th>
			</tr>
			<tr>
				<td colspan="4">No hay articulos en el carrito</td>
			</tr>
			<tr>
                <td colspan="2"></td>
                <td colspan="2"></td>
			</tr>
       		<tr>
        		<td colspan="4"></td>
        	</tr>
	</table>
    </div>
	<?php
}
?>
<!--fin de nuestro carrito-->
</div>