<a name="ubicanos"></a>
<a name="inventario"></a>
<a name="only"></a>
<a name="empresa"></a>
<a name="disponibles"></a>
<a name="productos"></a>
<a name="enterate"></a>
<a name="clientesatifechos"></a>
<div class="buscador">
    <div class="container">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <form enctype="multipart/form-data" method="post" lang="es" id="buscar">
                <div class="input-group">
                <input class="form-control" type="text" id="consulta" name="query" placeholder="Que buscas?">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-search"><i class="fa fa-search"></i></button>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>