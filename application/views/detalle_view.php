<div class="col-hidden-md col-hidden-lg" style="padding:20px; 0"></div>
<div class="container detalle">
    <?php foreach($producto as $value):?>
    <div class="col-xs-12 router">
    <a href="<?=base_url()?>inicio"><i class="glyphicon glyphicon-home"></i>&nbsp;Inicio</a>&nbsp;/&nbsp;
    <a href="<?=base_url()?>productos#inventario">Productos</a>&nbsp;/&nbsp;
    <a href="<?=base_url()?>productos/<?=strtolower($value['c_nombre'])?>#only"><?=ucfirst($value['c_nombre'])?></a>&nbsp;/&nbsp;
    <span id="actual"><a name="info" style="text-decoration:none; color:#3A3A3A;"><?=$value['i_nombre']?></a></span>
    </div>
    <br/><br/>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div id="imagen">
            <div class="col-xs-12">
            <div id="nombre">
            <img src="<?=$value['imagen']?>" class="img-responsive" />
            <h1>
            <?=$value['i_nombre']?>
            </h1>
            </div>
            </div>
            <?php
            if(isset($value['i_clasificacion']))
            {
                switch($value['i_clasificacion'])
                {
                    case 4:
                    $label = 'oferta';
                    break;
                    case 2:
                    $label = 'recomendado';
                    break;
                    case 3:
                    $label = 'masvendido';
                    break;
                }
                if(isset($label))
                {?>
                <div class="product-label <?=$label?>"></div>
                <?php 
                }
            }
            if(isset($i_condicion) && $i_condicion > 0)
            {?>
                <div class="usado-label"></div>
            <?php }
            ?>          
              <!-- start Basic Jquery Slider -->
                <div id="theCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                      <?php
                      $j=0;
                      for($i=1;$i<4;$i++)
                      {
                          if(isset($value['p_'.$i]) && $value['p_'.$i]!='')
                          {
                              $j++;
                           ?>
                           <div class="item <?=$i==1?'active':''?>">
                            <img src="<?=$value['p_'.$i]?>" <?=$i==1?'id="unico"':''?> alt="<?=$value['i_cod'].' - '.$value['i_nombre']?>" class="img-responsive img-carousel"/>
                           </div>
                         <?php
                          }
                          else
                          {
                              break;
                          }
                      }
                    if($j > 0)
                    {
                        ?>
                      <a class="left carousel-control" href="#theCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Anterior</span>
                      </a>
                      <a class="right carousel-control" href="#theCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Siguiente</span>
                      </a>
                        <?php
                     }
                     ?>
                    </div>
                </div>
              <!-- end Basic jQuery Slider -->
            <?php
            if(isset($value['i_costoenvio']) && $value['i_costoenvio'] > 0)
            {?>
                <div class="col-xs-6">
                    <div class="genvio enviogratis">Envio Gratis</div>
                </div>
                <div class="col-xs-6 code">Cod:<?=$value['i_cod']?></div>
            <?php }else{ ?>
                <div class="col-xs-6 col-xs-offset-6 code">Cod:<?=$value['i_cod']?></div>
            <?php }?>
            <div class="clearfix"></div>
            <div class="col-xs-4">
            <img src="<?=base_url()?>img/logo.png" class="img-responsive" />
            </div>
            <?php
            if(isset($value['i_precio']))
            {
                if(strstr($value['i_precio'], '-'))
                {
                    $precios = explode('-', $value['i_precio']);
                    $pantes = $precios[0];
                    $pahora = $precios[1];
                    $descuento = (($pahora / $pantes)-1)*100;
                    if(-$descuento > 5)
                    {
            ?>
                        <div class="col-xs-8">
                            <div class="ofer">
                                <span id="descuento"><?=intval(-$descuento).'%'?></span>
                            </div>
                        </div>
            <?php
                    }
                    ?>
            <div class="clearfix"></div>
                <div class="col-xs-12 productbox">
                    <div class="col-xs-12">
                        <div class="pantes">
                            <span class="moneda">Bs </span>
                            <span class="antes"><?=number_format(sprintf("%01.2f",$pantes),"2", ",", ".")?></span>
                         </div>
                     </div>
                    <div class="col-xs-12">
                        <span class="moneda">Bs </span>
                        <span class="ahora"><?=number_format(sprintf("%01.2f",$pahora),"2", ",", ".")?></span>
                    </div>
                </div>
                <?php
                }
                else
                {
                ?>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 productbox">
                        <div class="col-xs-12">
                            <span class="moneda">Bs </span>
                            <span class="ahora"><?=number_format(sprintf("%01.2f",$value['i_precio']),"2", ",", ".")?></span>
                    </div>
                    </div>
                <?php
                }
            }
            ?>
            <div class="col-xs-12 col-md-12" style="padding:10px 0;margin:auto;display: flex;">
                <!-- AddToAny BEGIN -->
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="display:inline-block; margin:auto;">
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_google_plus"></a>
                <a class="a2a_button_whatsapp"></a>
                </div>
            </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
            <?=form_open(base_url().'agregarProducto','id="form0" class="boxform"') ?>
            <div class="col-xs-12 col-md-12">
                <div class="input-group" style="display: block;">
                    <select class="form-control cantidad" name="cantidad" id="cantidad<?=$value['i_id']?>" onchange="removedisable(<?=$value['i_id']?>)" tabindex="-1" aria-hidden="true">
                    <?php
                    for($i=0; $i <= $value['i_cantidad']; $i++)
                    {
                        if($i==0)
                        {
                            echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
                        }
                        else
                        {
                            echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                    }
                    ?>
                    </select>
                    <div class="input-group-btn btn-buy" id="btn-buy">
                        <?php
                        $button = array(
                                        "name"		=>	"action",
                                        //"value"   =>	"Agregar al Carrito",
                                        "content"	=>	"Al carrito <i style='font-size:28px' class='fa fa-shopping-cart'></i>",
                                        "class" 	=>	"btn btn-success cartbutton",
                                        "id"		=>	"save".$value['i_id'],
                                        "disabled"	=>	"disabled",
                                        "onclick"	=>	"addtocart2('#form0')"
                                        );
                        ?>
                        <?= form_button($button)?>
                        <?= form_hidden('id', $value['i_id']) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-7">
            <span class="valor"></span>
            </div>
            <?= form_close() ?>
            </div>
        </div>
    <?php
    $ancho = 'col-md-6';
    if(isset($value['v_url']) && $value['v_url']!='')
    {
        $ancho = 'col-md-12';
    ?>
    <div class="col-xs-12 col-md-6">
    <legend style="text-align:center;">Video Receta</legend>
    <div class="responsive-video">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$value['v_url']?>rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    </div>
    <div class="clearfix"></div>
    <?php
    }
    ?>
    <div class="col-xs-12 <?=$ancho?>" id="datos">
    <?php
    if(isset($value['i_descripcion']) && $value['i_descripcion']!='')
    {
        echo '<legend style="text-align:center;">Caracter&iacute;sticas</legend>';
        $nuevadescripcion = explode('-', $value['i_descripcion']);
        echo '<ul style="line-height:35px;padding:0px 15px;">';
        $contador = 0;
        foreach($nuevadescripcion as $value)
        {
            if($value!='' || $value!=NULL)
            {
                echo '<li>'.$value.'</li>';
            }
        }
        echo '</ul>';
    }
    ?>
    <br/><br/>
    <div class="col-xs-12" style="padding:0 0 10px 0; text-align:center;color:#6C6C6C">
    <strong><?=isset($visitas) ? ($visitas >1) ? $visitas.' personas se han interesado en este producto':$visitas.' persona se ha interesado en este producto':''?></strong>
    </div>
    </div>
    </div>
    <div class="clearfix"></div>
    <br/><br/>
    <?php endforeach?>
    <!--Facebook Comments-->
    <div class="col-xs-12">
    <div class="fb-comments" data-href="<?=base_url().$this->uri->slash_segment(1).$this->uri->slash_segment(2).$this->uri->segment(3);?>#info" data-width="100%" data-numposts="5"></div>
    </div>
    <!--End Facebook Comments-->
    <legend style="text-align:center;font-size:24px;font-style:italic;">Otros productos de inter&eacute;s</legend>
      <!-- start Basic Jquery Slider -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="theOwlCarousel">
                    <?php foreach($sugerencias as $result)
                    {
                        $this->load->view('templates/box.php', $result); 
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
      <!-- end Basic jQuery Slider -->
    </div>
<div class="clearfix"></div>
</div>
</div>
<script>
 $(document).ready(function(){
    $('#theCarousel').carousel({
            interval: 3000
        });
	$("#theOwlCarousel").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
  });
});	
$.fn.parpadear = function()
{
	this.each(function parpadear()
	{
		$(this).fadeIn(500).delay(300).fadeOut(500, parpadear);
	});
}
$('#descuento').parpadear();

if (screen.width < 760)
{
	$('#imagen').css('height', screen.height);
}

function addtocart2(formulario){
		var cart = $('#shop-cart');
        var imgtodrag = $('#unico');
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                	'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
	$.ajax({
		url: '<?=base_url('home/agregarProducto')?>',
		type: 'POST',
		data: $(formulario).serialize(),
		success:function(response){
			$('#numero').html(response);
			$(formulario).find('select').val('0')
			
		},
		error : function() {
            alert('Disculpe, existió un problema, Recargue la pagina');
        }
	});
}
function removeitem(id){
	var cart = $('#shop-cart');
	$.ajax({
		url: '<?=base_url()?>home/eliminarProducto/'+id,
		success:function(response){
			$('#numero').html(response);
			$('.dialog').load("../home/shopcart");
			},
		error : function() {
            alert('Disculpe, existió un problema, Recargue la pagina');
        }
	});
}

</script>
<!--<script type="text/javascript">
$(document).bind('contextmenu', function(e)
{
	return false;
});
</script>-->