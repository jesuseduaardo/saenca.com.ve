<section class="main-section">
    <div class="container" style="min-height:600px;">
    	<div class="row">
        	<h2 style="text-align:center;padding-top:20px;">Clientes satisfechos</h2>
			<?php
            if(isset($testimonios) && is_array($testimonios))
            {?>
                <div class="row">
                <?php
                foreach($testimonios as $testimonio)
                {
                    $articulo = explode('<br>', $testimonio['venta']);
                    $articulo = explode(':', $articulo[4]);
                    ?>
                    <article class="col-xs-12 testimonio">
                        <div class="col-xs-2" style="text-align:right;">
                        <img src="../img/user-icon.png" alt="<?=$testimonio['nombre']?>"/>
                        </div>
                        <div class="col-xs-10">
                        <h3><?=$testimonio['nombre']?></h3>
                        <p>
                        <?php
                        for($i=1;$i <= 5;$i++)
                        {
                            if($i <= $testimonio['valoracion'])
                            {
                                echo '<span class="fa fa-star stars filled"></span>';
                            }
                            else
                            {
                                echo '<span class="fa fa-star stars empty"></span>';
                            }
                        }
                        ?>
                        </p>
                        <p>"<?=$testimonio['comentarios']?>"</p>
                        <span><strong>Compra efectuada:&nbsp;</strong><?=trim($articulo[1])?></span>
                        - <span><?=date("d-m-Y", strtotime($testimonio['fecha']));?></span>
                        </div>
                    </article>
                <?php }?>
                </div>
            <?php }
            else
            {
                ?>
                <p>Disculpe no hay comentarios disponibles en este momento</p>
                <?php
            }
            ?>
        </div>
    </div>
</section>