<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_model extends CI_MODEL
{
    public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	public function get_product($categoria=NULL, $max_value=NULL)
	{
		if(!is_null($max_value))
		{
			$this->db->limit($max_value);
		}
		if(!is_null($categoria))
		{
			$this->db->where('c_nombre', $categoria);
			$query = $this->db->get('items_categoria');
			if ($query->num_rows() > 0)
			{
				$row = $query->row();
				$id = $row->c_id;
				$this->db->where('categoria', $id);
			}
		}
			$this->db->where('i_status', '1');
			$this->db->join('items_img', 'items_img.p_id = items.i_id', 'left');
			$this->db->join('items_marca', 'items_marca.m_id = items.marca');
			$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
			$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
			$this->db->order_by('fecha_creacion', 'DESC');
			$this->db->order_by('i_visitas', 'DESC');
			$articulos = $this->db->get('items');
			if($articulos->num_rows()>0)
			{
				return $articulos->result_array();
			}
			return FALSE;
	}
	
	private function if_exist($id, $tabla)
	{
		switch($tabla)
		{
			case 'items_adicional':
				$pre = 'v_';
			break;
			default:
				$pre = '';
			break;
		}
		$this->db->where($pre.'id', $id);
		$query = $this->db->get($tabla);
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}
	public function buscalo()
	{
		$query = $this->input->post('query');
		if($query!='' && $query!=NULL)
		{
			$input = str_split($this->input->post('query'));
			if( count($input) <= 3)
			{
				$query = strtolower($this->input->post('query'));
				
			}
			else
			{
				$query = substr(strtolower($this->input->post('query')),1,-1);
			}
			$this->db->like('i_nombre', $query);
			$result = $this->db->get('items');
			return $result->result_array();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_sliders()
	{
		$sliders = $this->db->get('slider');
		return $sliders->result_array();
	}
	public function get_redes()
	{
		$redes = $this->db->get('redes_sociales');
		return $redes->result_array();
	}
	public function get_productbyid($id, $nombre)
	{
		//$this->ingresavisitas($id);
		$this->db->from('items');
		$this->db->where('i_id', $id);
		$this->db->join('items_img', 'items_img.p_id = items.i_id');
		$this->db->join('items_marca', 'items_marca.m_id = items.marca');
		$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
		$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
		$this->db->join('items_adicional', 'items_adicional.v_id = items.i_id');
		$articulo = $this->db->get();
		if($articulo->num_rows()>0)
		{
			$row = $articulo->row();
			$c_nombre = trim($row->i_nombre);
			$c_nombre = str_replace(' ', '_', $c_nombre);
			if($nombre === strtolower($c_nombre))
			{
				return $articulo->result_array();		
			}
		}
		return false;
	}
	
	public function cuentavisitas($id)
	{
		  $ip = $_SERVER['REMOTE_ADDR'];
		  $this->db->where('codigo', $id);
		  $this->db->where('ip', $ip);
		  $query = $this->db->get('contador');
		  if ($query->num_rows() > 0)
		  {
			  $row = $query->row();
			  $fecha = $row->fecha;
			  $visit = $row->visitas;	
			  $segundos = strtotime('now') - strtotime($fecha);
			  $intervalo = intval($segundos/60/60);
			  if($intervalo > 1)
			  {
				  $visit++;
				  $data = array(
							  'visitas' => $visit,
							  'fecha' => date('Y-m-d H:i:s')
							  );
				  $this->db->where('codigo', $id);
				  $this->db->where('ip', $ip);
				  if($operacion = $this->db->update('contador', $data))
				  {
					  $this->db->where('codigo', $id);
					  $this->db->select_sum('visitas');
					  $sumavisitas = $this->db->get('contador');
					  $row = $sumavisitas->row();
					  $visitas = $row->visitas;
					  $this->db->where('i_id', $id);
					  $data = array('i_visitas' => $visitas);
					  $this->db->update('items', $data);
					  return $visitas;
				  }
			  }
			  else
			  {
				  $this->db->where('codigo', $id);
				  $this->db->select_sum('visitas');
				  $sumavisitas = $this->db->get('contador');
				  $row = $sumavisitas->row();
				  $visitas = $row->visitas;
				  return $visitas;
			  }
		  }
		  else
		  {
			  $data = array(
			  'codigo' 	=> $id,
			  'ip' 		=> $ip,
			  'visitas' 	=> 1,
			  'fecha'		=> date('Y-m-d H:i:s')
			  );
			  if($operacion = $this->db->insert('contador', $data))
			  {	
				  $this->db->where('codigo', $id);
				  $this->db->select_sum('visitas');
				  $sumavisitas = $this->db->get('contador');
				  $row = $sumavisitas->row();
				  $visitas = $row->visitas;
				  $this->db->where('i_id', $id);
				  $data = array('i_visitas' => $visitas);
				  $this->db->update('items', $data);
				  return $visitas;
			  }
			}
		}
	public function get_sugerencias($id)
	{
		$this->db->select('categoria');
		$this->db->where('i_id', $id);
		$articulo = $this->db->get('items');
		if ($articulo->num_rows() > 0)
		{
		  $row = $articulo->row();
		  $this->db->where('categoria', $row->categoria);
		  $this->db->where('i_status', '1');
		  $this->db->join('items_img', 'items_img.p_id = items.i_id');
		  $this->db->join('items_marca', 'items_marca.m_id = items.marca');
		  $this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
		  $this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
		  $this->db->order_by('id_articulo', 'RANDOM');
		  $this->db->limit(12);
		  $articulo = $this->db->get('items');
		}
		return $articulo->result_array();
	}
	
	public function indexproducts()
	{
		$this->db->select('*');
		$this->db->from('items');
		$this->db->where('i_status', '1');
		$this->db->join('items_img', 'items_img.p_id = items.i_id');
		$this->db->join('items_marca', 'items_marca.m_id = items.marca');
		$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
		$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
		//$this->db->order_by('i_clasificacion', 'DESC');
		$this->db->order_by('items.fecha_creacion', 'DESC');
		$this->db->limit(12);
		$articulos = $this->db->get();
		return $articulos->result_array();
	}
	public function recentproducts()
	{
		$this->db->select('*');
		$this->db->from('items');
		$this->db->where('i_status', '1');
		$this->db->join('items_img', 'items_img.p_id = items.i_id');
		$this->db->join('items_marca', 'items_marca.m_id = items.marca');
		$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
		$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
		$this->db->order_by('i_clasificacion', 'DESC');
		$this->db->order_by('fecha_creacion', 'DESC');
		$this->db->limit(12);
		$articulos = $this->db->get();
		return $articulos->result_array();
	}
	
	public function getCategorias()
	{
		$this->db->order_by('c_nombre', 'ASC');
		$categorias = $this->db->get('items_categoria');
		return $categorias->result_array();
	}
	public function get_contacto_info()
	{
		$this->db->select('*');
		$this->db->from('contacto');
		$this->db->join('redes_sociales', 'redes_sociales.id = contacto.id');
		$contacto = $this->db->get();
		return $contacto->result_array();
	}
	#testimonios
	public function inventario($id)
	{
		$this->db->where('i_id', $id);
		$this->db->join('items_marca', 'items_marca.m_id = items.marca');
		$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria', 'inner');
		$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion', 'inner');
		$this->db->join('items_img', 'items_img.p_id = items.i_id', 'left');
		$this->db->join('items_adicional', 'items_adicional.v_id = items.i_id', 'left');
		$query = $this->db->get('items');
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}
	public function check_coment_link($id, $key)
	{
		$this->db->where('id', $id);
		$this->db->where('clave', $key);
		$success =  $this->db->get('coment_customer');
		if($success->num_rows()>0)
		{
			return TRUE;
		}
		return FALSE;
	}
	public function check_coment($id)
	{
		$this->db->where('id', $id);
		$success =  $this->db->get('coment_customer');
		if($success->num_rows()>0)
		{
			$row = $success->row();
			if(strlen($row->comentarios) > 10)
			{
				return FALSE;
			}
			return TRUE;
		}
		return TRUE;
	}
	public function insertTestimonio()
	{
		$id = $this->input->post('nro');
		$key = $this->input->post('code');
		if($this->check_coment_link($id, $key))
		{
			$comentario = array(
								'valoracion'	=>	$this->input->post('value'),
								'comentarios'	=>	$this->input->post('coment'),
								'fecha'			=>	date('Y-m-d')
								);
			$this->db->where('id', $this->input->post('nro'));
			$this->db->where('clave', $this->input->post('code'));
			if($query = $this->db->update('coment_customer', $comentario))
			{
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}
	public function getOpinion()
	{
		$this->db->where('aprobado >', '0');
		$this->db->order_by('fecha','DESC');
		$opinion = $this->db->get('coment_customer');
		if($opinion->num_rows()>0)
		{
			return $opinion->result_array();
		}
		return false;
	}
	public function getOpinionbyid($id)
	{
		$this->db->where('id', $id); 
		$opinion = $this->db->get('opiniones');
		return $opinion->result_array();
	}
	public function insert_opinion()
	{
	$data = array(
		'nombre'		=>	$this->input->post('nombre'),
		'email'			=>	$this->input->post('email'),
		'producto' 		=>	$this->input->post('producto'),
		'valoracion'	=>	$this->input->post('valoracion'),
		'comentarios'	=>	$this->input->post('comentarios')
		);

	if($exito = $this->db->insert('opiniones', $data))
	{
		$nombre	=	$data['nombre'];
		$email	=	$data['email'];
		$this->db->select('id');
		$this->db->where('nombre', $nombre);
		$this->db->where('email', $email); 
		$query = $this->db->get('opiniones');
		$id = $query->result_array();
		foreach($id as $val)
		{
			$did = $val['id'];
		}
		return $did;
	}
	else
	{
		return FALSE;
	}
	}
	#Blog
	public function getEntries($titulo='', $paginas='')
	{
		if($titulo=='')
		{
			$this->db->order_by('date DESC');
			$this->db->limit($paginas['per_page'], $paginas['page']);
			$query = $this->db->get('blog_entradas');
		}
		else
		{
			$this->db->where('permalink', $titulo);
			$this->db->order_by('fecha DESC');
			$query = $this->db->get('blog_entradas');
		}
		return $query->result_array();
	}
	public function masvisto()
	{
		$this->db->order_by('visitas', 'DESC');
		$this->db->limit(5);
		$query = $this->db->get('blog_entradas');
		return $query->result_array();
	}
	public function getmonth()
	{
		$this->db->select('date');
		$this->db->order_by('date DESC');
		$query = $this->db->get('blog_entradas');
		return $query->result_array();
	}
/************************************/
/**************Captcha***************/
/************************************/
	public function insert_captcha($cap)
    {
        //insertamos el captcha en la bd
        $data = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
            );
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
 
    }
    public function remove_old_captcha($expiration)
    {
        //eliminamos los registros de la base de datos cuyo 
        //captcha_time sea menor a expiration
        $this->db->where('captcha_time <',$expiration);
        $this->db->delete('captcha');
    }
	public function check_captcha()
	{
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ?";
		$binds = array($this->input->post('captcha'), $this->input->ip_address());
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		return $row;
	}
	/*********************************/
	/************Fin Captcha**********/
	/********************************/
	/*********************************/
	/*********Carro de Compras********/
	/********************************/
	//cuando pulsemos en añadir al carrito esta función será la encargada de saber que producto hemos seleccionado por su id, que la envíamos desde la vista al controlador, y desde el controlador aquí, el modelo.    
	function porId()
	{
		$id = trim($this->input->post('id'));
		$this->db->where('i_id', $id);
		$this->db->join('items_marca', 'items_marca.m_id = items.marca');
		$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria');
		$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion');
		$productos = $this->db->get('items');
		return $productos->row_array();
	}
	function imgproduct($codigo)
	{
		$this->db->where('p_id', $codigo);
		$productos = $this->db->get('items_img');
		foreach ($productos->result() as $producto)
		{
			$img 	= $producto->p_1;
		}
		return $img;
	}
	/*********************************/
	/****Fin Carro de Compras********/
	/********************************/
	/*********************************/
	/************Pedido***************/
	/********************************/
	private function get_code($id)
	{
		$this->db->where('i_id', $id);
		$query = $this->db->get('items');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->i_cod;
		}
		return FALSE;
	}
	
	public function checkout()
	{
		if($carrito = $this->cart->contents())
		{
			$valid = 0;
			$novalid = 0;
			foreach ($this->cart->contents() as $items)
			{
				if($items['options']['cuponeable'] == 1)
				{
					$valid+= $items['price'] * $items['qty'];
				}
				else
				{
					$novalid+= $items['price'] * $items['qty'];
				}
			}
			if(isset($_SESSION['cupon']))
			{
				$cupval = $_SESSION['cupon'];
				$cupon = explode(',', $cupval);
				if($cupon[0] == '%')
				{
					$valid-= ($valid * $cupon[1]) / 100;
				}
				else if($cupon[0] == 'Bs')
				{
					$valid-= $cupon[1];
				}
			}
			$total = $novalid + $valid;
			$subtotal = $total / 1.12;
			$iva = $total-$subtotal;
			return array($subtotal, $iva, $total);
		}
		return 0;
	}
	
	function insertpedido()
	{
		$orden = array();
		$insertados=0;
		$orden['cirif'] = $this->input->post('cirif');
		$type_id = explode('-', $orden['cirif']);
		$data = array(
		'nombre'		=>	$this->input->post('nombre'),
		'cirif'			=>	$orden['cirif'],
		'telefono'		=>	$this->input->post('telefono'),
		'email'			=>	$this->input->post('email'),
		'direccion1' 	=>	$this->input->post('direccion1'),
		'direccion2'	=>	!is_null($this->input->post('direccion2')) && $this->input->post('direccion2')!='' ? $this->input->post('direccion2') : $this->input->post('direccion1'),
		'iva'			=>	$this->cart->total() < 200000 && strtolower($type_id[0]) =='v' ? 1.10 : 1.12
		);
		if($exito = $this->db->insert('pedido', $data))
		{
			$orden['nropedido'] = $this->db->insert_id();
			$carrito = $this->cart->contents();
			$type_id = explode('-', $orden['cirif']);
			foreach($carrito as $val)
			{
			  	$data2['id']		= $orden['nropedido'];
			  	$data2['i_cod']	= $this->get_code($val['id']);
			  	$data2['cantidad']= $val['qty'];
			  	$data2['subtotal']= round($val['price'] / $data['iva'], 2);
			  	$data2['iva']	  = round($val['price'] - $data2['subtotal'], 2);
			  	$data2['i_nombre']= $val['name'];
			  	$data2['p_1']	  = $val['options']['img'];
			  	$data2['m_nombre']= $val['options']['marca'];
			  	$data2['c_nombre']= $val['options'] ['categoria'];
				if($val['options']['cuponeable'] == 1)
				{
					$data2['i_cupon'] = 1;
				}
				else
				{
					$data2['i_cupon'] = 0;
				}
			  	if($exito = $this->db->insert('pedido_content', $data2))
			  	{
					$insertados++;
				  	$producto = $this->db->get_where('items', array('i_id' => $val['id']));
					if ($producto->num_rows() > 0)
					{
				  		$row = $producto->row_array();
				  		$update['i_cantidad'] = $row['i_cantidad'] - $val['qty'];
				  		$this->db->where('i_id', $val['id']);
				  		$this->db->update('items', $update);
					}
			  }
			}
			if(isset($_SESSION['cupon']))
			{
				$cupon = explode(',', $_SESSION['cupon']);
				$cup_data = $this->db->get_where('cupones', array('cu_id' => $cupon[3]));
				$row = $cup_data->row();
				
				$data2 = array();
				$data2['id'] = $orden['nropedido'];
				$data2['cantidad'] = 1;
				$data2['i_cod'] = 'cupon-'.$row->cu_id;
				$data2['i_nombre'] = $row->cu_promo;
				if($cupon[0] == '%')
				{
					$data2['m_nombre']= 'porc';
					$valid-= ($valid * $row->cu_monto) / 100;
				}
				else if($cupon[0] == 'Bs')
				{
					$data2['m_nombre']= 'monto';
					$valid-= $row->cu_monto;
				}
				else
				{
					$data2['m_nombre']= 'envio';
				}
				unset($cupon);
				$data2['c_nombre'] = $row->cu_code;
				$data2['p_1'] = 'no-image';
				$data2['iva'] = 0;
				$data2['subtotal'] = $row->cu_monto;
				if($exito = $this->db->insert('pedido_content', $data2))
			  	{
					$insertados++;
					$data2 = array(
								'id_cupon' => $row->cu_id,
								'id_pedido'=> $orden['nropedido']
								);
					if($exito = $this->db->insert('cupones_stats', $data2))
					{
				  		$data2 = array('cu_usos' => intval($row->cu_usos) + 1);
				  		$this->db->where('cu_id', $row->cu_id)->update('cupones', $data2);
					}
			  	}
			}
			if($insertados > 0)
			{
				$this->cart->destroy();
				unset($data, $data2, $_SESSION['cupon']);
				return $orden;
			}	
			else
			{
				return FALSE;
			}
		}
	}
	public function pedidoinfo($nropedido, $nroidentificacion)
	{
		$this->db->where('id', $nropedido);
		$this->db->where('cirif', $nroidentificacion);
		$pedido = $this->db->get('pedido');
		if($pedido->num_rows() > 0)
		{
			$pedido_info = $pedido->row_array();
			$this->db->where('id', $nropedido);
			$detalles = $this->db->get('pedido_content');
			$pedido_info['productos'] = $detalles->result_array();
			return $pedido_info;
		}
		return FALSE;
	}
	/*********************************/
	/************Fin Pedido***********/
	/********************************/
}