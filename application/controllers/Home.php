<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper(array('url','form','html'));
		$this->load->model('site_model','model');
	}
	public function index($page='index',$options='')
	{
		//$n = '2880';
		//$this->output->cache($n);
		switch($page)
		{
			case 'index':
			$header = array(
							'descripcion'	=> 'Empresa dedicada a la venta de equipos electronicos y soluciones tecnologicas',
							'titulo'		=> 'Saenca Comercial | Tu Soluci&oacute;n en Electronica',
							'css'			=>  array(''),
							'pagina'		=> 'inicio'
							);
			$data = $this->header_generator($header);
			$options['categorias']= $this->model->getCategorias();
			$options['productos']=$this->model->indexproducts();
			$options['recientes']= $this->model->recentproducts();
			$contacto['jquery']=array(
									'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js'
									);
			break;
			default:
				$header = array(
								'descripcion'	=>	$options['descripcion'],
								'titulo'		=>	$options['titulo'],
								'css'			=>	$options['css'],
								'pagina'		=> 	$options['pagina']
								);

				$data = $this->header_generator($header);
				$contacto['jquery']  =$options['jquery'];
			break;
		}
		$header['productos'] = isset($options['categorias']) ? $options['categorias'] : '';
		$data = $this->header_generator($header);
		$this->load->view('templates/header', $data);
		if($page=='index')
		{
			$slider['slider'] = $this->model->get_sliders();
			$this->load->view('templates/carousel', $slider);
		}
		$this->load->view($page, $options);
		$contacto['contacto'] = $this->model->get_contacto_info();
		//$contacto['captcha'] = $this->captcha();
		$contacto['form'] = array(
					'name' => $this->security->get_csrf_token_name(),
        			'hash' => $this->security->get_csrf_hash()
					);
		$this->load->view('templates/footer', $contacto);
	}
	public function inicio()
	{
		$this->index($page='index');
	}
	public function condiciones()
	{
		$this->index($page='condiciones_view');
	}
	private function header_generator($header)
	{
		header('Cache-Control: must-revalidate');
		header('Expires: '.gmdate ('D, d M Y H:i:s', time() + 60*60*24*30).' GMT');
		$descripcion= $header['descripcion'];
		$titulo		= $header['titulo'];
		$css		= $header['css'];
		$pagina		= $header['pagina'];
		$meta = array(
        array(
                'name' => 'google',
                'content' => 'notranslate'
        ),
		array(
				'name'	=>	"google-site-verification",
				'content'=> "KWKAnHsG4NEOGPlmdQ95EmbQM0sfz_mL6GX4Flc-7Wc"
		),
        array(
                'name' => 'author',
                'content' => 'La Victoria 3021 R.L, lavictoria3021@gmail.com'
        ),
		array(
                'name' => 'pagename',
                'content' => $header['titulo']
        ),
        array(
                'name' => 'distribution',
                'content' => 'global'
        ),
		array(
                'name' => 'description',
                'content' => $descripcion
        ),
		array(
                'name' => 'robots',
                'content' => 'no-cache'
        ),
		array(
                'name' => 'googlebot',
                'content' => 'noarchive'
        ),
        array(
                'name' => 'Content-type',
                'content' => 'text/html;charset=utf8', 'type' => 'equiv'
        ),
		array(
				'name' => 'viewport',
				'content' => 'width=device-width, initial-scale=1'
		),
		 array(
                'name' => 'X-UA-Compatible',
                'content' => 'IE=edge', 
				'type' => 'equiv'
        ),
		array(
                'name' => 'Cache-Control',
                'content' => 'max-age = 604800, must-revalidate'
        )
	);
	$meta2 = array(
    				'og:site_name'	=> 	'www.saenca.com.ve',
                    'og:image'		=>	base_url().'/img/logo.png',
                    'og:description'=>	$descripcion
    				);
	$link = array(
					link_tag('favicon.ico', 'shortcut icon', 'image/x-icon'),
					link_tag(CBOOTSTRAP,'stylesheet', 'text/css','', 'screen, print'),
					link_tag(FONTAWESOME,'stylesheet', 'text/css','', 'screen'),
					link_tag('css/style.css','stylesheet', 'text/css','', 'screen'),
					link_tag('css/principal.css','stylesheet', 'text/css','', 'screen'),
					link_tag('css/responsive.css','stylesheet', 'text/css','', 'screen'),
					link_tag('css/animate.css','stylesheet', 'text/css','', 'screen'),
					link_tag(JQUERYUIC,'stylesheet', 'text/css','', 'screen'),
					link_tag('css/printver.css','stylesheet', 'text/css','', 'print')
					);
	if(isset($css))
	{
		foreach($css as $val)
		{
			if($val!='' || $val!=NULL || is_int($val))
			{
				$link[] = link_tag('css/'.$val,'stylesheet','text/css','','screen');
			}
		}
	}
	unset($val);
	$sliders = $this->model->get_sliders();
	unset($header);
	$header = array(
						'doctype' => 'html5',
						'lang' => 'es',
						'meta' => $meta,
						'meta2'=> $meta2,
						'link' => $link,
						'title' => $titulo,
						'slider'=> $sliders,
						'pagina'=> $pagina	
						);
	return $header;
	}
	public function descargas()
	{
		$options['descripcion'] = "Descargue las providencias administrativas vigentes y manuales de equipos";
		$options['titulo']	=	'Descargas';
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'descargas#providencias&manuales';
		$categorias = $this->model->getCategorias();
		$options['categorias'] = $categorias;
		$products = array();
		foreach($categorias as $value)
		{
			$products[] = $this->model->get_product($value['nombre_category'],4);
		}
		$options['productos']=$products;
		$this->index('downloads_view',$options);
	}
	
	public function productos()
	{
		$options['pagina']	=	'productos';
		$header = array(
						'descripcion'	=> 'Lider en productos de electronica',
						'titulo'		=> 'Productos',
						'css'			=>  array('owl.carousel.min.css', 'component.css', 'normalize.css', 'detalle.css'),
						'pagina'		=>  $options['pagina']
						);
		$data = $this->header_generator($header);
		$options['jquery']	=	array(
			base_url('js/classie.js'), 
			base_url('js/menu.js'));
		$this->load->view('templates/header', $data);
		$options['categorias'] = $this->model->getCategorias();
		$this->load->view('productos_view',$options);
		$contacto['contacto'] = $this->model->get_contacto_info();
		//$contacto['captcha'] = $this->captcha();
		$contacto['form'] = array(
					'name' => $this->security->get_csrf_token_name(),
        			'hash' => $this->security->get_csrf_hash()
					);
		$this->load->view('templates/footer', $contacto);
	}
	
	public function invntr($categoria=NULL, $id=NULL, $nombre=NULL)
	{
		$products = array();
		if(!is_null($categoria) && !is_null($id) && !is_null($nombre))
		{
			$options['categoria_url'] =	$categoria;
			$options['categoria'] 	  =	ucwords(str_replace('_',' ', $categoria));
			if($options['producto']	= $this->model->get_productbyid($id, $nombre))
			{
				foreach($options['producto'] as $key=>$val)
				{
					$options['producto'][$key]['visitas'] = $this->model->cuentavisitas($id);
					$precio = explode('-', $val['i_precio']);
					if(isset($precio[1]) && $precio[1]!='' && !is_null($precio[1]))
					{
						$options['producto'][$key]['precio_viejo'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");
						$options['producto'][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[1]),"2", ",", ".");
					}
					else
					{
						$options['producto'][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");	
					}
					$i_descripcion = explode('-', $options['producto'][$key]['i_descripcion']);
					$options['producto'][$key]['i_descripcion'] = '<ul>';
					foreach($i_descripcion as $value)
					{
						$options['producto'][$key]['i_descripcion'] .= '<li>'.$value.'</li>';
					}
					$options['producto'][$key]['i_descripcion'] .= '</ul>';
				}
				$options['datos'] = $this->model->get_contacto_info();
				$options['url'] = base_url().'/productos#/'.$categoria.'/'.$id.'/'.$nombre;
				$options['sugerencias'] = $this->model->get_sugerencias($id);
				$this->output->set_content_type('application/json')->set_output(json_encode($options));
			}
		}
		elseif(!is_null($categoria))
		{
			$categoria = str_replace('_',' ', $categoria);
			$options['pagina']	= ucwords($categoria);
			$options['categoria'] = array(array('c_nombre'=>$categoria));
			$products[$categoria] = $this->model->get_product($categoria);
			foreach($products[$categoria] as $key => $val)
			{
				$url = trim($val['i_nombre']);
				$url = strtolower(str_replace(' ', '_', $url));
				$products[$categoria][$key]['url'] = '#/'.str_replace(' ', '_', $categoria).'/'.$val['i_id'].'/'.$url;
				$precio = explode('-', $val['i_precio']);
				if(isset($precio[1]) && $precio[1]!='' && !is_null($precio[1]))
				{
					$products[$categoria][$key]['precio_viejo'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");
					$products[$categoria][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[1]),"2", ",", ".");
				}
				else
				{
					$products[$categoria][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");
				}
				
			}
			$options['productos']=$products;
			$this->output->set_content_type('application/json')->set_output(json_encode($options));
		}
		else
		{
			$options['pagina']	= 'Productos';
			$options['categoria'] = $this->model->getCategorias();
			foreach($options['categoria'] as $value)
			{
				$products[$value['c_nombre']] = $this->model->get_product($value['c_nombre'], 8);
				if(isset($products[$value['c_nombre']]) && !empty($products[$value['c_nombre']]))
				{
					foreach($products[$value['c_nombre']] as $key => $val)
					{
						$url = trim($val['i_nombre']);
						$url = strtolower(str_replace(' ', '_', $url));
						$products[$value['c_nombre']][$key]['url'] = '#/'.str_replace(' ', '_', $value['c_nombre']).'/'.$val['i_id'].'/'.$url;
						$precio = explode('-', $val['i_precio']);
						if(isset($precio[1]) && $precio[1]!='' && !is_null($precio[1]))
						{
							$products[$value['c_nombre']][$key]['precio_viejo'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");
							$products[$value['c_nombre']][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[1]),"2", ",", ".");
						}
						else
						{
							$products[$value['c_nombre']][$key]['i_precio'] = number_format(sprintf("%01.2f", $precio[0]),"2", ",", ".");
						}
					}
				}
			}
			$options['productos']=$products;
			$this->output->set_content_type('application/json')->set_output(json_encode($options));
		}
		return false;
	}
	private function crea_meta($categoria)
	{
		$meta = array();
		$categorias = $this->model->getCategorias();
		foreach ($categorias as $val)
		{
			if($categoria == strtolower($val['c_nombre']))
			{
				$meta['descripcion'] = $val['c_nombre'];
				$meta['titulo'] =  ucfirst($val['c_nombre']);
			}
		}
		return($meta);
	}
	public function contacto($options='')
	{
		//$options['captcha'] = $this->captcha();
		$options['descripcion'] = "Pongase en contacto con nosotros y solicite un presupuesto sin compromiso";
		$options['titulo']	=	'Contacto';
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'contacto#ubicanos';
		$options['contacto']=	$this->model->get_contacto_info();
		$this->index('contacto_view',$options);
	}
	
	private function check_g_recaptcha()
	{
		if($this->input->post('g-recaptcha-response') != NULL && $this->input->post('g-recaptcha-response') != '')
		{
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='
							  .GKEY.'&response='.$this->input->post('g-recaptcha-response'));
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}
	
	public function nosotros()
	{
		$options['descripcion'] = "Conozca nuestro Origen y Compromiso";
		$options['titulo']	=	'Nosotros';
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'nosotros#empresa';
		$this->index('empresa_view',$options);
	}
#Servicios
	public function servicios()
	{
		$options['descripcion'] = "Conozca los servicios que tenemos para ud";
		$options['titulo']	=	'Servicios';
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'servicios#disponibles';
		$this->index('servicios_view',$options);
	}
#Testimonios
	public function testimonios()
	{
		$options['descripcion'] = "Conozca la opinion de nuestros clientes";
		$options['titulo']	=	'Testimonios de nuestros clientes';
		$options['testimonios'] = $this->model->getOpinion();
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'testimonios#clientesatifechos';
		$this->index('testimonios_view',$options);
	}
/*************Opiniones*************/
	public function testimonio($id, $key)
	{
		if($this->model->check_coment_link($id, $key) && $this->model->check_coment($id))
		{
			$options['coment'] = TRUE;
			$options['nro'] = $id;
			$options['code']= $key; 
			$options['jquery']	=	array(base_url('js/bootstrap-star-rating.js'));
		}
		else
		{
			$options['coment'] = FALSE;
		}
		$options['descripcion'] = "Para nosotros su opinion es muy importante";
		$options['titulo']	=	'Denos su testimonio de compra';
		$options['css']		=	array('');
		$options['pagina']	=	'testimonio#opinion';
		$this->index('testimonio_view',$options);
	}
	public function send_opinion()
	{
		if($this->model->insertTestimonio())
		{
			$this->output->set_status_header('200');
			echo json_encode(array('response' => 'Hemos recibido su testimonio, Muchas gracias!'));
		}
		else
		{
			$this->output->set_status_header('400');
			echo 'Disculpe no se pudo recibir la informacion, por favor intente mas tarde';
		}
	}
	public function opiniones()
	{
		$options['opiniones']=$this->model->getOpinion();
		$options['descripcion'] = "Enterese de nuestra calidad atraves de la opinion de nuestros clientes";
		$options['titulo']	=	'Opiniones Clientes';
		$options['css']		=	array('opinion.css');
		$options['jquery']	=	array('');
		$options['pagina']	=	'opiniones#opinion';
		$this->index('opiniones_view',$options);
	}

	public function mi_opinion($result='')
	{
		if($result!='')
		{
		$options['result']	= $result;
		}
		$options['captcha'] = $this->captcha();
		$options['descripcion'] = "Denos su opinion sobre la calidad de nuestro servicio";
		$options['titulo']	=	'Mi Opinion';
		$options['css']		=	array('opinion.css');
		$options['jquery']	=	array('js/ajax.js');
		$this->index('mi_opinion_view',$options);
	}
	private function notificacion($id)
	{
		$this->load->library('email');
		$data['data'] = $this->model->getOpinionbyid($id);
		foreach ($data['data'] as $val)
		{
			$email = $val['email'];
			$nombre = $val['nombre'];
		}
		$this->email->from('noreply@lavictoria3021.com.ve');
		$this->email->to('root@192.168.1.108'); 
		//$this->email->cc('another@another-example.com'); 
		//$this->email->bcc('lavictoria3021@gmail.com'); 
		$this->email->subject('Nueva opinion de - '.$nombre." (".$email.")");
		$this->email->message( $this->load->view( 'notificacion', $data, true ));
		return $this->email->send();
	}
/**************Fin Opiniones**********/
#Blog
	public function blog()
	{
		$this->load->library('pagination');
		$options['descripcion']="Enterese de nuestras novedades en productos y servicios que tenemos para Ud";
		$options['titulo']	=	'Blog';
		$options['css']		=	array('blog.css');
		$options['jquery']	=	array('');
		$options['pagina']	=	'blog#enterate';
			$config['base_url'] 	= base_url().'blog/';
			$config['total_rows'] 	= $this->db->count_all('blog_entradas');
			$config['per_page'] 	= 3;
			$config['uri_segment']	= 2;
			$choice = $config['total_rows'] / $config['per_page'];
			$config['num_links'] 	= floor($choice);
			 //config for bootstrap pagination class integration
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['first_link'] 		= false;
			$config['last_link'] 		= false;
			$config['first_tag_open'] 	= '<li>';
			$config['first_tag_close'] 	= '</li>';
			$config['prev_link'] 		= '&laquo';
			$config['prev_tag_open'] 	= '<li class="prev">';
			$config['prev_tag_close'] 	= '</li>';
			$config['next_link'] 		= '&raquo';
			$config['next_tag_open'] 	= '<li>';
			$config['next_tag_close'] 	= '</li>';
			$config['last_tag_open'] 	= '<li>';
			$config['last_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a href="#">';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['num_tag_open'] 	= '<li>';
			$config['num_tag_close'] 	= '</li>';
			$this->pagination->initialize($config);
		$options['page'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
			$paginas['per_page']=	$config['per_page'];
			$paginas['page'] 	=	$options['page'];
		$options['entries']		=	$this->model->getEntries('', $paginas);
		$options['masvisto']	=	$this->model->masvisto();
		$options['fechas']		= 	$this->fechas();
		$options['paginas']		=	$this->pagination->create_links();
		$this->index('blog_view',$options);
	}
	
	public function articulo($titulo)
	{
		$options['entries'] = $this->model->getEntries($titulo);
		foreach($options['entries'] as $entry)
		{
			$id = $entry['id'];
			$options['descripcion'] = $entry['titulo'];
			$options['titulo'] = $entry['titulo'];	
			$tags = $entry['tags'];
		}
		$options['css']		=	array('blog.css');
		$options['jquery']	=	array('');
		$options['pagina']	=	'blog#enterate';
		$options['visitas'] = $this->model->cuentavisitas($id);
		$options['relacionadas']= $this->model->relacionadas($tags);
		$this->index('view_entry', $options);
	}
	
	public function fechas()
	{
		$fechas = $this->model->getmonth();
		return $fechas;
	}
	public function shopcart()
	{
		$this->load->view('templates/carrito_new.php');	
	}
	public function check_pedido($nropedido,$nroidentificacion)
	{
		$options['descripcion'] = "Detalle de pedido nro ".str_pad($nropedido, 6, '0', STR_PAD_LEFT);
		$options['titulo']	=	'Pedido Nro '.str_pad($nropedido, 6, '0', STR_PAD_LEFT);
		$options['css']		=	array('');
		$options['jquery']	=	array('');
		$options['pagina']	=	'productos#productos';
		$options['pedidoinfo']= $this->model->pedidoinfo($nropedido,$nroidentificacion);
		$options['contacto'] = $this->model->get_contacto_info();
		$this->index('pedidoinfo_view',$options);
	}
	public function buscar()
	{
		$busqueda = $this->input->post('query');
		$options['descripcion'] = "Resultados para la busqueda de ".$busqueda;
		$options['titulo']	=	$busqueda." en Saenca";
		$options['pagina']	=	'productos#productos';
		$options['productos'] = $this->model->buscalo();
		$this->index('results_view',$options);
	}
	public function creapedido()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre','Nombre','trim|required');
		$this->form_validation->set_rules('cirif','Cedula o Rif','trim|required');
		$this->form_validation->set_rules('telefono','Tel&eacute;fono','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('direccion1','Direcci&oacute;n fiscal','trim|required');
		$this->form_validation->set_rules('captcha','Captcha','trim|required|callback_validate_captcha');
		$this->form_validation->set_message('required', 'El campo %s es requerido');
		$this->form_validation->set_message('valid_email', 'El campo %s no contiene un email valido');
		$this->form_validation->set_message('integer', 'El %s debe ser solo numeros');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if($this->form_validation->run() == FALSE)
		{
			$this->pedido();
		}
		else
		{
			if($pedido = $this->model->insertpedido())
			{
				if($this->send_pedido($this->input->post('email'), $pedido))
				{
					$this->model->insertTestimonio($pedido);	
				}
				redirect(base_url('pedido/'.$pedido['nropedido'].'/'.$pedido['cirif']));
			}
		}
	}
	private function send_pedido($email, $pedido)
	{
		//test
		$options['pedidoinfo']= $this->model->pedidoinfo($pedido['nropedido'], $pedido['cirif']);
		//$this->load->view('templates/notificapedido', $options, true );
		
		$this->load->library('email');
		$this->email->from('ventas@saenca.com.ve', 'Saenca tu tienda en linea');
		$this->email->reply_to('ventas@saenca.com.ve');
		$this->email->to($email);
		//$this->email->to('casamagdaleno@gmail.com'); 
		//$this->email->cc($from); 
		$this->email->bcc('saenca.ventas@gmail.com'); 
		$this->email->subject('Se ha creado el pedido nro'. str_pad($pedido['nropedido'], 6, '0', STR_PAD_LEFT) .' en Saenca tu tienda en linea');
		$options['pedidoinfo']= $this->model->pedidoinfo($pedido['nropedido'], $pedido['cirif']);
		$this->email->message($this->load->view('templates/notificapedido', $options, true ));
		if($this->email->send())
		{
			return TRUE;
		}
		return FALSE;
	}
	public function pedido()
	{
		$options['captcha'] = $this->captcha();
		$options['descripcion'] = "Complete los datos de su pedido";
		$options['titulo']	=	'Crear Pedido';
		$options['css']		=	array('');
		$options['jquery']	=	array('js/ajax.js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js', 'js/validapedido.js');
		$options['pagina']	=	'Pedido';
		$this->index('pedido',$options);
	}

	private function notify($data)
	{
		$this->load->view('notificacion', $data);
	}
	public function send_mail()
	{
		if($this->check_g_recaptcha())
		{
			$this->load->library('email');
			$mensaje['nombre'] 	= $this->input->post('nombre');
			$mensaje['email']	= $this->input->post('email');
			$mensaje['mensaje']	= $this->input->post('mensaje');
			$mensaje['asunto']	= $this->input->post('asunto');
			$this->email->from('ventas@saenca.com.ve', 'Saenca');
			$this->email->reply_to($mensaje['email'], $mensaje['nombre']);
			$this->email->to('saenca.ventas@gmail.com');
			$this->email->subject($mensaje['asunto']);
			$this->email->message($mensaje['mensaje']);
			if($this->email->send())
			{
				$this->output->set_status_header('200');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('response' => 'Su mensaje ha sido enviado. Muchas gracias, pronto nos pondremos en contacto con ud')));
			}
			else
			{
				$this->output->set_status_header('200');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('response' => 'No se pudo enviar el mensaje')));
			}
		}
		else
		{
			$this->output->set_status_header('200');
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('response' => 'Favor marque el reto captcha')));
		}

	}
	
	/*********************************************
						CAPTCHA
	/**********************************************/
	public function recarga_captcha()
	{
		$nuevo_captcha = $this->captcha();
		$imagen_nueva = $nuevo_captcha['image'];
		$data['captcha'] = $imagen_nueva;
		$this->load->view('new_captcha_view', $data);
	}

	private function captcha()
    {
		$this->load->helper('captcha');
        //configuramos el captcha
        $conf_captcha = array(
            'word'		=>	'',
            'img_path'	=>	'captcha/',
            'img_url'	=> 	base_url().'captcha/',
            //fuente utilizada por mi, poner la que tengáis
            'font_path'	=>	'fonts/webfont.ttf',
            'img_width'	=>	'150px',
            'img_height'=>	'50px',
            //decimos que pasados 5 minutos elimine todas las imágenes
            //que sobrepasen ese tiempo
            'expiration' => 300,
        	'word_length'=> 5,
        	'font_size'  => 20,
        	'pool'       => '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ', 
			'colors'        => array(
                'background' => array(242, 255, 243),
                'border' => array(255, 255, 255),
                'text' => array(15, 72, 22),
                'grid' => array(3, 156, 43))
        );
        //guardamos la info del captcha en $cap
        $cap = create_captcha($conf_captcha);
        //pasamos la info del captcha al modelo para 
        //insertarlo en la base de datos
		$expiration = time()-300; // Límite de 5 minutos
		$this->model->remove_old_captcha($expiration);
        $this->model->insert_captcha($cap);
        //devolvemos el captcha para utilizarlo en la vista
        return $cap;
    }
	public function validate_captcha()
	{
		//comprobamos si es correcta la imagen introducida
		$check = $this->model->check_captcha();
		/*
		|si el número de filas devuelto por la consulta es igual a 1
		|es decir, si el captcha ingresado en el campo de texto es igual
		|al que hay en la base de datos, junto con la ip del usuario
		|entonces dejamos continuar porque todo es correcto
		*/
		if($check->count == 1)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('validate_captcha', 'Captcha Invalido intente de nuevo');
			return FALSE;
		}
	}	
	
	/**************************
			FIN CAPTCHA
	**************************/
	/********************Carro de Compras**********/
	public function checkout()
	{
		return $this->model->checkout();
	}
	public function addcupon()
	{
		$des = 0;$nodes=0;
		if($this->cart->contents())
		{
			foreach ($this->cart->contents() as $items)
			{
				if($items['options']['cuponeable'] == 1)
				{
					$des++;
				}
				else
				{
					$nodes++;
				}
			}
			if($des > 0)
			{
				$this->db->where('cu_code', $this->input->post('codigo'));
				$query = $this->db->get('cupones');
				if($query->num_rows() > 0)
				{
					$row = $query->row();
					switch($row->cu_modo)
					{
						case 1:
							$_SESSION['cupon'] = '%,'.$row->cu_monto.','.$row->cu_code.','.$row->cu_id;
						break;
						case 2:
							$_SESSION['cupon'] = 'Bs,'.$row->cu_monto.','.$row->cu_code.','.$row->cu_id;
						break;
						case 3:
							if($nodes > 0)
							{
								return $this->output->set_status_header('400')
										->set_output('Este pedido contiene productos que no aplican para este cupon');
							}
							else
							{
								$_SESSION['cupon'] = 'freesend,'.$row->cu_monto.','.$row->cu_code.','.$row->cu_id;
							}
						break;
					}
					$cupon = $query->result_array();
					$cupon[0]['total'] = $this->model->checkout();
					return $this->output
									->set_status_header('200')
									->set_content_type('application/json')
									->set_output(json_encode(array('response' => $cupon)));
				}
				return $this->output->set_status_header('400')
									->set_output('Cupon no valido');
			}
			else
			{
				return $this->output->set_status_header('400')
									->set_output('Este cupon no aplica para este pedido');
			}
		}
		return $this->output->set_status_header('400')
							->set_output('No hay articulos en el carrito');
	}
	
	function agregarProducto()
	 {
		 $producto = $this->model->porId();
		 $cantidad = $this->input->post('cantidad');
		 //obtenemos el contenido del carrito
		 $carrito = $this->cart->contents();
		 foreach ($carrito as $item)
		 {
			 //si el id del producto es igual que uno que ya tengamos
			 //en la cesta le sumamos uno a la cantidad
			 if ($item['id'] == $producto['i_id'])
			 {
				 $cantidad = $this->input->post('cantidad');
				 break;
			 }
			 //si tiene el mismo id pero distinta cantidad la sumamos
			 
		 }
		 $precio = explode('-',$producto['i_precio']);
		 if(isset($precio[1]))
		 {
			 $nuevoprecio = $precio[1];
		 }
		 else
		 {
			 $nuevoprecio = $precio[0];
		 }
			//cogemos los productos en un array para insertarlos en el carrito
			$insert = array(
							'id' => $producto['i_id'],
							'qty' => $cantidad,
							'price' => $nuevoprecio,
							'name' => $producto['i_nombre']
							);
			/*si hay opciones creamos un array con las opciones y lo metemos        
			en el carrito*/
			$imagen = $this->model->imgproduct($producto['i_id']);
			$insert['options'] = array('img' =>$imagen, 'marca'=> $producto['m_nombre'], 'categoria'=> $producto['c_nombre'], 'cuponeable'=>$producto['i_cupon']);
			//insertamos al carrito
			$this->cart->insert($insert);
			//cogemos la url para redirigir a la página en la que estabamos
			//$uri = $this->input->post('uri');
			//redirigimos mostrando un mensaje con las sesiones flashdata
			//de codeigniter confirmando que hemos agregado el producto
			//$this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
			//redirect(base_url().$uri, 'refresh');
			echo count($this->cart->contents());
		}
			
		function eliminarProducto($rowid)
		{
			//para eliminar un producto en especifico lo que hacemos es conseguir su id
			//y actualizarlo poniendo qty que es la cantidad a 0
			$producto = array(
							'rowid' => $rowid,
							'qty' => 0
							);
			//después simplemente utilizamos la función update de la librería cart
			//para actualizar el carrito pasando el array a actualizar
			$this->cart->update($producto);
			//$this->session->set_flashdata('productoEliminado', 'El producto fue eliminado correctamente');
			//return TRUE;
			echo count($this->cart->contents());
		}
			
		function eliminarCarrito()
		{
			if(isset($_SESSION['cupon']))
			{
				unset($_SESSION['cupon']);
			}
			$this->cart->destroy();
			$this->session->set_flashdata('destruido', 'El carrito fue eliminado correctamente');
			return TRUE;
		}		
}