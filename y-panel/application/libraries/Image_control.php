<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image_control{
	
	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();// Assign the CodeIgniter super-object
		$this->CI->load->library(array('session'));
		$this->CI->load->helper(array('url','form','html'));
	}
	
	function crear_thumb($img, $dir, $width='50', $height='50')
	{
        //unset al arreglo config por si existe en memoria
		unset($config);
		$carpeta_img = '../img/';
		$ruta = $carpeta_img.$dir.'/thumbs';
		if(!file_exists($ruta))
		// comprobamos que exista la ruta 
		{
			// Si no existe la creamos  
			$ruta = $carpeta_img.$dir.'/thumbs';
			$dirmake = mkdir("$ruta", 0777);
			$ruta.="/";
		}
		else
		{
			$ruta = $carpeta_img.$dir.'/thumbs/';//carpteta donde vamos a guardar la imagen	
		}
		$config['image_library']  = 'GD2';
		$config['source_image']   = $carpeta_img.$dir.'/'.$img;
                //se debe de crear la carpeta thumb dentro de nuestro directorio $dir
		$config['new_image']      = $ruta.$img;
		$config['create_thumb']   = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']          = $width;
		$config['height']         = $height;
 
                //verificamos que no este vacio nuestro archivo a subir
		if(!empty($config['source_image']))
		{
                        //cargamos desde CI  a nuestra libreria image_lib
			$this->CI->load->library('image_lib', $config);
                        // iniciamos image_lib con el contenido de $config
			$this->CI->image_lib->initialize($config);
 
                        //le hacemos resize a nuestra imagen
			if (!$this->CI->image_lib->resize())
			{
				$error = array('error'=>$this->CI->image_lib->display_errors());
				return $error;
			}
			else
			{
				return TRUE;
			}
                        //limpiamos el contenido de image_lib esto para crear varias thumbs 
			$this->CI->image_lib->clear();
		}
	}

	function sube_imagenes($imagen, $dir)
	{
		//unset al arreglo config por si existe en memoria
		unset($config);
		$carpeta_img ='../../img/';
		$ruta = $carpeta_img.$dir;
		
		if(!file_exists($ruta))
		// comprobamos que exista la ruta 
		{
			// Si no existe la creamos  
			$dirmake = mkdir("$ruta", 0777);
			$ruta.="/";
		}
		else
		{
			$ruta.='/';//carpteta donde vamos a guardar la imagen	
		}
	  	$config['upload_path'] 		= $ruta;
	  	$config['allowed_types'] 	= 'jpg|png|jpeg|gif';
	  	$config['max_size']			= '2048';
	  	$config['max_width']  		= '1024';
	  	$config['max_height']  		= '1024';
	  	$config['overwrite']		=  FALSE;
		$config['remove_spaces'] 	=  TRUE;
		$config['encrypt_name']		=  TRUE;
	  	$name 						=  $_FILES["$imagen"]["name"]; // get file name from form
	  	$fileNameParts   			= explode( '.', $name ); // explode file name to two part
	  	$fileExtension   			= end( $fileNameParts ); // give extension
	  	$fileExtension   			= strtolower( $fileExtension ); // convert to lower case
	  	$encripted_pic_name   		= md5("pic".'_'.rand(0,1000)).'.'.$fileExtension;  // new file name
	  	$config['file_name'] 		= $encripted_pic_name; //set file name
		$this->CI->load->library('upload', $config);
		if(!$this->CI->upload->do_upload($imagen))
		{
			//$error = array('error' =>$this->CI->upload->display_errors());
			return FALSE;
		}
		else 
		{
			 return $this->CI->upload->data('file_name');
		} 
	  }
	  
	 public function  borrar_imagen($dir,$file)
	 {
		unset($config);
		$carpeta_img = '../img/';
		$ruta = $carpeta_img.$dir.'/'.$file;
		if(!file_exists($ruta))
		// comprobamos que exista la ruta 
		{
			// Si no existe la creamos  
			return FALSE;
		}
		else
		{
		  //el archivo1 es obligatorio el archivo2 es solo si es imagen y se creo thumbnail esto para borrar ambos
		  // $dir es el nombre de la carpeta o direccion $file es el archivo
		  $archivo1  		= $ruta;
		  $array_nombre 	= explode('.',$file);
		  $picname 		= $array_nombre[0];
		  $extension 		= $array_nombre[1];
		  $array_nombre2	= explode('_',$picname);
		  $picname		= $array_nombre2[0];
		  $picfullname 	= $picname."_thumb.".$extension;
		  $archivo2  		= $carpeta_img.$dir.'/thumbs/'.$picfullname; 
		  //buscamos si el archivo1 existe
		  if(file_exists($archivo1) && unlink($archivo1))
		  {
				  $exito = 1; 
			  } else {
				  $exito = 0; 
			  }
		  //lo mismo pero solo si es imagen y tememos thumb
		  if(file_exists($archivo2) && unlink($archivo2))
		  {
				  $exito += 1; 
		  }
			  return $exito;
		}
	}
}
?>