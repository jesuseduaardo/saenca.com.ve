<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clean_string{
	
	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();// Assign the CodeIgniter super-object
		$this->CI->load->helper(array('url','html'));
	}
	
	private function sanear($url){
	 $url =  html_entity_decode(strtolower($url));
	 //Reemplazamos caracteres especiales latinos
	 $find = array('á','é','í','ó','ú','â','ê','î','ô','û','ã','õ','ç','ñ');
	 $repl = array('a','e','i','o','u','a','e','i','o','u','a','o','c','n');
	 $url = str_replace($find, $repl, $url);
	 //Añadimos los guiones
	 $find = array(' ', '&amp;', '\r\n', '\n','+');
	 $url = str_replace($find, '-', $url);
	 //Eliminamos y Reemplazamos los demas caracteres especiales
	 $find = array('/[^a-z0-9\-&lt;&gt;]/', '/[\-]+/', '/&lt;{^&gt;*&gt;/');
	 $repl = array('', '-', '');
	 $url = preg_replace($find, $repl, $url);
	 return $url;
	}
	
	public function permalink($string){
		return $this->sanear($string);
	}
}
?>