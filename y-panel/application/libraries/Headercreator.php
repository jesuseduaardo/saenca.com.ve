<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Headercreator{
	
	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();// Assign the CodeIgniter super-object
		$this->CI->load->helper(array('url','html'));
	}
	public function header_generator($header)
	{
		header('Cache-Control: must-revalidate');
		header('Expires: '.gmdate ('D, d M Y H:i:s', time() + 60*60*24*30).' GMT');
		$titulo		= $header['titulo'];
		$meta = array(
        array(
                'name' => 'google',
                'content' => 'notranslate'
        ),
        array(
                'name' => 'author',
                'content' => 'La Victoria 3021 R.L, lavictoria3021@gmail.com'
        ),
		array(
                'name' => 'designer',
                'content' => 'Jesus Castillo, jesus-castillo@outlook.com'
        ),
		array(
                'name' => 'pagename',
                'content' => $header['titulo']
        ),
		array(
                'name' => 'robots',
                'content' => 'no-cache'
        ),
		array(
                'name' => 'googlebot',
                'content' => 'noarchive'
        ),
        array(
                'name' => 'Content-type',
                'content' => 'text/html;charset=utf8', 'type' => 'equiv'
        ),
		array(
				'name' => 'viewport',
				'content' => 'width=device-width, initial-scale=1'
		),
		 array(
                'name' => 'X-UA-Compatible',
                'content' => 'IE=edge', 
				'type' => 'equiv'
        ),
		array(
                'name' => 'Cache-Control',
                'content' => 'max-age = 604800, must-revalidate'
        )
	);
	$css = array(
				CBOOTSTRAP,
				'css/bootstrap-theme.min.css',
				ADMINLTE,
				ADMINLTE_THEMES,
				'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.35.1/css/bootstrap-dialog.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/jquery.dataTables.min.css',
				FONTAWESOME,
				'css/ionicons.min.css',
				'css/panel.css'
				);
	if(isset($css))
	{
		foreach($css as $val)
		{
			if($val!='' || $val!=NULL || !is_int($val))
			{
				$link[] = link_tag($val,'stylesheet','text/css','','screen');
			}
		}
	}
	unset($val);
	$newheader = array(
						'doctype' => 'html5',
						'lang' => 'es',
						'meta' => $meta,
						'link' => $link,
						'title' => $titulo
						);
	return $newheader;
	}
}
	