<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	private function registra_intento($ip)
	{
		$this->db->where('ip', $ip);
		$query = $this->db->get('intentos_login');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$segundos = strtotime('now') - strtotime($row->fecha);
			$intervalo = intval($segundos);
			$intentos = $row->intentos;
			if($intentos < 3)
			{
				$intentos+= 1;
				$data = array(
							'intentos' => $intentos,
							'fecha' => date('Y-m-d H:i:s')
							);
				$this->db->where('ip', $ip);
				$this->db->update('intentos_login', $data);
			}
			if($intentos >= 3)
			{
				if($intervalo < 180)
				{
					$this->session->set_tempdata('time', strtotime($row->fecha), 180);
				}
				else
				{
					$data = array(
								'intentos' => 0,
								'fecha' => date('Y-m-d H:i:s')
							);
					$this->db->where('ip', $ip);
					$this->db->update('intentos_login', $data);
				}
			}
		}
		else
		{
			$data = array(
					'ip' => $ip,
					'intentos' => '0',
					'fecha' => date('Y-m-d H:i:s')
			);
			$this->db->insert('intentos_login', $data);
		}
	}
	
	public function get_intentos()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->db->where('ip', $ip);
		$query = $this->db->get('intentos_login');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->intentos;
		}
	}
	public function login_user($user,$pass)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->db->where('email',$user);
		$query = $this->db->get('users');
		if($query->num_rows() == 1)
		{
			$row = $query->row();
		  	if($row->activo == 1 && $pass == $row->pass)
		  	{
				$data = array(
						  'user_id'	=> $row->id, 
						  'ip'		=> $ip,
						  'fecha' 	=> date('Y-m-d H:i:s')
						  );
				$this->db->insert('users_login', $data);
				return $row;
			}
			else
			{
				$this->registra_intento($ip);
				return FALSE;
			}
		}
		else
		{
			$this->registra_intento($ip);
			return FALSE;
		}
	}
}