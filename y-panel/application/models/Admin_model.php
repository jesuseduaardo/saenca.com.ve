<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function duplicate($tabla, $cod, $nombre, $id=NULL)
	{
		switch($tabla)
		{
			case 'items':
				$pre = 'i_';
			break;
			case 'items_categoria':
				$pre = 'c_';
			break;
			case 'items_marca':
				$pre = 'm_';
			break;
		}
		$this->db->where($pre.'cod', $cod);
		if(isset($id))
		{	
			$this->db->where($pre.'id!=', $id);
		}
		$result = $this->db->get($tabla);
		if($result->num_rows()>0)
		{
			return 'Codigo';
		}
		else
		{
			$this->db->where($pre.'nombre', $nombre);
			if(isset($id))
			{	
				$this->db->where($pre.'id!=', $id);
			}
			$result = $this->db->get($tabla);
			if($result->num_rows()>0)
			{
				return 'Nombre';
			}
		}
		return FALSE;
	}
	public function duplicate_user($email, $user)
	{
		$this->db->where('email', $email);
		$result = $this->db->get('users');
		if($result->num_rows()>0)
		{
			return 'Email';
		}
		$this->db->where('user', $user);
		$result = $this->db->get('users');
		if($result->num_rows()>0)
		{
			return 'Usuario';
		}
		return FALSE;
	}
	public function ocupado($type, $id)
	{
		$this->db->where($type, $id);
		$query = $this->db->get('items');
		if($query->num_rows()>0)
		{
			$result = array('No se puede eliminar el registro mientras este en uso por los items con codigo: ');
			foreach($query->result_array() as $row)
			{
				$result[] = $row['i_cod'];
			}
			return $result;
		}
		else
		{
			return FALSE;
		}
	}
	public function inventario($id = NULL)
	{
		if(is_null($id))
		{
			$this->db->join('items_marca', 'items_marca.m_id = items.marca');
			$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria', 'inner');
			$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion', 'inner');
			$this->db->join('items_img', 'items_img.p_id = items.i_id', 'left');
			$query = $this->db->get('items');
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$this->db->where('i_id', $id);
			$this->db->join('items_marca', 'items_marca.m_id = items.marca');
			$this->db->join('items_categoria', 'items_categoria.c_id = items.categoria', 'inner');
			$this->db->join('items_control', 'items_control.ctrl_id = items.i_clasificacion', 'inner');
			$this->db->join('items_img', 'items_img.p_id = items.i_id', 'left');
			$this->db->join('items_adicional', 'items_adicional.v_id = items.i_id', 'left');
			$query = $this->db->get('items');
			if($query->num_rows()>0)
			{
				return $query->row_array();
			}
			else
			{
				return FALSE;
			}
		}
		
	}
	public function cupones($id=NULL)
	{
		if(is_null($id))
		{
			$cupones = $this->db->get('cupones');
			$tcupones = $cupones->result_array();
			foreach($tcupones as $key => $cupon)
			{
				$this->db->where('id_cupon', $cupon['cu_id']);
				$status = $this->db->get('cupones_stats');
				if($status->num_rows()>0)
				{
					$tcupones[$key]['stats'] = $status->result_array();
				}
			}
			return $tcupones;
		}
		else
		{
			$this->db->where('cu_id', $id);
			$cupones = $this->db->get('cupones');
			return $cupones->result_array();
		}
	}
	public function resumen_inventario()
	{
		$this->db->order_by('fecha_creacion', 'DESC');
		$this->db->limit(10);
		$query = $this->db->get('items');
		return $query->result_array();
		
	}
	public function resumen_pedidos($id=NULL, $limit=NULL)
	{
		$this->db->order_by('fecha', 'DESC');
		if(!is_null($limit))
		{
			$this->db->limit($limit);	
		}
		if(!is_null($id))
		{
			$this->db->where('id', $id);	
		}
		$query = $this->db->get('pedido');
		if($query->num_rows() > 0)
		{
			$result = $query->result_array();
			foreach($result as $key => $val)
			{
				$result[$key]['fecha'] = date_format(date_create($val['fecha']), 'd-m-Y');
				$iva = 0;
				$sub = 0;
				$this->db->where('id', $val['id']);
				$query = $this->db->get('pedido_content');
				$result[$key]['productos'] = $query->result_array();
				foreach($result[$key]['productos'] as $clave => $value)
				{
					$iva += $value['cantidad'] * $value['iva'];
					$sub += $value['cantidad'] * $value['subtotal'];
				}
				$result[$key]['iva'] = number_format(sprintf("%01.2f", $iva),"2", ",", ".");
				$result[$key]['sub'] = number_format(sprintf("%01.2f", $sub),"2", ",", ".");
				$result[$key]['total'] = number_format(sprintf("%01.2f", $iva + $sub),"2", ",", ".");
			}
			return $result;
		}
		return FALSE;
		
	}
	
	public function count_data($tabla)
	{
		return $this->db->count_all_results($tabla);
	}
	public function cuentavisitas()
	{
		$this->db->select_sum('i_visitas');
		$query = $this->db->get('items');
		$row = $query->row();
		return $row->i_visitas;
	}
	public function rating_average()
	{
		$this->db->select_avg('valoracion');
		$query = $this->db->get('coment_customer');
		$row = $query->row();
		return $row->valoracion;
	}
	public function users_get($id=NULL)
	{
		if(!is_null($id))
		{
			switch ($this->session->userdata('perfil')) {
				case '0':
					$this->db->where('id', $id);
					$query = $this->db->get('users');
					if($query->num_rows()>0)
					{
						return $query->row_array();
					}
					return FALSE;
				break;
				case '1':
					$this->db->where('id', $id);
					$this->db->where('perfil', '>=1');
					$query = $this->db->get('users');
					if($query->num_rows()>0)
					{
						return $query->row_array();
					}
					return FALSE;
				break;
				default:
					return FALSE;
				break;		
			}
		}
		else
		{
			switch ($this->session->userdata('perfil')) {
				case '0':
					$query = $this->db->get('users');
					if($query->num_rows()>0)
					{
						return $query->result_array();
					}
					return FALSE;
				break;
				case '1':
					$this->db->where('perfil >=', '1');
					$query = $this->db->get('users');
					if($query->num_rows()>0)
					{
						return $query->result_array();
					}
					return FALSE;
				break;
				default:
					return FALSE;
				break;		
			}
		}
	}
	
	private function pre($tabla)
	{
		switch($tabla)
		{
			case 'items':
				$pre = 'i_';
			break;
			case 'items_categoria':
				$pre = 'c_';
			break;
			case 'items_marca':
				$pre = 'm_';
			break;
			default:
				$pre = '';
			break;
		}
		return $pre;
	}
	
	public function count_depends($code, $campo)
	{
		$this->db->where($campo, $code);
		$this->db->from('items');
		return $this->db->count_all_results();
	}
	public function pedidos_get($id=NULL)
	{
		if(is_null($id))
		{
			$query = $this->db->get('pedido');
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
		}
		else
		{
			$this->db->where('id', $id);
			$query = $this->db->get('pedido');
			if($query->num_rows()>0)
			{
				return $query->result_array();
			}
		}
		return FALSE;
	}
	public function pedido_content($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('pedido_content');
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		return FALSE;
	}
	public function model_get($tabla, $id)
	{
		if(!is_null($tabla) && !is_null($id))
		{
			$pre = $this->pre($tabla);
			$query = $this->db->where($pre.'id', $id)->get($tabla);
			if($query->num_rows()>0)
			{
				return $query->row_array();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			if(!is_null($tabla))
			{
				$query = $this->db->get($tabla);
				if($query->num_rows()>0)
				{
					return $query->result_array();
				}
				else
				{
					return FALSE;
				}
			}
		}
	}
	
	public function save($tabla, $post)
	{
		$this->db->insert($tabla, $post);
		if($this->db->affected_rows()>0)
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function update($tabla, $post)
	{
		//$this->db->where($pre, $id);
		$this->db->replace($tabla, $post);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function intercambiar($id)
	{
		$coment = $this->model_get('coment_customer', $id);
		switch($coment['aprobado'])
		{
			case '0':
				$data=array('aprobado'=>1);
			break;
			default:
				$data=array('aprobado'=>0);
			break;
		}
		$this->db->where('id',$id);
		$this->db->update('coment_customer', $data);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete($tabla, $pre, $id)
	{
		$this->db->where($pre,$id);
		$this->db->delete($tabla);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function vaciar($tabla)
	{
		return $this->db->truncate($tabla);
	}
	
	public function accesos()
	{
		$this->db->select('*');
		$this->db->from('users_login');
		$this->db->where('result', 1);
		$this->db->join('users', 'users.id = users_login.user_id');
		$this->db->order_by('fecha', 'DESC');
		$this->db->limit(6);
		$visitas = $this->db->get();
	  	return $visitas->result_array();	
	}
	
	public function updateTestimonio($id, $key)
	{
		$datos = array('clave'=>$key);
		$this->db->where('id', $id);
		if($this->db->update('coment_customer', $datos))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}