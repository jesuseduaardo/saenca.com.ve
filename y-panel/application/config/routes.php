<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['admin/inventario']['get'] = 'admin/item';
$route['admin/inventario/(:any)']['get'] = 'admin/item/$1';
$route['admin/inventario']['post'] = 'admin/item';
$route['admin/inventario/(:any)']['put'] = 'admin/item/$1';
$route['admin/inventario/(:any)']['delete'] = 'admin/item/$1';

$route['admin/categoria']['get'] = 'admin/categoria';
$route['admin/categoria/(:any)']['get'] = 'admin/categoria/$1';
$route['admin/categoria']['post'] = 'admin/categoria';
$route['admin/categoria/(:any)']['put'] = 'admin/categoria/$1';
$route['admin/categoria/(:any)']['delete'] = 'admin/categoria/$1';

$route['admin/marca']['get'] = 'admin/marca';
$route['admin/marca/(:any)']['get'] = 'admin/marca/$1';
$route['admin/marca']['post'] = 'admin/marca';
$route['admin/marca/(:any)']['put'] = 'admin/marca/$1';
$route['admin/marca/(:any)']['delete'] = 'admin/marca/$1';

$route['admin/contacto']['get'] = 'admin/contacto';
$route['admin/contacto/(:any)']['put'] = 'admin/contacto/$1';

$route['admin/redes']['get'] = 'admin/redes';
$route['admin/redes/(:any)']['put'] = 'admin/redes/$1';

$route['admin/blog']['get'] = 'admin/blog';
$route['admin/blog/(:any)']['get'] = 'admin/blog/$1';
$route['admin/blog']['post'] = 'admin/blog';
$route['admin/blog/(:any)']['put'] = 'admin/blog/$1';
$route['admin/blog/(:any)']['delete'] = 'admin/blog/$1';

$route['admin/coment']['get'] = 'admin/coment';
$route['admin/coment/(:any)']['get'] = 'admin/coment/$1';
$route['admin/coment']['post'] = 'admin/coment';
$route['admin/coment/(:any)']['put'] = 'admin/coment/$1';
$route['admin/coment/(:any)']['delete'] = 'admin/coment/$1';

$route['admin/apariencia']['get'] = 'admin/sliders';
$route['admin/apariencia/(:any)']['get'] = 'admin/sliders/$1';
$route['admin/apariencia']['post'] = 'admin/sliders';
$route['admin/apariencia/(:any)']['delete'] = 'admin/sliders/$1';

$route['admin/emergente']['get'] = 'admin/emergente';
$route['admin/emergente']['post'] = 'admin/emergente';
$route['admin/emergente/(:any)']['put'] = 'admin/emergente/$1';
$route['admin/emergente/(:any)']['delete'] = 'admin/emergente/$1';

$route['admin/usuarios']['get'] = 'admin/usuarios';
$route['admin/usuarios']['post'] = 'admin/usuarios';
$route['admin/usuarios/(:any)']['put'] = 'admin/usuarios/$1';
$route['admin/usuarios/(:any)']['delete'] = 'admin/usuarios/$1';

$route['admin/tipouser']['get'] = 'admin/tipouser';

$route['admin/resume']['get'] = 'admin/resumen';

$route['admin/orders']['get'] = 'admin/orders';
$route['admin/orders/(:num)']['get'] = 'admin/orders/$1';

$route['admin/salida'] = 'login/logout_ci';

$route['login/timer'] = 'login/end_timer';
$route['login/intentos'] = 'login/trata';
$route['login/token'] = 'login/token';

$route['default_controller'] = 'login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;