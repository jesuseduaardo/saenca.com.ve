<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Admin extends REST_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library(array('session', 'clean_string'));
		$this->load->helper(array('url','form','html'));
		$this->load->model('admin_model','model');
	}
	
	private function failed($tabla, $pre, $id)
	{
		//Si falla el registro de un producto, borramos lo que pudo haber guardado
		if(NULL == $id && NULL == $pre && NULL == $tabla)
		{
			return FALSE;
		}
		else
		{
			return $this->model->delete($tabla, $pre, $id);
		}
	}
	
	private function respuesta($type, $boolean)
	{
		switch($type)
		{
			case 'post':
				if($boolean)
				{
					$this->response(array('response'=>'Registro Exitoso'), 200);
				}
				else
				{
					$this->response(array('error'=>'Registro Fallido'), 200);
				}
			break;
			case 'put':
				if($boolean)
				{
					$this->response(array('response'=>'Edicion Exitosa'), 200);
				}
				else
				{
					$this->response(array('error'=>'Edicion Fallida'), 200);
				}
			break;
			case 'delete':
				if($boolean)
				{
					$this->response(array('response'=>'Borrado Exitoso'), 200);
				}
				else
				{
					$this->response(array('error'=>'Borrado Fallido'), 200);
				}
			break;
		}
	}
	/************************************************************/
							#RESUMEN
	/************************************************************/
	public function resumen_get()
	{
		$resultado['inventario'] = $this->model->resumen_inventario();
		$resultado['totali'] = $this->model->count_data('items');
		$resultado['puntuacion'] = $this->model->rating_average();
		$resultado['totalp'] = $this->model->count_data('pedido');
		$resultado['pedidos'] = $this->model->resumen_pedidos(NULL, 10);
		$resultado['visitas'] = $this->model->cuentavisitas();	
		$this->response(array('response'=>$resultado), 200);
	}
	/************************************************************/
							#Pedidos
	/************************************************************/
	public function orders_get($id=NULL)
	{	
		if($resultado = $this->model->resumen_pedidos($id))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No hay pedidos'), 200);
		}
	}
	/************************************************************/
							#INVENTARIO
	/************************************************************/
	public function item_get($item = NULL)
	{
		if(is_null($item))
		{
			$resultado = $this->model->inventario();
			if(!is_null($resultado))
			{
				$this->response(array('response'=>$resultado), 200);
			}
			else
			{
				$this->response(array('error'=>'No hay inventario'), 200);
			}
		}
		else
		{
			$resultado['items'] = $this->model->inventario($item);	
			$resultado['categorias'] = $this->model->model_get('items_categoria', NULL);
			$resultado['marcas'] = $this->model->model_get('items_marca', NULL);
			$resultado['control'] = $this->model->model_get('items_control', NULL);
			if(!is_null($resultado))
			{
				$this->response(array('response'=>$resultado), 200);
			}
			else
			{
				$this->response(array('error'=>'No se encontro el producto'), 200);
			}
		}
	}
	public function item_post()
	{
		if(!$this->post('items'))
		{
			$this->response(NULL, 404);
		}
		else
		{
			$item = $this->post('items');
			if($clave = $this->model->duplicate('items', $item['i_cod'], $item['i_nombre']))
			{
				return $this->response(array('error'=>'Ya existe un registro con ese '.$clave), 200);
			}
			else
			{
				
				$post =array(
						'i_cod' 		=> $item['i_cod'],
						'i_nombre' 		=> $item['i_nombre'],
						'marca'			=> $item['marca'],
						'categoria'		=> $item['categoria'],
						'i_descripcion' => $item['i_descripcion'],
						'i_precio' 		=> $item['i_precio'],
						'i_cantidad' 	=> $item['i_cantidad'],
						'i_clasificacion'=>$item['i_clasificacion'],
						'i_condicion'	=> isset($item['i_condicion']) ? $item['i_condicion'] : NULL,
						'i_costoenvio'	=> isset($item['i_costoenvio']) ? $item['i_costoenvio']: NULL,
						'i_status' 		=> isset($item['i_status'])?$item['i_status']:NULL,
						'i_cupon' 		=> $item['i_cupon']
						);
				if($id = $this->model->save('items', $post))
				{
					$img = array(
							'p_id'		=> $id,
							'p_1' 		=> $item['p_1'],
							'p_2' 		=> isset($item['p_2']) ? $item['p_2'] : NULL,
							'p_3'		=> isset($item['p_3']) ? $item['p_3'] : NULL 
							);
					if(!NULL == $this->model->save('items_img', $img))
					{
						$vid =array(
							'v_id'		=> $id,
							'v_url'		=> isset($item['v_url']) ? $item['v_url'] : NULL
							);
						if(!NULL == $this->model->save('items_adicional', $vid))
						{
							return $this->respuesta('post', TRUE);
						}
						else
						{
							$this->failed('items', 'i_id', $id);
							$this->failed('items_img', 'p_id', $id);
							return $this->respuesta('post', FALSE);
						}
					}
					else
					{
						$this->failed('items', 'i_id', $id);
						return $this->respuesta('post', FALSE);
					}
				}
				else
				{
					$this->respuesta('post', FALSE);
				}
			}
		}
	}
	public function item_put($id)
	{
		if(!$this->put('items') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$item = $this->put('items');
			$post =array(
						'i_id' 			=> $item['i_id'],
						'i_cod' 		=> $item['i_cod'],
						'i_nombre' 		=> $item['i_nombre'],
						'marca'			=> $item['marca'],
						'categoria'		=> $item['categoria'],
						'i_descripcion' => $item['i_descripcion'],
						'i_precio' 		=> $item['i_precio'],
						'i_cantidad' 	=> $item['i_cantidad'],
						'i_clasificacion'=>$item['i_clasificacion'],
						'i_condicion'	=> isset($item['i_condicion']) ? $item['i_condicion'] : NULL,
						'i_costoenvio'	=> isset($item['i_costoenvio']) ? $item['i_costoenvio']: NULL,
						'i_status' 		=> isset($item['i_status']) ? $item['i_status']:NULL,
						'i_cupon' 		=> $item['i_cupon']
						);
			if($this->model->update('items', $post))
			{
				$img = array(
						'p_id'		=> $id,
						'p_1' 		=> $item['p_1'],
						'p_2' 		=> isset($item['p_2']) ? $item['p_2'] : NULL,
						'p_3'		=> isset($item['p_3']) ? $item['p_3'] : NULL
						);
				if($this->model->update('items_img', $img))
				{
					$vid =array(
						'v_id'		=> $id,
						'v_url'		=> isset($item['v_url']) ? $item['v_url'] : NULL
						);
					if($this->model->update('items_adicional', $vid))
					{
						return $this->respuesta('put', TRUE);
					}
					else
					{
						return $this->respuesta('put', FALSE);
					}
				}
				else
				{
					return $this->respuesta('put', FALSE);
				}
			}
			else
			{
				$this->respuesta('put', FALSE);
			}
		}

	}
	public function item_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			if($this->model->delete('items', 'i_id', $id))
			{
				if($this->model->delete('items_img', 'p_id', $id))
				{
					$this->model->delete('items_adicional', 'v_id', $id);
					$this->respuesta('delete', TRUE);
				}
				else
				{
					$this->respuesta('delete', FALSE);
				}
			}
			else
			{
				$this->respuesta('delete', FALSE);
			}
		}
	}
	/************************************************************/
							#CATEGORIAS
	/************************************************************/
	public function categoria_get($categoria = NULL)
	{
		$resultado = $this->model->model_get('items_categoria', $categoria);
		if(is_null($categoria))
		{
			foreach($resultado as $key => $value)
			{
				$resultado[$key]['c_items'] = $this->model->count_depends($value['c_id'], 'categoria');
			}
		}
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la categoria'), 404);
		}
	}
	public function categoria_post()
	{
		if(!$this->post('categoria'))
		{
			$this->response(NULL, 400);
		}
		else
		{
			$categoria = $this->post('categoria');
			if($clave = $this->model->duplicate('items_categoria', $categoria['c_cod'], $categoria['c_nombre']))
			{
				return $this->response(array('error'=>'Ya existe un registro con ese '.$clave), 200);
			}
			else
			{
				$post =array(
						'c_cod' 		=> $categoria['c_cod'],
						'c_nombre' 		=> $categoria['c_nombre'],
						'c_descripcion'	=> $categoria['c_descripcion'],
						'c_pic'			=> isset($categoria['c_pic']) && !is_null($categoria['c_pic'])?$categoria['c_pic']:''
						);
				$this->respuesta('post', $this->model->save('items_categoria', $post));
			}
		}
	}
	public function categoria_put($id)
	{
		if(!$this->put('categoria') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$categoria = $this->put('categoria');
			if($clave = $this->model->duplicate('items_categoria', $categoria['c_cod'], $categoria['c_nombre'], $id))
			{
				return $this->response(array('error'=>'Ya existe un registro con ese '.$clave), 200);
			}
			else
			{
				$post =array(
						'c_id'	 		=> $id,
						'c_cod' 		=> $categoria['c_cod'],
						'c_nombre' 		=> $categoria['c_nombre'],
						'c_descripcion'	=> $categoria['c_descripcion'],
						'c_pic'			=> isset($categoria['c_pic']) && !is_null($categoria['c_pic'])?$categoria['c_pic']:''
						);
				$this->respuesta('put', $this->model->update('items_categoria', $post));
			}
		}
	}

	public function categoria_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			if($resultado = $this->model->ocupado('categoria', $id))
			{
				$this->response(array('error'=>$resultado), 200);
			}
			else
			{
				$this->respuesta('delete',  $this->model->delete('items_categoria', 'c_id', $id));
			}
		}
	}
	/************************************************************/
							#MARCAS
	/************************************************************/
	public function marca_get($id = NULL)
	{
		$resultado = $this->model->model_get('items_marca', $id);
		if(is_null($id))
		{
			foreach($resultado as $key => $value)
			{
				$resultado[$key]['m_items'] = $this->model->count_depends($value['m_id'], 'marca');
			}
		}
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la categoria'), 404);
		}
	}
	public function marca_post()
	{
		if(!$this->post('marca'))
		{
			$this->response(NULL, 400);
		}
		else
		{
			$marca = $this->post('marca');
			if($clave = $this->model->duplicate('items_marca', $marca['m_cod'], $marca['m_nombre']))
			{
				return $this->response(array('error'=>'Ya existe un registro con ese '.$clave), 200);
			}
			else
			{
				$post =array(
						'm_cod' 		=> $marca['m_cod'],
						'm_nombre' 		=> $marca['m_nombre'],
						'm_descripcion'	=> $marca['m_descripcion'],
						'm_pic'			=> $marca['m_pic']
						);
				$this->respuesta('post', $this->model->save('items_marca', $post));
			}
		}
	}
	public function marca_put($id)
	{
		if(!$this->put('marca') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$marca = $this->put('marca');
			if($clave = $this->model->duplicate('items_marca', $marca['m_cod'], $marca['m_nombre'], $id))
			{
				return $this->response(array('error'=>'Ya existe un registro con ese '.$clave), 200);
			}
			else
			{
				$post =array(
							'm_id'	 		=> $id,
							'm_cod' 		=> $marca['m_cod'],
							'm_nombre' 		=> $marca['m_nombre'],
							'm_descripcion'	=> $marca['m_descripcion'],
							'm_pic'			=> $marca['m_pic']
							);
				$this->respuesta('put', $this->model->update('items_marca', $post));
			}
		}
	}
	public function marca_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			if($resultado = $this->model->ocupado('marca', $id))
			{
				$this->response(array('error'=>$resultado), 200);
			}
			else
			{
				$this->respuesta('delete',  $this->model->delete('items_marca', 'm_id', $id));
			}
		}
	}
	/************************************************************/
							#CONTACTO
	/************************************************************/
	public function contacto_get()
	{
		$tabla = 'contacto';
		$resultado = $this->model->model_get($tabla, '1');
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontraron los datos'), 404);
		}
	}
	public function contacto_put($id)
	{
		if(!$this->put('contacto') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$contacto = $this->put('contacto');
			$post =array(
						'id'			=>	$id,
						'email' 		=>  $contacto['email'],
						'telefono' 		=>  $contacto['telefono'],
						'direccion'		=>  $contacto['direccion'],
						'gmaps'			=>  $contacto['gmaps']
						);
			$this->respuesta('put', $this->model->update('contacto', $post));
		}

	}
	
	/************************************************************/
							#REDES
	/************************************************************/
	public function redes_get()
	{
		$tabla = 'redes_sociales';
		$resultado = $this->model->model_get($tabla, '1');
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontraron los datos'), 404);
		}
	}
	public function redes_put($id)
	{
		if(!$this->put('redes') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$contacto = $this->put('redes');
			$post =array(
						'id'			=>	$id,
						'facebook' 		=>  $contacto['facebook'],
						'twitter' 		=>  $contacto['twitter'],
						'youtube'		=>  $contacto['youtube'],
						'googleplus'	=>  $contacto['googleplus'],
						'instagram'		=>	$contacto['instagram']
						);
			$this->respuesta('put', $this->model->update('redes_sociales', $post));
		}

	}
	/************************************************************/
								#BLOG
	/************************************************************/

	public function blog_get($id = NULL)
	{
		$tabla = 'blog_entradas';
		$resultado = $this->model->model_get($tabla, $id);
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 404);
		}
	}
	public function blog_post()
	{
		if(!$this->post('blog'))
		{
			$this->response(NULL, 400);
		}
		else
		{
			$blog = $this->post('blog');
			$permalink = $this->clean_string->permalink($blog['title']);
			$post =array(
						'permalink' 	=> $permalink,
						'author' 		=> $blog['author'],
						'title' 		=> $blog['title'],
						'description' 	=> $blog['description'],
						'intro'			=> $blog['intro'],
						'content'		=> $blog['content'],
						'img'			=> $blog['img1'].', '.$blog['img2'],
						'date'			=> date('Y-m-d'),
						'tags'			=> $blog['tags']
						);
				$this->respuesta('post', $this->model->save('blog_entradas', $post));
		}
		
	}
	public function blog_put($id)
	{
		if(!$this->put('blog') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$blog = $this->put('blog');
			$permalink = $this->clean_string->permalink($blog['title']);
			$post =array(
						'id'			=> $id,
						'permalink' 	=> $permalink,
						'author' 		=> $blog['author'],
						'title' 		=> $blog['title'],
						'description' 	=> $blog['description'],
						'intro'			=> $blog['intro'],
						'content'		=> $blog['content'],
						'img'			=> $blog['img1'].', '.$blog['img2'],
						'date'			=> date('Y-m-d'),
						'tags'			=> $blog['tags']
						);
			$this->respuesta('put', $this->model->update('blog_entradas', $post));
		}

	}
	public function blog_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$this->respuesta('delete',  $this->model->delete('blog_entradas', 'id', $id));
		}
	}
	
	/************************************************************/
								#TESTIMONIOS
	/************************************************************/

	public function coment_get($id = NULL)
	{
		$tabla = 'coment_customer';
		$resultado = $this->model->model_get($tabla, $id);
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 404);
		}
	}
	public function coment_put($id)
	{
		if($this->model->intercambiar($id))
		{
			$this->response(array('response'=>'Se ha modificado el comentario'), 200);
		}
		else
		{
			$this->response(array('error'=>'Se ha desactivado el comentario'), 404);
		}
	}
	public function coment_post()
	{
		if(!$this->post('coment'))
		{
			$this->response(array('error'=>'No se pudo realizar la operacion, recargue la pagina e intente de nuevo'), 200);
		}
		else
		{
			$coment = $this->post('coment');
			switch($coment['tipoventa'])
			{
				case "productos":
					$id = $coment['productos'];
					$items = $this->model->inventario($id);
					$venta = 	'{"codigo" : "'.$items['i_cod'].'",'.
								'"control" : "'.$items['ctrl_nombre'].'",'.
								'"categoria":"'.$items['c_nombre'].'",'.
								'"marca" :"'.$items['m_nombre'].'",'.
								'"nombre":"'.$items['i_nombre'].'",'.
								'"img":"'.$items['p_1'].'",'.
								'"cantidad":"'.$coment['cantidad'].'",'.
								'"clasificacion":"'.$items['i_clasificacion'].'",'.
								'"condicion":"'.$items['i_condicion'].'",'.
								'"fecha":"'.date('Y-m-d').'"}';
				break;
				case "servicios":
					$venta = $coment['servicios'];
				break;
			}
			$coment['key'] = $this->keygen();
			$post =array(
						'email' 	=> $coment['email'],
						'tipoventa' => $coment['tipoventa'],
						'venta' 	=> $venta,
						'clave'		=> $coment['key']
						);
			if($coment['id'] = $this->model->save('coment_customer', $post))
			{
				$coment['asunto'] = 'Comentanos tu experiencia comprando en Saenca';
				if($this->send_mail($coment, 'testimonio'))
				{
					$this->response(array('response'=>'Se ha enviado la solicitud de calificacion al email '.$coment['email']), 200);
				}
				else
				{
					$this->coment_delete($coment['id']);
					$this->response(array('error'=>'No se pudo enviar la solicitud a '.$coment['email'].' intente mas tarde'), 404);
				}
			}
			else
			{
				$this->response(array('error'=>'No se pudo enviar la solicitud a '.$coment['email'].' intente mas tarde'), 404);
			}
		}
		
	}
	
	private function send_mail($data, $view)
	{
		$this->load->library('email');
		
		$this->email->from(DEFAULT_EMAIL, NOMBRE_EMPRESA);
		//$this->email->reply_to($email, $nombre);
		$this->email->to($data['email']);
		//$this->email->cc('lavictoria3021@gmail.com'); 
		$this->email->bcc(SECOND_EMAIL); 
		$this->email->subject($data['asunto']);
		$this->email->message($this->load->view("$view", $data, true));
		if($this->email->send())
		{
			return TRUE;
		}
		return FALSE;
	}
	
	public function coment_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			if($this->model->delete('coment_customer', 'id', $id))
			{
				$this->model->delete('coment_reply', 'cc_id', $id);
				$this->respuesta('delete',  TRUE);
			}
		}
	}
	
	/************************************************************/
							#APARIENCIA
	/************************************************************/
	
	#Sliders
	public function sliders_get($id = NULL)
	{
		$resultado = $this->model->model_get('slider', $id);
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 200);
		}
	}
	public function sliders_post()
	{
		$slider = $this->post('slider');
		if(!is_null($slider))
		{
			if($this->model->vaciar('slider'))
			{
					foreach($slider as $key => $img)
					{
						$post =array('nombre'=>$img);
						if($success = $this->model->save('slider', $post))
						{
							continue;
						}
						else
						{
							return $this->response(array('error'=>'Hubo un error guardando las imagenes, intente de nuevo'), 200);
						}
					}
					$this->respuesta('put', TRUE);
			}
		}
	}
	#Logo
	public function logo_get($id = NULL)
	{
		$resultado = $this->model->model_get('img_logo', '1');
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 404);
		}
	}
	public function logo_put($id)
	{
		$logo = $this->put('logo');
		$post =array(
				'id'		=> $id,
				'archivo' 	=> $logo['archivo']
			);
		$this->respuesta('put', $this->model->update('img_logo', $post));
	}
	#Emergente
	public function emergente_get($id = NULL)
	{
		$resultado = $this->model->model_get('img_emergente', '1');
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 404);
		}
	}
	public function emergente_put()
	{
		$logo = $this->put('slider');
		$post =array(
				'id'		=> $logo['id'],
				'archivo' 	=> $logo['archivo']
			);
		$this->respuesta('put', $this->model->update('img_emergente', $post));
	}
	/************************************************************/
							#CUPONES
	/************************************************************/
	public function cupon_get($id = NULL)
	{
		$resultado = $this->model->cupones($id);
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se encontro la entrada'), 404);
		}
	}
	public function cupon_post()
	{
		if(!$this->post('cupon'))
		{
			$this->response(NULL, 400);
		}
		else
		{
			$cupon = $this->post('cupon');
			if(isset($cupon['cu_modo']) && $cupon['cu_modo'] > 0)
			{
				if($cupon['cu_modo'] < 3 && $cupon['cu_monto'] > 0)
				{
					if(isset($cupon['cu_code']) && $cupon['cu_code'] != '')
					{
						$post =array(
								'cu_promo' 	=> $cupon['cu_promo'],
								'cu_img' 	=> isset($cupon['cu_img']) ? $cupon['cu_img'] : NULL,
								'cu_code' 	=> strtoupper($cupon['cu_code']),
								'cu_modo' 	=> $cupon['cu_modo'],
								'cu_monto'	=> $cupon['cu_monto'],
								'cu_usos'	=> $cupon['cu_usos'],
								'cu_caduca'	=> $cupon['cu_caduca'],
								'cu_fecha_creacion'	=> date('Y-m-d'),
								'cu_activo'	=> $cupon['cu_activo']
								);
						$this->respuesta('post', $this->model->save('cupones', $post));
					}
				}
				elseif($cupon['cu_modo'] > 2 && isset($cupon['cu_code']) && $cupon['cu_code'] != '')
				{
					$post =array(
								'cu_promo' 	=> $cupon['cu_promo'],
								'cu_img' 	=> isset($cupon['cu_img']) ? $cupon['cu_img'] : NULL,
								'cu_code' 	=> strtoupper($cupon['cu_code']),
								'cu_modo' 	=> $cupon['cu_modo'],
								'cu_monto'	=> NULL,
								'cu_usos'	=> $cupon['cu_usos'],
								'cu_caduca'	=> $cupon['cu_caduca'],
								'cu_fecha_creacion'	=> date('Y-m-d'),
								'cu_activo'	=> $cupon['cu_activo']
								);
					$this->respuesta('post', $this->model->save('cupones', $post));
				}
				else
				{
					return $this->response(array('error'=>'Debe indicar un <b>Modo Cupon</b> o un <b>Valor/Monto*</b> para el cupon'), 200);
				}
			}
			else
			{
				return $this->response(array('error'=>'Debe indicar un <b>Modo Cupon*</b> para el cupon'), 200);
			}
		}
	}
	public function cupon_put()
	{
		echo 'p';	
	}
	public function cupon_delete()
	{
		echo 'p';
	}
	/************************************************************/
							#USUARIOS
	/************************************************************/
	public function tipouser_get()
	{
		switch ($this->session->userdata('perfil')) {
			case '0':
					$this->response(array('tipousu'=>array(
						array('id'=>'0', 'tipo'=>'Programador'), 
						array('id'=>'1', 'tipo'=>'Administrador'), 
						array('id'=>'2', 'tipo'=>'Usuario')
						)));
					exit();
			break;
			case '1':
					$this->response(array('tipousu'=>array(
						array('id'=>'1', 'tipo'=>'Administrador'), 
						array('id'=>'2', 'tipo'=>'Usuario')
						)));
					exit();
			break;
			default:
				return FALSE;
			break;
		}
	}
	public function usuarios_get($id = NULL)
	{
		$tabla = 'users';
		$resultado = $this->model->users_get($id);
		if(!is_null($resultado))
		{
			$this->response(array('response'=>$resultado), 200);
		}
		else
		{
			$this->response(array('error'=>'No se el usuario'), 404);
		}
	}
	public function usuarios_post()
	{
		if(!$this->post('users'))
		{
			$this->response(NULL, 400);
		}
		else
		{
			$user = $this->post('users');
			if(!isset($user['pass']) || !isset($user['passconfirm']))
			{
				return  $this->response(array('error'=>'Las contraseñas no coinciden!'), 200);
			}
			else
			if($user['pass'] != $user['passconfirm'])
			{
				return  $this->response(array('error'=>'Las contraseñas no coinciden!'), 200);
			}
			else
			{
				$pass = sha1($user['pass']);
				$post =array(
						'user'		=> $user['user'],
						'email' 	=> $user['email'],
						'pass' 		=> $pass,
						'perfil'	=> $user['tipo'],
						'activo'	=> $user['activo']
						);
				$this->respuesta('post', $this->model->save('users', $post));
			}
		}
	}
	public function usuarios_put($id)
	{
		if(!$this->put('users') || !$id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			$user = $this->put('users');
			if(!isset($user['pass']) || !isset($user['passconfirm']))
			{
				return  $this->response(array('error'=>'Las contraseñas no coinciden!'), 200);
			}
			else
			if($user['pass'] != $user['passconfirm'])
			{
				return  $this->response(array('error'=>'Las contraseñas no coinciden!'), 200);
			}
			$pass = sha1($user['pass']);
			$post =array(
						'id'		=> $user['id'],
						'user'		=> $user['user'],
						'email' 	=> $user['email'],
						'pass' 		=> $pass,
						'perfil'	=> $user['perfil'],
						'activo'	=> $user['activo']
						);
			$this->respuesta('put', $this->model->update('users', $post));
		}

	}
	public function usuarios_delete($id)
	{
		if(NULL === $id)
		{
			$this->response(NULL, 404);
		}
		else
		{
			if($resultado = $this->model->ocupado('users', $id))
			{
				$this->response(array('error'=>$resultado), 200);
			}
			else
			{
				$this->respuesta('delete',  $this->model->delete('users', 'id', $id));
			}
		}
	}
	
	/********************************************************************/
	public function index_get($page='panel/admin_view', $options='')
	{
		if($this->session->userdata('perfil') == NULL)
		{
			redirect(base_url());
		}
		$header = array(
			'titulo'		=> 'Y-Panel'
			);
		$this->load->view('panel');
	}
	
	private function keygen($length=10)
	{
		$key = '';
		list($usec, $sec) = explode(' ', microtime());
		mt_srand((float) $sec + ((float) $usec * 100000));
		
		$inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));
	
		for($i=0; $i<$length; $i++)
		{
			$key .= $inputs{mt_rand(0,61)};
		}
		return $key;
	}
}