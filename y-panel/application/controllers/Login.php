<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('login_model');
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form','security'));
    }
	
	public function index()
	{	
		switch ($this->session->userdata('perfil')) {
			//2 = Usuario
			//1 = Administrador
			//0 = Programador
			case '0':
			case '1':
			case '2':
				redirect(base_url('admin'));
			break;
			default:
				if($this->session->userdata('token'))
				{
					$this->session->unset_userdata('token');
				}
				$this->load->view('login_view');
			break;		
		}
	}
	
	
	public function new_user()
	{
		if(isset($_SESSION['time']))
		{
			$this->output->set_status_header('403');
			exit();	
		}
		if($this->input->post('token') && ($this->input->post('token') == $this->session->userdata('token')))
		{
			$this->form_validation->set_rules('username','nombre de usuario','required|trim|min_length[3]');
            $this->form_validation->set_rules('password', 'password', 'required|trim|min_length[8]|max_length[15]');
            //lanzamos mensajes de error si es que los hay
            $this->form_validation->set_message('required', 'El {field} es requerido');
            $this->form_validation->set_message('min_length', 'El {field} debe tener minimo {param} carácteres');
            $this->form_validation->set_message('max_length', 'El {field} debe tener maximo {param} carácteres');
			if($this->form_validation->run() === FALSE)
			{
				$this->session->unset_userdata('token');
				$this->output->set_status_header('401');
				echo validation_errors();

			}else{
				$user = $this->input->post('username');
				$pass = sha1($this->input->post('password'));
				if($check_user = $this->login_model->login_user($user,$pass))
				{
					$data = array(
	                	'is_logued_in' 	=> 		TRUE,
	                	'id'		 	=> 		$check_user->id,
	                	'perfil'		=>		$check_user->perfil,
	                	'email' 		=> 		$check_user->email
            			);		
					$this->session->set_userdata($data);
					/*$this->index();*/
					$this->output->set_status_header('200');
					$this->output->set_content_type('application/json');
					echo json_encode(array('response' => 'Logueado! Redirigiendo...'));
				}
				else
				{
					$this->session->unset_userdata('token');
					$this->output->set_status_header('401');
					echo 'Usuario o Contraseña incorrecto';
				}
			}
		}else{
			$this->session->unset_userdata('token');
			$this->output->set_status_header('400');
			echo 'Recargue la pagina e intente de nuevo';
		}
	}
	
	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		echo $token;
	}
	
	public function trata()
	{
		echo $this->login_model->get_intentos();
	}
	
	public function end_timer()
	{
		$this->session->sess_destroy();
	}
	
	public function logout_ci()
	{
		if($this->session->sess_destroy())
		{
			$this->output->set_status_header('200');
		}
	}
}
