<?php
function creathumb($img)
{
	if($img!='' && $img!=NULL)
	{
/*		$divide_img = explode('.',$img);
		$thumb = $divide_img[0].'_thumb.'.$divide_img[1];
		$imgfile = '<img src="'.ROOT.'img/articulos/thumbs/'.$thumb.'" />';*/
		$imgfile = '<img src="'.$img.'" class="img-responsive"/>';
	}
	else
	{
		$imgfile ='<img src=""/>';
	}
	return $imgfile;
}
?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Resumen
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Resumen</li>
    </ol>
  </section>
  <section class="content">
  <div class="row">
	<div class="col-sm-12 main">
          <div class="row">
          <!-- PRODUCT LIST -->
          <div class="col-xs-6 col-sm-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <?php
              		if(isset($inventario) && !empty($inventario))
			  		{
			  		?>
                  <h3 class="box-title">Productos Recientes</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                  
                   <?php
					  $i=0;
					  $j=0;
					  $newinventario = array_reverse($inventario);
					  foreach($newinventario as $val)
					  {
						  ?>
                          <li class="item">
                          <div class="product-img">
                           <?=creathumb($val['img'.(1)])?>
                           </div>
                           <div class="product-info">
                           <a href="<?=base_url().'admin/select_articulo/'.$val['id_articulo']?>" class="product-title"><?=$val['marcas'].' - '.$val['nombre']?><span class="label label-success pull-right">Bs <?=$val['precio']?></span></a>
                            <span class="product-description">
                          <?=$val['descripcion']?>
                         </span>
                       	 </div>
                   		 </li><!-- /.item -->
                          <?php
						  if($val['activo']==1)
						  {
							  $j++;
						  }
						  $i++;
					  }
					  ?>
                  </ul>
                  <?php
					}
				  ?>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="<?=base_url()?>admin/inventario" class="uppercase">Ver todos los productos</a>
                </div><!-- /.box-footer -->
              </div>
           <!-- /.box -->
          
 
          	</div>
<!--                          <?php
                      if(isset($visitas) && !empty($visitas))
                      {
					 unset($val);
					 $cantidad=0;
					 $codigo = array(
									1=>0,
									2=>0,
									3=>0,
									4=>0,
									5=>0
									);
					   foreach($visitas as $val)
					   {
						   if($val['visitas'] > $cantidad)
						   {
							   $cantidad = $val['visitas'];
							   $seccion = $val['codigo'];
						   }
						   $codigo[$val['codigo']]+=$val['visitas'];
					   }
					}
					$paginas = array(
									  1=>'Inicio',
									  2=>'Contacto',
									  3=>'Servicios',
									  4=>'Productos'
									  );
					?>-->
            <div class="col-xs-6 col-sm-6">
             <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion-person-stalker"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Visitas</span>
                  <span class="info-box-number"><?=!empty($codigo)?array_sum($codigo):'';?></span>
                  <span class="progress-description">
                    20% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            
				              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-eye"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Secci&oacute;n m&aacute;s vista</span>
                  <span class="info-box-number"><!--<?=!empty($visitas)?$paginas[$seccion].' - '.str_replace('0','',$cantidad):''?>--></span>
                  <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
          </div>
            <div class="col-xs-6 col-sm-6">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Productos m&aacute;s Visitados</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Cod.</th>
                          <th>Nombre</th>
                          <th>Status</th>
                          <th>Visitas</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR9842</a></td>
                          <td>Call of Duty IV</td>
                          <td><span class="label label-success">Shipped</span></td>
                          <td><div class="sparkbar" data-color="#00a65a" data-height="20"><canvas width="34" height="20" style="display: inline-block; width: 34px; height: 20px; vertical-align: top;"></canvas></div></td>
                        </tr>
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                </div><!-- /.box-footer -->
              </div>
          </div>
            <div class="col-xs-6 col-sm-6">
            <?php if ($this->session->userdata('perfil')=='admin'){ ?>
            
            <!-- TABLE: LATEST ACCESS -->
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Ultimos Accesos</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
              	<?php
              		if(isset($accesos))
             		{
					?>
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Usuario</th>
                          <th>Direccion IP</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
					  foreach($accesos as $val)
				  	  {
					  ?>
                        <tr>
                          <td>
                          <a href="pages/examples/invoice.html">
						  <?=$val['user'].'</b> (<em>'.$val['firstname'].'&nbsp;'.$val['lastname'].'</em>)'?>
                          </a>
                          </td>
                          <td><?=$val['ip']?></td>
                          <td><?=date_format(date_create($val['fecha']), 'd-m-Y H:m:sa')?></td>
                        </tr>
                        <?php
						}
			  			?>
                      </tbody>
                    </table>
                    <?php
					}
					?>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			 <?php }?>
            </div><!-- /.col -->
            </div>
           </div>
           </div>
           </section>
           </div>