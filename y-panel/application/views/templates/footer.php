<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
  </div>
  <strong>Copyright © <a href="http://lavictoria3021.com.ve">LaVictoria3021</a> - <?=date('Y')?></strong> All rights reserved.
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/2.5.1/jquery-confirm.min.js"></script>
<script src="<?=ROOT?>js/app.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=ROOT?>js/animatescroll.js"></script>
<?php
if(isset($jquery)&&(!empty($jquery)))
	{
		foreach($jquery as $val)
		{
			if($val!='' || $val!=NULL || is_int($val))
			{
				echo '<script type="text/javascript" src="'.ROOT.'js/'.$val.'"></script>';
			}
		}
	}
?>
<script type="text/javascript">
$(document).ready(function (){
	$('.theform').on('change', function(){
		$('input[type=submit]').removeAttr('disabled');
		});
	
	jQuery("#up").hide();
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 200) {
				jQuery('#up').fadeIn('slow');
			}else{
				jQuery('#up').fadeOut('slow');
			}
		});
	$('#up').on('click', function(){
	$('header').animatescroll({scrollSpeed:3000})
	});
});
</script>
<script type="text/javascript">
	function confirmacion($id, $url){
		
		var elem = $(this).closest('.item');
		
		$.confirm({
			'title'		: 'Confirmación de Borrado',
			'message'	: 'Esta seguro de borrar este item?<br />Una vez borrado no podra restaurarse!',
			'buttons'	: {
				'Si'	: {
					'class'	: 'btn btn-lg btn-default',
					'action': function(){
						document.location.href ='<?=base_url()?>admin/'+$url+'/'+$id;
					}
				},
				'No'	: {
					'class'	: 'btn btn-lg btn-danger',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
				}
			}
		});
		
	}
</script>
<script>
  $(function () {
	$('#tabla').DataTable();
  });
</script>
</body>
</html>