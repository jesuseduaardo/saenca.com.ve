<aside class="main-sidebar">
<section class="sidebar">
 <div class="user-panel">
     <div class="pull-left info">
     	<h4><?=$this->session->userdata('user')?></h4>
      </div>
     </div>
       <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php
			$activo = 'active';
			?>
             <li class="<?=($seccion=='inicio')?$activo:''?>">
			 <a href="<?=base_url().'admin/index/'?>"><i class="fa fa-home"></i><span>Inicio</span></a>
              </li>
            <li class="treeview <?=($seccion=='inventario')?$activo:''?>">
                  <a href="#">
                    <i class="fa fa-check-square-o"></i><span>Inventario</span><i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?=base_url().'admin/inventario/'?>"><i class="fa fa-circle-o"></i>Ver Inventario</a></li>
                    <li><a href="<?=base_url().'admin/nuevoarticulo/'?>"><i class="fa fa-circle-o"></i>Agregar Articulo</a></li>
                    <li><a href="<?=base_url().'admin/marcas/'?>"><i class="fa fa-circle-o"></i>Marcas</a></li>
                    <li><a href="<?=base_url().'admin/categorias/'?>"><i class="fa fa-circle-o"></i>Categorias</a></li>
                  </ul>
            </li>
            <li class="treeview <?=($seccion=='contacto')?$activo:''?>">
              <a href="#">
                <i class="fa fa-commenting-o"></i><span>Contacto</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url().'contacto/'?>"><i class="fa fa-circle-o"></i>Informaci&oacute;n de Contacto</a></li>
                <li><a href="<?=base_url().'contacto/redes'?>"><i class="fa fa-circle-o"></i>Redes Sociales</a></li>
              </ul>
            </li>
            <li class="treeview <?=($seccion=='blog')?$activo:''?>">
              <a href="#">
                <i class="fa fa-files-o"></i><span>Blog</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url().'admin/blog/'?>"><i class="fa fa-circle-o"></i>Blog</a></li>
                <li><a href="<?=base_url().'admin/blog/nuevopost'?>"><i class="fa fa-circle-o"></i>Nueva Entrada</a></li>
              </ul>
            </li>
            <li class="treeview <?=($seccion=='apariencia')?$activo:''?>">
              <a href="#">
                <i class="fa fa-eye"></i><span>Apariencia</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url().'admin/apariencia/slider'?>"><i class="fa fa-circle-o"></i>Carousel</a></li>
              </ul>
            </li>
             <li class="<?=($seccion=='trafico')?$activo:''?>">
			 <a href="<?=base_url().'admin/trafico/'?>"><i class="fa fa-bar-chart"></i><span>Estadisticas del Sitio</span></a>
              </li>
            <?php if ($this->session->userdata('perfil')=='admin'){ ?>
            <li class="treeview <?=($seccion=='usuarios')?$activo:''?>">
                  <a href="#">
                    <i class="fa fa-users"></i><span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?=base_url().'admin/usuarios/'?>"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                    <li><a href="<?=base_url().'admin/usuarios/nuevousuario/'?>"><i class="fa fa-circle-o"></i>Agregar Usuario</a></li>
                  </ul>
            </li>
            <?php }?>
          </ul>         
</section>
</aside>