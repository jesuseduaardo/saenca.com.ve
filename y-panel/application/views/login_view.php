<!doctype html>
<html ng-app="app">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Y-Panel | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate"/>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <!--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <script src="js/jQuery-2.1.4.min.js"></script>
  <script src="js/angular.min.js"></script>
  <script src="js/angular-route.min.js"></script>
  <script src="js/angular-resource.min.js"></script>
  <script src="js/angular-cookies.min.js"></script>
  <script src="js/routes.js"></script>
  <style>
  	.login-page{background: #ffffff;}
	.login-box-head{
		height:50px;
		}
	.loading{
	position: absolute;
    background-color:rgba(255,255,255, 1);
	background-image:url(images/loading.gif);
	background-repeat:no-repeat;
	background-position: 35% 15%;
    width: 100%;
    height: 100%;
    z-index: 999;
	}
	.loading:after{
		display: block;
		content: 'Cargando...';
		position: relative;
		top: 36%;
		overflow: hidden;
		left: 34%;
		}
	.loading.ng-enter, .loading.ng-leave {
	  transition:all cubic-bezier(0.250, 0.460, 0.450, 0.940) 0.5s;
	}
	
	.loading.ng-enter,
	.loading.ng-leave.ng-leave-active {
	  opacity:0;
	}
	
	.loading.ng-leave,
	.loading.ng-enter.ng-enter-active {
	  opacity:1;
	}
  </style>
</head>
<body>
<div class="container">
<!--Cargando-->
<div class="loading"></div>
<!--/Cargando-->
<?php
if(isset($_SESSION['time']))
{
	$segundos = (strtotime('now') - $_SESSION['time']) * 1000;
}
include('pages/login-form.php');
?>
</div>
<footer class="footer">
    <div class="container">
    	<p class="text-muted" style="text-align:center">&copy;La Victoria 3021 - <?=date('Y')?></p>
    </div>
</footer>
<script src="js/login.js"></script>
<script src="js/jquery.runner-min.js"></script>
<?php
if(isset($_SESSION['time']))
{
?>

<script type="text/javascript">
	$('#submit').runner({
		autostart: true,
		countdown: true,
		startAt: <?=$segundos?>,
		stopAt: 0,
		miliseconds : false
	})
	.on('runnerFinish', function(){
		$('#submit').val('');
		$.get("/login/timer", function(){
				location.reload();
			});
		
	});
</script>
<?php
}
?>
</body>
</html>