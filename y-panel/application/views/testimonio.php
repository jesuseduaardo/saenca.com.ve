<html>
<head>
<meta charset="utf-8">
<title>Documento sin título</title>
</head>
<body>
<table width="800" border="0" align="center">
	<thead>
    	<tr>
			<th><img src="LOGO" alt="<?=NOMBRE_EMPRESA?>"/></th>    
    	</tr>
  	</thead>
  	<tbody>
    	<tr>
      		<td>
            	<h1><b><?=isset($nombre)?$nombre:''?></b>&nbsp;¡Queremos conocer tu experiencia en <?=NOMBRE_EMPRESA?>!</h1>
                <?php
                if($tipoventa=='servicios')
				{
				?>
                	<p>Te invitamos a dar tus impresiones sobre nuestro servicio atraves del siguiente enlace</p>
                <?php
				}
				if($tipoventa=='productos')
				{
				?>    
                	<p>Te invitamos a dar tus impresiones sobre tu compra atraves del siguiente enlace: </p>
                <?php
				}
				?>
                <p><a href="<?=PAGINA_WEB.'comentario/'.$id.'/'.$key?>"><?=PAGINA_WEB.'comentario/'.$id.'/'.$key?></a></p>
                <p>Es de gran ayuda para nosotros y Tomara menos de un minuto!</p>
            </td>
    	</tr>
        <?php
        if(isset($comentarios) && $comentarios !== NULL)
		{
		?>
        <tr>
        	<td>
            <blockquote>
            <?=$comentarios?>
            <h4>- Saul Castillo - Presidente en <?=NOMBRE_EMPRESA?> -</h4>
            </blockquote>
            </td>
        </tr>
        <?php
		}
		?>
  	</tbody>
    <tfoot>
    	<tr>
        	<td>&nbsp;</td>
        </tr>
    </tfoot>
</table>
</body>
</html>