<aside class="main-sidebar">
<section class="sidebar">
 <div class="user-panel">
     <div class="pull-left info">
     	<h4>Usuario</h4>
      </div>
     </div>
       <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
             <li class="activo">
			 <a href="#/"><i class="fa fa-home"></i><span>Inicio</span></a>
              </li>
            <!--<li class="">
			 <a class="choice"><i class="fa fa-bar-chart"></i><span>Estadisticas del Sitio</span></a>
            </li>-->
            <li class="activo">
				<a href="#/pedidos"><i class="fa fa-shopping-cart"></i><span>Pedidos</span></a>
              </li>
            <li class="treeview">
                  <a class="choice">
                    <i class="fa fa-check-square-o"></i><span>Inventario</span><i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#/inventario"><i class="fa fa-circle-o"></i>Ver Inventario</a></li>
                    <li><a href="#/marcas"><i class="fa fa-circle-o"></i>Marcas</a></li>
                    <li><a href="#/categorias"><i class="fa fa-circle-o"></i>Categorias</a></li>
                  </ul>
            </li>
            <li class="treeview">
                  <a class="choice">
                    <i class="fa fa-check-square-o"></i><span>Cupones</span><i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#/cupon/nuevo"><i class="fa fa-circle-o"></i>Nuevo Cup&oacute;n</a></li>
                    <li><a href="#/cupon"><i class="fa fa-circle-o"></i>Cupones</a></li>
                  </ul>
            </li>
            <li class="treeview">
              <a class="choice">
                <i class="fa fa-commenting-o"></i><span>Contacto</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#/contacto"><i class="fa fa-circle-o"></i>Contacto</a></li>
                <li><a href="#/redes"><i class="fa fa-circle-o"></i>Redes Sociales</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a class="choice">
                <i class="fa fa-rss-square"></i><span>Blog</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#/blog"><i class="fa fa-circle-o"></i>Articulos</a></li>
                <li><a href="#/blog/nuevo"><i class="fa fa-circle-o"></i>Nuevo articulo</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a class="choice">
                <i class="fa fa-quote-right"></i><span>Calificaciones</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#/testimonios"><i class="fa fa-circle-o"></i>Ver Calificaciones</a></li>
                <li><a href="#/testimonios/solicitar"><i class="fa fa-circle-o"></i>Solicitar Calificacion</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a class="choice">
                <i class="fa fa-eye"></i><span>Apariencia</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#/carrusel"><i class="fa fa-circle-o"></i>Carrusel</a></li>
                <!--<li><a href="#/logo"><i class="fa fa-circle-o"></i>Logo</a></li>-->
                <li><a href="#/emergente"><i class="fa fa-circle-o"></i>Modal de inicio</a></li>
              </ul>
            </li>
            <?php if($this->session->userdata('perfil') < 2) { ?>
            <li class="treeview">
                  <a class="choice">
                    <i class="fa fa-users"></i><span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#/usuarios"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                    <li><a href="#/usuario"><i class="fa fa-circle-o"></i>Agregar Usuario</a></li>
                  </ul>
            </li>
            <?php } ?>
          </ul>         
</section>
</aside>