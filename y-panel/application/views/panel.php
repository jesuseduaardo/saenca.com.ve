<!doctype html>
<html ng-app="app">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate"/>
<title>Y-Panel</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css">
<link href="css/_all-skins.min.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/alertify.default.css" rel="stylesheet" type="text/css">
<link href="css/panel.css" rel="stylesheet" type="text/css">
<link href="css/printver.css" rel="stylesheet" type="text/css" media="print">
<script src="js/jQuery-2.1.4.min.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-route.min.js"></script>
<script src="js/angular-resource.min.js"></script>
<script src="js/angular-cookies.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/angular-animate.min.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
     <header class="main-header" ng-controller="PanelCtrl">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Y</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Y-<b>Panel</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a class="sidebar-toggle choice" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
              <a class="dropdown-toggle choice" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">Ud Tiene 10 notificaciones</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">Ver Todas</a></li>
              </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li>
              <a class="choice" ng-really-message="Se cerrara la sessi&oacute;n en curso" ng-really-click="desactivate()">Salir</a>
           </li>
          </ul>
        </div>
      </nav>
    </header>
        <?php $this->load->view('sidebar')?>
    	<div ng-view></div>
     <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
      </div>
      <strong>Copyright © <a href="http://lavictoria3021.com.ve">LaVictoria3021</a> - 2016</strong> All rights reserved.
    </footer>
    </div>
<script src="js/bootstrap.min.js"></script>
<script src="js/routes.js"></script>
<script src="js/app.js"></script>
<!--Controllers-->
<script src="js/controllers/ordersctrl.js"></script>
<script src="js/controllers/itemctrl.js"></script>
<script src="js/controllers/categctrl.js"></script>
<script src="js/controllers/marcactrl.js"></script>
<script src="js/controllers/contactctrl.js"></script>
<script src="js/controllers/blogctrl.js"></script>
<script src="js/controllers/testctrl.js"></script>
<script src="js/controllers/desingctrl.js"></script>
<script src="js/controllers/cuponctrl.js"></script>
<!--/Controllers-->
<script src="js/upload.js"></script>
<script src="js/app.min.js"></script>
<script src="js/alertify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="js/animatescroll.min.js"></script>
<script src="js/html2canvas.js"></script>
<script src="js/canvas2image.js"></script>
<script type="text/javascript">
$(document).ready(function (){
	
	alertify.set({ 
		labels: {ok: "Aceptar", cancel:"Cancelar"},
	 	buttonFocus: "cancel",
		buttonReverse: true
		});

	$('.theform').on('change', function(){
		$('input[type=submit]').removeAttr('disabled');
		});
	
	jQuery("#up").hide();
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 200) {
				jQuery('#up').fadeIn('slow');
			}else{
				jQuery('#up').fadeOut('slow');
			}
		});
	$('#up').on('click', function(){
	$('header').animatescroll({scrollSpeed:3000})
	});
	
	function desactivate(){
	$.get( "admin/salida")
	  .done(function() {
		location.reload();
	  })
	}
});
</script>
</body>
</html>