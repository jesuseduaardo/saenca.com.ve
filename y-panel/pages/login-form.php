<div class="login-box">
    <div class="login-logo">
      <b>Y</b>-Panel
    </div><!-- /.login-logo -->
    <div class="login-box-head">
    
    </div>
    <div class="login-box-body">
    <?php
    $attributes = array(
						'name'	=> 'loginUserForm',	
						'class' => 'form-signin',
						'id' 	=> 'login-form'
						);
    echo form_open('', $attributes)
	?>
        <div class="form-group has-feedback">
          <input type="email" name="username" class="form-control" placeholder="Email" ng-model="user.email" <?=isset($_SESSION['time']) ? 'disabled="disabled"':'autofocus'?> required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Contraseña" ng-model="user.password" <?=isset($_SESSION['time']) ? 'disabled="disabled"':''?> required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="col-xs-12">
            <button type="submit" id="send" ng-disabled="!loginUserForm.$valid" <?=isset($_SESSION['time']) ? 'disabled="disabled"':''?> class="btn btn-primary btn-block btn-flat"><?=isset($_SESSION['time']) ?"Intentando de nuevo en <span id='submit'></span>":"Entrar" ?></button>
          </div><!-- /.col -->
        <input type="hidden" name="token" id="token">
	    <?=form_close()?>
	  	<div style="padding-top:75px;text-align:center;margin:auto;">
      		<a href="#">Olvide mi contrase&ntilde;a</a>
        </div>
    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->