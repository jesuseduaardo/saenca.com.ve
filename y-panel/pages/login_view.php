<!DOCTYPE html>
	<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Y-Panel</title>
        <link rel="shortcut icon" href="../login/img/controlPanel.ico">
        <link rel="stylesheet" type="text/css" href="<?=CBOOTSTRAP?>"/>
        <link rel="stylesheet" type="text/css" href="css/login.css"/>
        <script src="<?=JQUERY?>"></script>
	</head>
	<body>
	<?php
	$username = array('name' => 'username', 'placeholder' => 'Usuario', 'class'=>'form-control', 'required'=>'', 'autofocus'=>'');
	$password = array('name' => 'password',	'placeholder' => 'Password', 'id' => 'inputPassword', 'class'=>'form-control', 'required'=>'');
	$submit = array('name' => 'submit', 'value' => 'Iniciar sesión', 'title' => 'Iniciar sesión', 'id'=>'boton2', 'class'=> 'btn btn-lg btn-primary btn-block');
	$button = array('value'=>'Iniciar sesión', 'content'=> 'Iniciar sesión', 'disabled'=>'disabled', 'class'=> 'btn btn-lg btn-primary btn-block');
	?>
    <div class="container">
     <?php 
	  if(isset($_SESSION['time']))
	  {
		  $segundos = strtotime('now') - $_SESSION['time'];
		  $segundos = 90-intval($segundos);
	  ?>
	  <div style="text-align:center;" class="alert alert-danger" role="alert">
	  <strong>Error!</strong>&nbsp;Espere <span id="timerText"></span>seg para intentar nuevamente
	  </div>
	  <p class="error"></p>
	  <?php
	  }
	  ?>
    <?php 
	  if($this->session->flashdata('usuario_incorrecto'))
	  {
	  ?>
	  <div style="text-align:center;" class="alert alert-danger" role="alert">
	  <strong>Error!</strong>&nbsp;<?=$this->session->flashdata('usuario_incorrecto')?>
	  </div>
	  <p class="error"></p>
	  <?php
	  }
	  ?>
        <h2 class="form-signin-heading" style="text-align:center">Y-Panel</h2>
					<?=form_open(base_url().'login/new_user', 'class="form-signin"')?>
 					<label for="username" class="sr-only">Nombre de usuario</label>
					<?=form_input($username)?><?=form_error('username')?>
					<label for="password" class="sr-only">Introduce tu password:</label>
					<?=form_password($password)?><?=form_error('password')?>
					<?=form_hidden('token',$token)?>
					<?=isset($_SESSION['time']) ? form_button($button) : form_submit($submit)?>
					<?=form_close()?>
	</div>
     <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy;LaVictoria3021 - <?=date('Y')?></p>
      </div>
    </footer>
   <script src="<?=JBOOTSTRAP?>"></script>
   <?php
   if(isset($_SESSION['time']))
   {
	  	?>
   		   <script src="js/jquery.runner-min.js"></script>
		   <script>
           $('#runner').runner({
                countdown: true,
                startAt: <?=$segundos?>,
                stopAt: 0,
                milliseconds: false
                }).on('runnerFinish', function(){
                    location.reload();
            });
           </script>
   		<?php
   }
   ?>
    <script type="text/javascript">
    /*var secs = <?=isset($_SESSION['time'])?$segundos:'0'?>;
    var currentSeconds = 0;
    var currentMinutes = 0;
    setTimeout('Decrement()',1000);

    function Decrement() {
        currentMinutes = Math.floor(secs / 60);
        currentSeconds = secs % 60;
        if(currentSeconds <= 9) currentSeconds = "0" + currentSeconds;
        secs--;
        document.getElementById("timerText").innerHTML = currentMinutes + ":" + currentSeconds; //Set the element id you need the time put into.
        if(secs !== -1) setTimeout('Decrement()',1000);
    }*/
    </script>
	</body>
</html>