<div class="subcontenedor">
<section class="colores_container">
<?php
echo form_open_multipart('admin/apariencia/actualizacolores');
foreach ($colores as $color): ?>
<div class="colores">
<input type="text" name="color<?=$color['id'] ?>" value="<?=$color['codigo'] ?>" class="kolorPicker">
<span>Color <?=$color['id'] ?></span>
<div class="colorcube" style="background-color:<?=$color['codigo'] ?>">.</div>
<p>Color Actual</p>
</div>
<?php endforeach ?>
<?php
echo "<div id='botones'>";
echo form_submit('submit', 'Guardar');
$js = "onClick='volver_colores()'";
echo form_button('cancel', 'Cancelar', $js);
echo "<div/>";
?>
<?=form_close()?>
</section>	
</div>
<script type="text/javascript">
function volver_colores()
{
	location.href='<?=base_url();?>admin/apariencia/colores';
}
</script>
<script src="<?=base_url()?>js/jquery.kolorpicker.js" type="text/javascript"></script>
<script src="<?=base_url()?>js/jquery-1.5.2.min.js" type="text/javascript"></script>