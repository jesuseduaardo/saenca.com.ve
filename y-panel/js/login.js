function endTimer()
{
	$.get( "/login/timer");
}
function get_token(){
		$.get( "/y-panel/login/token", function(data) {
			$('#token').val(data);
	  }).done($('.loading').hide('slow'));
	}
function get_intentos()
{
	$.get( "/y-panel/login/intentos", function(data) {
		 return data;
	});
}
$(document).ready(function(e) {
	get_token();
	$('#login-form').on('submit', function(e){
		e.preventDefault();
		$('.alert').remove();
		$.ajax({
			url:'login/new_user',
			type: 'POST',
			dataType: "json",
			data: $('#login-form').serializeArray(),
			beforeSend:function(){
				
			},
			success:function(data){
				$div = $("<div class='alert alert-success'>"+data.response+" <span id='timer'></span></div>");
				$('.login-box-head').append($div);
				$('#timer').runner({
					autostart: true,
					countdown: true,
					startAt: 4000,
					stopAt: 0,
					milliseconds : false
				})
				.on('runnerFinish', function(){
					location.reload();
				});
			},
			error: function (data) {
				if(data.status == 403)
				{
					location.reload();
				}
				else
				{
					$div = $("<div class='alert alert-danger'>"+data.responseText+"</div>");
					$('.login-box-head').append($div);
					get_token();
				}
			}
		});
	});
});
