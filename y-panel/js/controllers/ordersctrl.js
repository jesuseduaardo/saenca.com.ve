app.controller('OrdersCtrl', ['$scope', 'Orders', '$route', '$routeParams', function($scope, Orders, $route, $routeParams){
	
	$scope.loading = true;
	
	var id = $routeParams.id;
	if(isFinite(id))
	{
		Orders.get({id:id}, function(data){
			$scope.categ = data.response;
			$scope.loading = false;
		})
	}else{
		Orders.get(function(data){
			$scope.categ = data.response;
			$scope.loading = false;
		})
	}
	$scope.remove = function(id)
	{
		Categ.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
}])