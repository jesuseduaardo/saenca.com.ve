app.controller('ContactoCtrl', ['$scope', 'Contact', '$route', '$routeParams', function($scope, Contact, $route, $routeParams){
	
	$scope.loading = true;
	
	Contact.get(function(data){
		$scope.contacto = data.response;
		$scope.loading = false;
		})
			
	$scope.submit = function(){
		Contact.update(
					{id: $scope.contacto.id},
					{contacto: $scope.contacto}, 
					function(data){
						if(typeof(data.response)!== 'undefined')
						{
							alertify.success(data.response);
						}
						else if(typeof(data.error)!== 'undefined')
						{
							alertify.error(data.error);
						}
					});
		}
}])
app.controller('RedesCtrl', ['$scope', 'Redes', '$route', '$routeParams', function($scope, Redes, $route, $routeParams){
	
	$scope.loading = true;
	
	Redes.get(function(data){
		$scope.redes = data.response; 
		$scope.loading = false;
		})
			
	$scope.submit = function(){
		Redes.update(
					{id: $scope.redes.id},
					{redes: $scope.redes}, 
					function(data){
						if(typeof(data.response)!== 'undefined')
						{
							alertify.success(data.response);
						}
						else if(typeof(data.error)!== 'undefined')
						{
							alertify.error(data.error);
						}
					});
		}
}])