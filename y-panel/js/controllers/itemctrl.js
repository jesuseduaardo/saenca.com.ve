//Item Controller
app.controller('ItemCtrl', ['$scope', 'Item', 'Categ', 'Marca', '$route', '$routeParams', function($scope, Item, Categ, Marca, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/inventario";
	var id = $routeParams.id;
	if(isFinite(id)){
		Item.get({id:id}, function(data){
				$scope.items = data.response;
				$scope.titulo = "Editar Producto";
				$scope.boton = "Actualizar";
			})
		Categ.get(function(data){
				$scope.categ = data.response; 
			})
		Marca.get(function(data){
				$scope.marcas = data.response;
			})
		$scope.control = [{'ctrl_id':'1', 'ctrl_nombre':'Ninguno'},
							  {'ctrl_id':'2', 'ctrl_nombre':'Recomendado'},
							  {'ctrl_id':'3', 'ctrl_nombre':'Mas Vendido'},
							  {'ctrl_id':'4', 'ctrl_nombre':'Oferta'}];
		$scope.loading = false;
	}else if(id == 'nuevo'){
		
		$scope.titulo = "Nuevo Producto";
		$scope.boton = "Guardar";
		$scope.items = { 'items' : { 'i_cupon' : '1' }}
		Categ.get(function(data){
			$scope.categ = data.response; 
		})
		Marca.get(function(data){
			$scope.marcas = data.response;
		})
		$scope.control = [{'ctrl_id':'1', 'ctrl_nombre':'Ninguno'},
						  {'ctrl_id':'2', 'ctrl_nombre':'Recomendado'},
						  {'ctrl_id':'3', 'ctrl_nombre':'Mas Vendido'},
						  {'ctrl_id':'4', 'ctrl_nombre':'Oferta'}];
		$scope.loading = false;
		
	}else{
		Item.get(function(data){
			$scope.items = data.response;
			$scope.titulo = "Inventario";
			$scope.loading = false;
		})
	}
		
	$scope.guardar = function(_items){
			$scope.loading = true;
			Item.save({items: _items}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					$scope.loading = false;
					alertify.success(data.response);
				}else if(typeof(data.error)!== 'undefined')
				{
					$scope.loading = false;
					alertify.error(data.error);
				}
			})
		}
	$scope.update = function(_id, _items){
		$scope.loading = true;
		Item.update({id:_id}, {items:_items}, 
			function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					$scope.loading = false;
					alertify.success(data.response);
				}
				else if(typeof(data.error)!== 'undefined')
				{
					$scope.loading = false;
					alertify.error(data.error);
				}
			});
		}
	$scope.remove = function(id)
	{
		$scope.loading = true;
		Item.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				$scope.loading = false;
				$('#fila-'+id).slideUp('slow');
				alertify.success(data.response);
			}
			else if(typeof(data.error)!== 'undefined')
			{
				$scope.loading = false;
				alertify.error(data.error);
			}
		});
	}
}])