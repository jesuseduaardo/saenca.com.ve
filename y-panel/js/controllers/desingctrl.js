app.controller('AparienciaCtrl', ['$scope', 'Imagen', '$route', '$routeParams', function($scope, Imagen, $route, $routeParams){
	
	$scope.loading = true;
	$scope.imagen = {};
	
	Imagen.get(function(data){
		$scope.loading = false;
		var imagen = data.response;
		$scope.imagen.img1 = imagen[0].nombre;
		$scope.imagen.img2 = imagen[1].nombre;
		$scope.imagen.img3 = imagen[2].nombre;
		$scope.imagen.img4 = imagen[3].nombre;
		})
		
	$scope.actualizar = function(_data){
		$scope.loading = true;
		console.log(_data);
		Imagen.save({slider:_data},function(data){
			if(typeof(data.response)!== 'undefined'){
				$route.reload();
				$scope.loading = false;
				alertify.success(data.response);
			}
			else if(typeof(data.error)!== 'undefined')
			{
				$scope.loading = false;
				alertify.error(data.error);
			}
		});
	}

}])
app.controller('LogoCtrl', ['$scope', 'Pic', '$route', '$routeParams', function($scope, Pic, $route, $routeParams){
	
	$scope.loading = true;
	
	Pic.get(function(data){
		$scope.logo = data.response;
		$scope.loading = false;
		})
		
	$scope.submit = function(){
		Pic.update(
					{id: $scope.logo.id},
					{logo: $scope.logo}, 
					function(data){
						if(typeof(data.response)!== 'undefined')
						{
							alertify.success(data.response);
						}
						else if(typeof(data.error)!== 'undefined')
						{
							alertify.error(data.error);
						}

					});
		}
}])
app.controller('PopCtrl', ['$scope', 'Pop', '$route', '$routeParams', function($scope, Pop, $route, $routeParams){
	
	Pop.get(function(data){
		$scope.emergente = data.response;
		})
	$scope.submit = function(){
		Pop.update(
					{id: $scope.emergente.id},
					{slider: $scope.emergente}, 
					function(data){
						if(typeof(data.response)!== 'undefined')
						{
							alertify.success(data.response);
						}
						else if(typeof(data.error)!== 'undefined')
						{
							alertify.error(data.error);
						}

					});
		}
}])