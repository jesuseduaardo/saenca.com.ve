app.controller('CategCtrl', ['$scope', 'Categ', '$route', '$routeParams', function($scope, Categ, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/categorias";
	
	var id = $routeParams.id;
	if(isFinite(id))
	{
		Categ.get({id:id}, function(data){
			$scope.categ = data.response;
			$scope.titulo = "Editar Categoria";
			$scope.boton = "Actualizar";
			$scope.loading = false;
		})
	}else if(id=='nuevo'){
		$scope.titulo = "Crear Categoria";
		$scope.boton = "Guardar";
		$scope.loading = false;
	}
	else{
		Categ.get(function(data){
			$scope.categ = data.response;
			$scope.loading = false;
		})
	}

	$scope.guardar = function(_data){
		Categ.save({categoria:_data}).$promise.then(function(data){
			if(typeof(data.response)!== 'undefined'){
				$route.reload();
				alertify.success(data.response);
			}else if(typeof(data.error)!== 'undefined'){
				alertify.error(data.error);
			}
		})
	}
	$scope.update = function(_id, _data){
		Categ.update({id:_id}, {categoria:_data}, function(data){
			if(typeof(data.response)!== 'undefined')
			{
				$route.reload();
				alertify.success(data.response);
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
	$scope.remove = function(id)
	{
		Categ.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
}])