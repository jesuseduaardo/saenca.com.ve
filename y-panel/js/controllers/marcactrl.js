app.controller('MarcaCtrl', ['$scope', 'Marca', '$route', '$routeParams', function($scope, Marca, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/marcas"; 
	var id = $routeParams.id;
	
	if(isFinite(id)){
		Marca.get({id:id}, function(data){
			$scope.marca = data.response;
			$scope.titulo = "Editar Marca";
			$scope.boton = "Actualizar";
			$scope.loading = false;
		})
	}else if(id == 'nuevo'){
			$scope.titulo = "Nueva Marca";
			$scope.boton = "Guardar";
			$scope.loading = false;
	}else{
		Marca.get(function(data){
			$scope.marcas = data.response;
			$scope.loading = false;
		});
		}
	$scope.update = function(_id, _data){
		$scope.loading = true;	
			Marca.update({id:_id}, {marca:_data}, function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					$scope.loading = false;	
					alertify.success(data.response);
				}
				else if(typeof(data.error)!== 'undefined')
				{
					alertify.error(data.error);
				}
	
			});
	}
	$scope.guardar = function(_data){
		$scope.loading = true;
			Marca.save({marca:_data}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined'){
					$route.reload();
					$scope.loading = false;
					alertify.success(data.response);
				}else if(typeof(data.error)!== 'undefined'){
					alertify.error(data.error);
				}
			})
	}
	
	$scope.remove = function(id)
	{
		Marca.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
}])