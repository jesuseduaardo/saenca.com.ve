app.controller('BlogCtrl', ['$scope', 'Blog', '$route', '$routeParams', function($scope, Blog, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/blog";
	var id = $routeParams.id;
	
	if(isFinite(id)){
		Blog.get({id:id}, function(data){
			$scope.blog = data.response;
			$scope.titulo = "Editar Articulo";
			$scope.boton = "Editar";
			$scope.loading = false;
		})
	}else if(id == 'nuevo'){
		$scope.titulo = "Nuevo Articulo";
		$scope.boton = "Guardar";
		$scope.loading = false;
	}else{
		Blog.get(function(data){
			$scope.blog = data.response;
			$scope.loading = false;
		})	
	}
	$scope.guardar = function(_data){
		$scope.submit = function(){
			Blog.save({blog:_data}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined'){
					$route.reload();
					alertify.success(data.response);
				}else if(typeof(data.error)!== 'undefined'){
					alertify.error(data.error);
				}
			});
		}
	}
	$scope.update = function(_id, _data){
		$scope.submit = function(){
			Blog.update({id:_id}, {blog:_data}, function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					alertify.success(data.response);
				}
				else if(typeof(data.error)!== 'undefined')
				{
					alertify.error(data.error);
				}
			});
		}
	}
	$scope.remove = function(id)
	{
		Blog.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
}])