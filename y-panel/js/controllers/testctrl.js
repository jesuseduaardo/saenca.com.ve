app.controller('TestimonioCtrl', ['$scope', 'Item', 'Testimonio', '$route', '$routeParams', function($scope, Item, Testimonio, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/testimonios";
	var id = $routeParams.id;
	
	if(isFinite(id)){
		Testimonio.get({id:id}, function(data){
			$scope.com = data.response;
			$scope.loading = false;
		});
		Item.get(function(data){
			$scope.pro = data.response;
			$scope.loading = false; 
		});
		$scope.titulo = "Responder comentario";
		$scope.boton = "Responder";
	}else if(id == 'solicitar'){
		$scope.titulo = "Solicitar Testimonio";
		$scope.boton = "Pedir Testimonio";
		$scope.com ='';
		Item.get(function(data){
			$scope.pro = data.response;
			$scope.loading = false; 
		});	
	}else{
		Testimonio.get(function(data){
			$scope.coment = data.response;
			$scope.loading = false;
		});
	}
	
	$scope.guardar = function(_data){
		$scope.loading = true;
		Testimonio.save({coment:_data}).$promise.then(function(data){
		if(typeof(data.response)!== 'undefined'){
			$route.reload();
			$scope.loading = false;
			alertify.success(data.response);
		}else if(typeof(data.error)!== 'undefined'){
			$scope.loading = false;
			alertify.error(data.error);
			}
		});
	}
	$scope.hide = function(_id){
		Testimonio.get({id:_id}, function(data){
			var com = data.response;
			com.aprobado = 0;
			Testimonio.update({id:_id}, {com:com}, function(data){
				  if(typeof(data.response)!== 'undefined'){
					  $route.reload();
					  $scope.loading = false;
					  alertify.success(data.response);
				  }else if(typeof(data.error)!== 'undefined'){
					  $scope.loading = false;
					  alertify.error(data.error);
				  }
			});
		});
	}
	$scope.show = function(_id){
		Testimonio.get({id:_id}, function(data){
			var com = data.response;
			com.aprobado = 1;
			Testimonio.update({id:_id}, {com:com}, function(data){
				  if(typeof(data.response)!== 'undefined'){
					  $route.reload();
					  $scope.loading = false;
					  alertify.success(data.response);
				  }else if(typeof(data.error)!== 'undefined'){
					  $scope.loading = false;
					  alertify.error(data.error);
				  }
			});
		});
	}
	
	$scope.update = function(_id, _data){
		$scope.loading = true;
		Testimonio.update({id:_id}, {com:_data}, function(data){
			  if(typeof(data.response)!== 'undefined'){
				  $route.reload();
				  $scope.loading = false;
				  alertify.success(data.response);
			  }else if(typeof(data.error)!== 'undefined'){
				  $scope.loading = false;
				  alertify.error(data.error);
			  }
		});
	}
	$scope.remove = function(id){
		$scope.loading = true;
		Testimonio.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				$scope.loading = false;
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				$scope.loading = false;
				alertify.error(data.error);
			}
		});
	}
}])
.directive('datosventa', function() {
  return {
	restrict: 'E',
	link: function(scope, elem, attrs){
		var clasifi = [];
		clasifi[2]='Recomendado';
		clasifi[3]='Mas vendido';
		clasifi[4]='Oferta';
		var cond=[];
		cond[1] = 'Usado';
		scope.data = JSON.parse(attrs.info);
		scope.clasificacion = clasifi[scope.data.clasificacion];
		scope.condicion = cond[scope.data.condicion];
	},
    template: '<span class="quanty">{{ data.cantidad | number }}</span>'+
			  '<div class="product-img"><img src="{{ data.img }}" alt="{{ data.nombre }}"></div>'+
              '<div class="product-info">'+
			  '<dl>'+
              '<dt class="product-title">{{ data.marca }} - {{ data.nombre }}</dt>'+                    
              '<dd class="product-description"><i class="fa fa-barcode"></i> {{ data.codigo }}</dd>'+
			  '<dd class="product-description">{{ data.categoria }}</dd>'+
			  '<dd ng-if="clasificacion" class="product-description">{{ clasificacion }}</dd>'+
			  '<dd ng-if="condicion" class="product-description">{{condicion }}</dd>'+
			  '</dl>'+
              '</div>'
  };
});