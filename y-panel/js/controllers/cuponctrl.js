app.controller('CuponCtrl', ['$scope', 'Cupon', '$route', '$routeParams', function($scope, Cupon, $route, $routeParams){
	
	$scope.loading = true;
	$scope.cancel = "#/cupon";
	
	var id = $routeParams.id;
	
	if(isFinite(id)){
		Cupon.get({id:id}, function(data){
			$scope.cupon = data.response[0];
			$scope.titulo = "Editar cupon";
			$scope.boton = "Editar";
			$scope.loading = false;
		})
	}else if(id == 'nuevo'){
		$scope.titulo = "Nuevo Cupon";
		$scope.boton = "Guardar";
		$scope.cupon = { cu_caduca : 0, cu_monto : 0, cu_usos : 0, cu_activo: '1'}
		$scope.loading = false;
	}else{
		Cupon.get(function(data){
			$scope.cupones = data.response;
			$scope.loading = false;
		})	
	}
	
	$scope.codegen = function() {
		var text = "";
		var possible = "ABCDEFGHJKLMNPQRSTUVWXYZ0123456789";
		for(var i = 0; i < 8; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		$scope.cupon.cu_code = text;
	}
	
	$scope.create = function()
	{
		html2canvas($("#cupon-prev"), {
			onrendered: function(canvas) {
				theCanvas = canvas;
				document.body.appendChild(canvas);
	
				// Convert and download as image
				//Canvas2Image.saveAsBMP(canvas);
				alertify.alert('Se ha generado su cupon, de click derecho y seleccione la <em>opci&oacute;n guardar im&aacute;gen como</em>');	 
				$(".alertify-message").append(canvas);
				// Clean up 
				document.body.removeChild(canvas);
			}
		});
	}
	
	$scope.guardar = function(_data){
		$scope.loading = true;
		Cupon.save({cupon: _data}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined'){
					//$route.reload();
					$scope.loading = false;
					// alertify.success(data.response);
					$scope.create();
				}else if(typeof(data.error)!== 'undefined'){
					$scope.loading = false;
					alertify.error(data.error);
				}
			});
		}
	
	$scope.update = function(_id, _data){
			Cupon.update({id:_id}, {cupon:_data}, function(data){
				if(typeof(data.response)!== 'undefined')
				{
					$route.reload();
					//alertify.success(data.response);
					$scope.create();
				}
				else if(typeof(data.error)!== 'undefined')
				{
					alertify.error(data.error);
				}
			});
		}
	$scope.remove = function(id)
	{
		Cupon.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
		});
	}
}])