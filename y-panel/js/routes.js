var app = angular.module('app', ['ngRoute', 'ngResource'])

.config(['$compileProvider', function ($compileProvider) {
  //$compileProvider.debugInfoEnabled(false);
}])

.config(['$routeProvider', function($routeProvider){
	$routeProvider.when('/', {
			templateUrl:'pages/admin_view.html',
			controller:'MainCtrl'
		})
		.when('/cupon', {
			templateUrl:'pages/cupon.html',
			controller: 'CuponCtrl'
		})
		.when('/cupon/:id', {
			templateUrl:'pages/cupon_post.html',
			controller: 'CuponCtrl'
		})
		.when('/pedidos', {
			templateUrl:'pages/orders.html',
			controller: 'OrdersCtrl'
		})
		.when('/pedidos/:id', {
			templateUrl:'pages/order_view.html',
			controller: 'OrdersCtrl'
		})
		.when('/inventario', {
			templateUrl:'pages/inventario.html',
			controller: 'ItemCtrl'
		})
		.when('/inventario/:id', {
			templateUrl:'pages/inventario_post.html',
			controller: 'ItemCtrl'
		})
		.when('/categorias', {
			templateUrl:'pages/categorias.html',
			controller: 'CategCtrl'
		})
		.when('/categorias/:id', {
			templateUrl:'pages/categorias_post.html',
			controller: 'CategCtrl'
		})
		.when('/marcas', {
			templateUrl:'pages/marcas.html',
			controller: 'MarcaCtrl'
		})
		.when('/marca/:id', {
			templateUrl:'pages/marcas_post.html',
			controller: 'MarcaCtrl'
		})
		.when('/contacto', {
			templateUrl:'pages/contacto.html',
			controller: 'ContactoCtrl'
		})
		.when('/redes', {
			templateUrl:'pages/redes.html',
			controller: 'RedesCtrl'
		})
		.when('/blog', {
			templateUrl:'pages/articulos.html',
			controller: 'BlogCtrl'
		})
		.when('/blog/:id', {
			templateUrl:'pages/articulo_post.html',
			controller: 'BlogCtrl'
		})
		.when('/testimonios', {
			templateUrl:'pages/testimonios.html',
			controller: 'TestimonioCtrl'
		})
		.when('/testimonios/:id', {
			templateUrl:'pages/testimonio_post.html',
			controller: 'TestimonioCtrl'
		})
		.when('/carrusel', {
			templateUrl:'pages/slider.html',
			controller: 'AparienciaCtrl'
		})
		.when('/logo', {
			templateUrl:'pages/logo.html',
			controller: 'LogoCtrl'
		})
		.when('/emergente', {
			templateUrl:'pages/popup.html',
			controller: 'PopCtrl'
		})
		.when('/usuarios', {
			templateUrl:'pages/users.html',
			controller: 'UserCtrl'
		})
		.when('/usuario/:id', {
			templateUrl:'pages/users_post.html',
			controller: 'UserCtrl'
		})
		.when('/usuario', {
			templateUrl:'pages/users_post.html',
			controller: 'CreateUsuarioCtrl'
		})
		.otherwise({ reditrectTo : "#" });
}])
	
.directive('ngReallyClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;
				alertify.confirm(message, function (e) {
					if (e) {
						scope.$apply(attrs.ngReallyClick);
					} else {
						return false;
					}
                });
            });
        }
    }
}])
