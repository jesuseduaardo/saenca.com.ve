app.controller('PanelCtrl', ['$scope', '$http', '$window', function($scope, $http, $window){
	$scope.desactivate = function(){
		$http.get('admin/salida').then(function(response){
      		 $window.location.reload();
   		});
		}
}]);
app.filter("htmlSafe", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    };
}]);
app.controller('MainCtrl', ['$scope', 'Main', '$route', '$routeParams', function($scope, Main, $route, $routeParams){
	$scope.loading = true;
	Main.get(function(data){
		$scope.main = data.response;
		$scope.loading = false; 
	})
}])
app.controller('UserCtrl', ['$scope', 'User', 'Types', '$route', '$routeParams', function($scope, User, Types, $route, $routeParams){
	
	User.get(function(data){
		$scope.usuarios = data.response; 
		})
	var id = $routeParams.id;
	User.get({id:id}, function(data){
			$scope.usuario = data.response;
			$scope.titulo = "Editar Usuario";
			$scope.boton = "Editar";
			$scope.cancel = "#/usuarios";
		})
	Types.get(function(data){
			$scope.tipousu = data.tipousu; 
		})
	$scope.submit = function(){
		User.update(
					{id: $scope.usuario.id},
					{users: $scope.usuario}, 
					function(data){
						if(typeof(data.response)!== 'undefined')
						{
							alertify.success(data.response);
						}
						else if(typeof(data.error)!== 'undefined')
						{
							alertify.error(data.error);
						}

					});
		}
	$scope.remove = function(id)
	{
		User.delete({id:id}).$promise.then(function(data)
		{
			if(typeof(data.response)!== 'undefined')
			{
				alertify.success(data.response);
				$('#fila-'+id).slideUp('slow');
			}
			else if(typeof(data.error)!== 'undefined')
			{
				alertify.error(data.error);
			}
			//$route.reload();
		});
	}
}])
app.controller('CreateUsuarioCtrl', ['$scope', 'User', 'Types', function($scope, User, Types){
	
		$scope.titulo = "Crear Usuario";
		$scope.boton = "Crear";
		$scope.cancel = "#/usuarios"; 
		
		Types.get(function(data){
			$scope.tipousu = data.tipousu; 
		})
		
		$scope.submit = function(){
			User.save({users: $scope.usuario}).$promise.then(function(data){
				if(typeof(data.response)!== 'undefined'){
					alertify.success(data.response);
					angular.copy({}, $scope.usuario);
				}else if(typeof(data.error)!== 'undefined'){
					alertify.error(data.error);
				}
				})
			}
	}])
app.factory('Item', ["$resource", function($resource){
	return $resource('admin/inventario/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})
	}])
app.factory('Categ', ["$resource", function($resource){
	return $resource('admin/categoria/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})
	}])
app.factory('Marca', ["$resource", function($resource){
	return $resource('admin/marca/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Contact', ["$resource", function($resource){
	return $resource('admin/contacto/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Redes', ["$resource", function($resource){
	return $resource('admin/redes/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Cupon', ["$resource", function($resource){
	return $resource('admin/cupon/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Blog', ["$resource", function($resource){
	return $resource('admin/blog/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Testimonio', ["$resource", function($resource){
	return $resource('admin/coment/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})
	}])
app.factory('Imagen', ["$resource", function($resource){
	return $resource('admin/apariencia/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Pic', ["$resource", function($resource){
	return $resource('admin/logo/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Pop', ["$resource", function($resource){
	return $resource('admin/emergente/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('User', ["$resource", function($resource){
	return $resource('admin/usuarios/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Types', ["$resource", function($resource){
	return $resource('admin/tipouser/:id', {id:"@_id"}, {
		update: { method: "PUT", params: { id: "@_id"}}
		})	
	}])
app.factory('Main', ["$resource", function($resource){
	return $resource('admin/resume')	
	}])
app.factory('Orders', ["$resource", function($resource){
	return $resource('admin/orders/:id', {id:"@_id"})	
	}])
app.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
	  attrs.$observe('ngSrc', function(value) {
		  if (!value && attrs.errSrc) {
			attrs.$set('src', attrs.errSrc);
		  }
		});
    }
  }
});
